package asset.trak.networklayer

import asset.trak.model.CaptchaResponse
import asset.trak.modelsrrtrack.*
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

/*https://resqqa.ril.com/RFIDService/AssetTracker/api/assetsync?LastSyncDateTime=2022-03-08%2019:35:31.463*/
interface AssetTrakAPIInterface {

    //AssetTracker/api/assetsync
    @GET("api/assetsync")
    suspend fun geLastSync(@QueryMap hashMap: Map<String, String>): Response<LastSyncResponse>

    @POST("api/assetsync")
    suspend fun posAssetSync(@Body body: RequestBody): Response<ResponseBody>

    //https://resqqa.ril.com/RFIDService/AssetTracker/api/assetsync/rfidUpdate
    @PATCH("api/assetsync/rfidUpdate")
    suspend fun updateMapLocation(@Body body: RequestBody): Response<ResponseBody>

    //-http://localhost:8080//AssetTracker/api/otp/getOtp?mNumber=9321901219
    @GET("api/otp/getOtp")
    suspend fun getOTP(@Query("mNumber") mNumber: String): Response<ResponseGeneral>

    //http://localhost:8080//AssetTracker/api/otp/getToken?mNumber=9321901219&otp=191798
    @GET("api/otp/getToken")
    suspend fun verifyOTP(
        @Query("mNumber") mNumber: String,
        @Query("otp") otp: String
    ): Response<OTPVerifyResponse>

    @GET("api/assetsync/getUserRoleLocData")
    suspend fun getUserLocation(@Query("mNumber") mNumber: String): Response<GetUserLocationResponse>


    @GET
    suspend fun getDummyResponse(
        @Url url: String
    ): Response<LastSyncResponse>

    @FormUrlEncoded
    @POST
    suspend fun captchaVerify(
        @Url url: String,
        @FieldMap hashMap: Map<String, String>
    ): Response<CaptchaResponse>


    //'http://localhost:8080/AssetTracker/api/inventory/post'
    @POST("api/inventory/post")
    suspend fun postOldInventory(@Body body: RequestBody): Response<ResponseBody>

    @GET("api/assetsync/getUserEmail")
    suspend fun getEmails(@Query("LocID") locId: Int): Response<GetEmailsModel>

}