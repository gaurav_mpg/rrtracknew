package asset.trak.model

data class CaptchaResponse(
    val apk_package_name: String,
    val challenge_ts: String,
    val success: Boolean,
    val `error-codes`: List<String>
)