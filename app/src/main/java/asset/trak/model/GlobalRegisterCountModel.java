package asset.trak.model;

public class GlobalRegisterCountModel {
    String Location,Registered,Scanned;

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getRegistered() {
        return Registered;
    }

    public void setRegistered(String registered) {
        Registered = registered;
    }

    public String getScanned() {
        return Scanned;
    }

    public void setScanned(String scanned) {
        Scanned = scanned;
    }
}
