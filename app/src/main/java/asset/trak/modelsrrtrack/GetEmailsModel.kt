package asset.trak.modelsrrtrack

data class GetEmailsModel(
    var `data`: List<Data>?=null,
    var message: String?,
    var statuscode: Int
) {
    data class Data(
        val Email: String
    )
}