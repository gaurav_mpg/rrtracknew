package asset.trak.modelsrrtrack

data class GetUserLocationResponse(
    val `data`: List<Data>?,
    val message: String?,
    val statuscode: Int?
){
    data class Data(
        val LocBarCode: String?,
        val LocID: Int?,
        val LocationName: String?,
        val ParentLocID: Int?
    )
}

