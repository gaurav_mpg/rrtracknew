package asset.trak.modelsrrtrack

data class ResponseGeneral(
    val statusCode:Int,
    val status:Boolean,
    val message:String
)