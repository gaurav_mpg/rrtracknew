package asset.trak.modelsrrtrack

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OTPVerifyResponse(
    val expiresIn: Long?=null,
    val status: Boolean?=null,
    val statusCode: Int?=null,
    val token: String?=null,
    val message: String? = null,
    val userId:String?=null
) : Parcelable