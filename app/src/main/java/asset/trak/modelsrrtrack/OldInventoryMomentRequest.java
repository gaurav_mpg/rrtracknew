package asset.trak.modelsrrtrack;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/*{
  "LocID": 0,
  "Recipent": "string",
  "RfidData": [
    "string"
  ],
  "ScannedBy": "string",
  "ScanID": "string",
  "ScanDate": "string"
}*/
public class OldInventoryMomentRequest {
    private Integer LocID;
    private String Recipent;
    private List<String> RfidData = new ArrayList<String>();
    private String ScannedBy;
    private String ScanID;
    private String ScanDate;

    public Integer getLocID() {
        return LocID;
    }

    public void setLocID(Integer LocID) {
        this.LocID = LocID;
    }

    public String getRecipent() {
        return Recipent;
    }

    public void setRecipent(String recipent) {
        Recipent = recipent;
    }

    public List<String> getRfidData() {
        return RfidData;
    }

    public void setRfidData(List<String> rfidData) {
        RfidData = rfidData;
    }

    public String getScannedBy() {
        return ScannedBy;
    }

    public void setScannedBy(String scannedBy) {
        ScannedBy = scannedBy;
    }

    public String getScanID() {
        return ScanID;
    }

    public void setScanID(String scanID) {
        ScanID = scanID;
    }

    public String getScanDate() {
        return ScanDate;
    }

    public void setScanDate(String scanDate) {
        ScanDate = scanDate;
    }
}