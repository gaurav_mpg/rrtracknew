package asset.trak.modelsrrtrack;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/*{
  "RfidData": [
    "string"
  ],
  "ScannedBy": "string"
}*/

public class PutAwayRequest {



   // @SerializedName("RfidData")
    private List<String> RfidData = new ArrayList<String>();
 //   @SerializedName("ScannedBy")
    private String ScannedBy;

    public List<String> getRfidData() {
        return RfidData;
    }

    public void setRfidData(List<String> rfidData) {
        RfidData = rfidData;
    }

    public String getScannedBy() {
        return ScannedBy;
    }

    public void setScannedBy(String scannedBy) {
        ScannedBy = scannedBy;
    }

    //    public List<String> getRfidData() {
//        return rfidData;
//    }
//
//    public void setRfidData(List<String> rfidData) {
//        this.rfidData = rfidData;
//    }
//
//    public String getScannedBy() {
//        return scannedBy;
//    }
//
//    public void setScannedBy(String scannedBy) {
//        this.scannedBy = scannedBy;
//    }
}
