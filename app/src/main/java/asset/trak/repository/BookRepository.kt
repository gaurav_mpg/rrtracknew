package asset.trak.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import asset.trak.database.BookDao
import asset.trak.model.CaptchaResponse
import asset.trak.modelsrrtrack.*
import asset.trak.networklayer.AssetTrakAPIInterface
import com.google.gson.Gson
import okhttp3.RequestBody
import okhttp3.ResponseBody
import java.io.IOException
import java.net.SocketTimeoutException
import javax.inject.Inject
import javax.inject.Singleton


private const val TAG = "BookRepository"

@Singleton
class BookRepository @Inject constructor(
    private var bookDao: BookDao, var assetTrakAPIInterface: AssetTrakAPIInterface
) {

    var mTakePickAction = MutableLiveData<String>()
    var mLastSync = MutableLiveData<LastSyncResponse>()
    var mAssetSync = MutableLiveData<Int>()
    private val mRetrofitException = MutableLiveData<Boolean>()
    var mOTPVerifyLiveData = MutableLiveData<OTPVerifyResponse>()


    suspend fun getLastSync(syncTime: String?, offLocation: String): LiveData<LastSyncResponse>? {

        try {
            //   val android_id = Settings.Secure.getString(Application.context.contentResolver, Settings.Secure.ANDROID_ID);

            val hashMap = HashMap<String, String>()
            hashMap["LastSyncDateTime"] = syncTime.toString()
            hashMap["OffLocation"] = offLocation
            // hashMap["LastSyncDateTime"]="2022-03-08"
            //  hashMap["MacId"] = android_id


            val response = assetTrakAPIInterface.geLastSync(hashMap)
            //   val response = assetTrakAPIInterface.getDummyResponse("https://raw.githubusercontent.com/manjhi/images_store/main/dummyData.json")
            if (!response.isSuccessful) {
                mLastSync.value =
                    LastSyncResponse(null, response.code(), errorCodeResponse(response.code()))
                return mLastSync
            }
            val data = response.body()
            mLastSync.value = data!!
            return mLastSync
        } catch (error: Throwable) {
            retrofitExceptionHandling(error)
            mLastSync.value = LastSyncResponse(null, null, error.message)
            return mLastSync
        }
        return null
    }


    suspend fun postAssetSync(body: RequestBody): LiveData<Int>? {

        try {
            val response = assetTrakAPIInterface.posAssetSync(body)
            mAssetSync.value = response.code()
            return mAssetSync
        } catch (error: Throwable) {
            retrofitExceptionHandling(error)
            mAssetSync.value = 0
            return mAssetSync
        }
        return null
    }

    suspend fun updateMapLocation(body: RequestBody): LiveData<Int>? {

        try {
            val response = assetTrakAPIInterface.updateMapLocation(body)
            mAssetSync.value = response.code()
            return mAssetSync
        } catch (error: Throwable) {
            retrofitExceptionHandling(error)
            mAssetSync.value = 0
            return mAssetSync
        }
        return null
    }

    suspend fun getOtp(mNumber: String): LiveData<ResponseGeneral>? {
        var mOTPVerifyLiveData = MutableLiveData<ResponseGeneral>()
        try {
            val response = assetTrakAPIInterface.getOTP(mNumber)
            if (response.isSuccessful) {
                mOTPVerifyLiveData.value = response.body()
            } else {
                mOTPVerifyLiveData.value =
                    Gson().fromJson(response.errorBody()?.string(), ResponseGeneral::class.java)
            }
            return mOTPVerifyLiveData
        } catch (error: Throwable) {
            retrofitExceptionHandling(error)
            mOTPVerifyLiveData.value = ResponseGeneral(0,false,error.message.toString())
            return mOTPVerifyLiveData
        }
        return null
    }

    suspend fun verifyOtp(mNumber: String, otp: String): LiveData<OTPVerifyResponse>? {
        try {
            val response = assetTrakAPIInterface.verifyOTP(mNumber, otp)
            if (response.isSuccessful) {
                mOTPVerifyLiveData.value = response.body()
            } else {
                mOTPVerifyLiveData.value =
                    Gson().fromJson(response.errorBody()?.string(), OTPVerifyResponse::class.java)
            }
            return mOTPVerifyLiveData
        } catch (error: Throwable) {
            retrofitExceptionHandling(error)
            mOTPVerifyLiveData.value = OTPVerifyResponse(status = false, statusCode = 0, message = error.message)
            return mOTPVerifyLiveData
        }
        return null
    }

    suspend fun getUserLocation(mNumber: String): LiveData<GetUserLocationResponse>? {
        val result = MutableLiveData<GetUserLocationResponse>()
        try {
            val response = assetTrakAPIInterface.getUserLocation(mNumber)
            if (response.isSuccessful) {
                result.value = response.body()
            } else {
                result.value =
                    GetUserLocationResponse(null, response.errorBody()?.string(), response.code())
            }
            return result
        } catch (error: Throwable) {
            retrofitExceptionHandling(error)
            result.value= GetUserLocationResponse(null,error.message.toString(),0)
            return result
        }
        return null
    }

    suspend fun captchaVerify(jsonObject: Map<String, String>): LiveData<CaptchaResponse>? {
        val result = MutableLiveData<CaptchaResponse>()
        try {
            val response = assetTrakAPIInterface.captchaVerify(
                "https://www.google.com/recaptcha/api/siteverify",
                jsonObject
            )
            if (response.isSuccessful) {
                result.value = response.body()
            }
            return result
        } catch (error: Throwable) {
            retrofitExceptionHandling(error)
        }
        return null
    }

    suspend fun getEmails(locId: Int): LiveData<GetEmailsModel>? {
        val result = MutableLiveData<GetEmailsModel>()
        return try {
            var response = assetTrakAPIInterface.getEmails(locId)
            if (response.isSuccessful) {
                result.value = response.body()
            } else {
                result.value = GetEmailsModel(null, response.errorBody()?.string(), response.code())
            }
            result
        } catch (error: Throwable) {
            retrofitExceptionHandling(error)
            result.value = GetEmailsModel(null, error.message, 0)
            return result
        }

        return null
    }


    fun retrofitExceptionHandling(error: Throwable) {
        Log.e(TAG, "Exception: $error")
        when (error) {
            is SocketTimeoutException -> {
                mRetrofitException.postValue(true)
            }
            is IOException -> {
                mRetrofitException.postValue(true)
            }
            is Exception -> {
                mRetrofitException.postValue(true)
            }
        }
    }

    suspend fun postOldInventory(body: RequestBody): LiveData<Int>? {

        try {
            val response = assetTrakAPIInterface.postOldInventory(body)
            mAssetSync.value = response.code()
            return mAssetSync
        } catch (error: Throwable) {
            retrofitExceptionHandling(error)
        }
        return null
    }

    private fun errorCodeResponse(code: Int): String {
        return when (code) {
            404 -> {
                "Url not found"
            }
            400 -> {
                "Invalid user"
            }
            else -> {
                "Something went wrong"
            }
        }
    }


}


