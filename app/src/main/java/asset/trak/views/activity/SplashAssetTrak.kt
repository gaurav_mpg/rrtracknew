package asset.trak.views.activity

import android.content.Intent
import android.os.Bundle
import asset.trak.utils.Constants.IS_USER_LOGIN
import asset.trak.utils.Constants.PREF_TOKEN
import asset.trak.utils.get
import asset.trak.utils.getAppVersion
import asset.trak.utils.put
import asset.trak.views.baseclasses.BaseActivity
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.home.MainActivity
import kotlinx.android.synthetic.main.splash_assettrak.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class SplashAssetTrak() : BaseActivity(), CoroutineScope {


    private var job: Job = Job()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_assettrak)
        tvVersion.text=getAppVersion()
    }

    override fun onStart() {
        super.onStart()
        launch {
            getNavigateToNext()
        }
    }

    override fun onRestart() {
        super.onRestart()
        launch {
            getNavigateToNext()
        }
    }

    private suspend fun getNavigateToNext() {
        delay(2000)
        // Application.sharedPreferences.put(PREF_TOKEN,"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI5NzcyNTA4OTA2Iiwib3RwIjoiMjk2ODkzIiwiaWF0IjoxNjUwNDU2NTEwLCJleHAiOjE2NTA1NDI5MTB9.Eieur9VWH0uJE6lMRh_9uqRXXuHtkXcbvtxWhHIWjPg")
        if (Application.sharedPreferences.get(IS_USER_LOGIN, false)) {
            val mainIntent = Intent(this, MainActivity::class.java)
            startActivity(mainIntent)
            finish()
        } else {
            Application.sharedPreferences.put(PREF_TOKEN,"NA")
            val mainIntent = Intent(this, AuthActivity::class.java)
            startActivity(mainIntent)
            finish()
        }

    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}