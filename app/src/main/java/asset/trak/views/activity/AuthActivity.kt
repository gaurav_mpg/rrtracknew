package asset.trak.views.activity

import android.content.*
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import asset.trak.database.BookDao
import asset.trak.modelsrrtrack.AppTimeStamp
import asset.trak.modelsrrtrack.GetUserLocationResponse
import asset.trak.modelsrrtrack.OffLocation
import asset.trak.utils.*
import asset.trak.utils.CommonAlertDialog.OnButtonClickListener
import asset.trak.utils.Constants.IS_USER_LOGIN
import asset.trak.utils.Constants.PREF_TOKEN
import asset.trak.views.module.InventoryViewModel
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.HintRequest
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Status
import com.google.android.gms.safetynet.SafetyNet
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.application.Application.sharedPreferences
import com.markss.rfidtemplate.common.Constants.SUCCESS
import com.markss.rfidtemplate.home.MainActivity
import com.shashank.sony.fancytoastlib.FancyToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_auth.*
import kotlinx.android.synthetic.main.content_main_scan.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@AndroidEntryPoint
class AuthActivity : AppCompatActivity() {

    private lateinit var apiClient: GoogleApiClient
    private val inventoryViewModel: InventoryViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        apiClient = GoogleApiClient.Builder(this)
            .addApi(Auth.CREDENTIALS_API).build()
        requestHint()

        btnLogin.isClickable = true
        btnLogin.isEnabled = true

        inventoryViewModel.sessionExpire.observe(
            this
        ) { aBoolean: Boolean ->
            if (aBoolean) {
                CommonAlertDialog(
                    this,
                    "Session Expired\nPlease Login again.",
                    "OK",
                    "",
                    object : OnButtonClickListener {
                        override fun onPositiveButtonClicked() {
                        }

                        override fun onNegativeButtonClicked() {}
                    }).show()
            }
        }

        /* checkRobot.setOnCheckedChangeListener { compoundButton, b ->
             if (b) {
                 if (edPhone.text.isNullOrEmpty()) {
                     checkRobot.isChecked = false
                     FancyToast.makeText(
                         this,
                         "Please enter valid phone number",
                         FancyToast.LENGTH_LONG,
                         FancyToast.WARNING,
                         false
                     ).show()
                     return@setOnCheckedChangeListener
                 }


                 SafetyNet.getClient(this)
                     .verifyWithRecaptcha("6LeJaI0fAAAAAAbYiuVvs9sGDE5Po0eRDgy-zn3R")
                     .addOnSuccessListener {
                         Log.e("DATA", "SUCCESS ${Gson().toJson(it)}")

                         val hashMap = HashMap<String, String>()
                         hashMap["response"] = it.tokenResult.toString()
                         hashMap["secret"] = "6LeJaI0fAAAAAJxIOeovkZZpi2zIGuF7MEUGJ5jH"
                         inventoryViewModel.captchaVerify(hashMap).observe(this) {
                             Log.e("DATA", Gson().toJson(it))
                             if (it.success) {
                                 btnLogin.isClickable = true
                                 btnLogin.isEnabled = true
                             } else {
                                 checkRobot.isChecked = false
                                 FancyToast.makeText(
                                     this,
                                     Gson().toJson(it.`error-codes`),
                                     FancyToast.LENGTH_LONG,
                                     FancyToast.WARNING,
                                     false
                                 ).show()
                             }
                         }

                     }.addOnFailureListener {

                         checkRobot.isChecked = false
                         Log.e("DATA", "FAILED ${Gson().toJson(it)}")
                     }
             } else {
                 btnLogin.isClickable = false
                 btnLogin.isEnabled = false
             }
         }*/

        captchaImg.regenerate()
        captchaImg.setCaptchaType(CaptchaImageView.CaptchaGenerator.BOTH)

        refreshCaptcha.setOnClickListener {
            captchaImg.regenerate()
        }

        btnLogin.setOnClickListener {
            if (!edPhone.text.isNullOrEmpty()) {
                hideKeyboard()
                if (!checkRobot.isChecked) {
                    FancyToast.makeText(
                        this,
                        "Please verify captcha",
                        FancyToast.LENGTH_LONG,
                        FancyToast.WARNING,
                        false
                    ).show()
                    return@setOnClickListener
                }

                if (!privacyCheck.isChecked) {
                    FancyToast.makeText(
                        this,
                        "Please accept Privacy and Policy",
                        FancyToast.LENGTH_LONG,
                        FancyToast.WARNING,
                        false
                    ).show()
                    return@setOnClickListener
                }


//                if (edCaptcha.text.toString() != captchaImg.captchaCode){
//                    FancyToast.makeText(
//                        this,
//                        "Please enter valid captcha",
//                        FancyToast.LENGTH_LONG,
//                        FancyToast.WARNING,
//                        false
//                    ).show()
//                    return@setOnClickListener
//                }

                progressBar.visibility = View.VISIBLE
                btnLogin.isClickable = false
                btnLogin.isEnabled = false
                sendOtp()

                /* if (btnLogin.tag.equals("1")) {
                   if (edOtp.text.toString().trim().isEmpty() && edOtp.text.toString()
                           .trim().length != 6
                   ) {
                       FancyToast.makeText(
                           this,
                           "Please Enter Valid OTP",
                           FancyToast.LENGTH_SHORT,
                           FancyToast.ERROR,
                           false
                       ).show()
                   } else {
                       btnLogin.isClickable = false
                       btnLogin.isEnabled = false
                       verifyOTP()
                       progressBar.visibility = View.VISIBLE
                   }
               }*/
            } else {
                FancyToast.makeText(
                    this,
                    "Please enter valid number",
                    FancyToast.LENGTH_LONG,
                    FancyToast.WARNING,
                    false
                ).show()
            }
        }

        /* resendOtp.setOnClickListener {
             btnLogin.isClickable = false
             btnLogin.isEnabled = false
             sendOtp()
             progressBar.visibility = View.VISIBLE
             edOtp.setText("")
         }*/
    }


    private fun requestHint() {
        val hintRequest = HintRequest.Builder()
            .setPhoneNumberIdentifierSupported(true)
            .build()
        val intent = Auth.CredentialsApi.getHintPickerIntent(
            apiClient, hintRequest
        )
        startIntentSenderForResult(
            intent.intentSender,
            1001, null, 0, 0, 0
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1001) {
            if (resultCode == RESULT_OK) {
                val credential: Credential? = data?.getParcelableExtra(Credential.EXTRA_KEY)
                Log.e("DATA", "${credential?.id}")
                edPhone.setText("${credential?.id}")
            }
        }
    }

    fun sendOtp() {
        inventoryViewModel.getOTP(edPhone.text.toString().replace("+91", "").trim())
            .observe(this) { statusCode ->
                progressBar.visibility = View.GONE
                btnLogin.isClickable = true
                btnLogin.isEnabled = true
                if (statusCode.statusCode == SUCCESS) {
                    Intent(this@AuthActivity, OtpVerifyActivity::class.java).apply {
                        putExtra("mobile", edPhone.text.toString().replace("+91", "").trim())
                        startActivity(this)
                    }
                } else {
                    FancyToast.makeText(
                        this, statusCode.message, FancyToast.LENGTH_LONG, FancyToast.ERROR,
                        false
                    ).show()
                }

            }
    }
}