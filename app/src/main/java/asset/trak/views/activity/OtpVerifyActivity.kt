package asset.trak.views.activity

import android.content.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Html
import android.view.View
import android.widget.AdapterView
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import asset.trak.modelsrrtrack.GetUserLocationResponse
import asset.trak.modelsrrtrack.OffLocation
import asset.trak.utils.Constants
import asset.trak.utils.Constants.PREF_TOKEN
import asset.trak.utils.Constants.USERNAME
import asset.trak.utils.mainCoroutines
import asset.trak.utils.put
import asset.trak.views.module.InventoryViewModel
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.application.Application.sharedPreferences
import com.markss.rfidtemplate.common.Constants.SUCCESS
import com.markss.rfidtemplate.home.MainActivity
import com.shashank.sony.fancytoastlib.FancyToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_otp_verify.*
import kotlinx.android.synthetic.main.activity_otp_verify.resendOtp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import java.util.regex.Matcher
import java.util.regex.Pattern

@AndroidEntryPoint
class OtpVerifyActivity : AppCompatActivity() {


    private val inventoryViewModel: InventoryViewModel by viewModels()

    private var userMobile = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verify)
        userMobile = intent?.getStringExtra("mobile").toString()
        sendOtp()
        submitButton.setOnClickListener {
            if (otpView.otp.isNullOrEmpty()) {
                FancyToast.makeText(
                    this,
                    "Please enter valid OTP",
                    FancyToast.LENGTH_LONG,
                    FancyToast.WARNING,
                    false
                ).show()
            } else {
                verifyOTP()
            }
        }


        val timer = object : CountDownTimer(59000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var seconds = if (millisUntilFinished / 1000 < 10) {
                    "0${millisUntilFinished / 1000}"
                } else {
                    "${millisUntilFinished / 1000}"
                }
                val output =
                    "<font color=#1178AD>Resend OTP</font> <font color=#000000> in ${seconds}s</font>"
                resendOtp.text = Html.fromHtml(output)
            }


            override fun onFinish() {
                resendOtpClick.visibility = View.VISIBLE
                resendOtp.visibility = View.GONE
            }
        }
        timer.start()


        resendOtpClick.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            inventoryViewModel.getOTP(userMobile).observe(this) {
                progressBar.visibility = View.GONE
                if (it.statusCode == SUCCESS) {
                    timer.start()
                    resendOtpClick.visibility = View.GONE
                    resendOtp.visibility = View.VISIBLE
                } else {
                    FancyToast.makeText(
                        this, it.message, FancyToast.LENGTH_LONG, FancyToast.ERROR,
                        false
                    ).show()
                }
            }
        }

        editNumber.setOnClickListener {
            onBackPressed()
        }

    }

    private fun sendOtp() {
        val client = SmsRetriever.getClient(applicationContext)
        client.startSmsUserConsent(null)
    }


    private val smsVerificationReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (SmsRetriever.SMS_RETRIEVED_ACTION == intent.action) {
                val extras = intent.extras
                val smsRetrieverStatus = extras?.get(SmsRetriever.EXTRA_STATUS) as Status

                when (smsRetrieverStatus.statusCode) {
                    CommonStatusCodes.SUCCESS -> {
                        val consentIntent =
                            extras.getParcelable<Intent>(SmsRetriever.EXTRA_CONSENT_INTENT)
                        try {
                            startActivityForResult(consentIntent, 1002)
                        } catch (e: ActivityNotFoundException) {
                        }
                    }
                    CommonStatusCodes.TIMEOUT -> {
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        registerReceiver(smsVerificationReceiver, intentFilter)

    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(smsVerificationReceiver)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1002) {
            if ((resultCode == RESULT_OK) && (data != null)) {
                val message: String = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE).toString()
                getOtpFromMessage(message)
            }
        }
    }

    private fun getOtpFromMessage(message: String) {
        val otpPattern: Pattern = Pattern.compile("(|^)\\d{6}")
        val matcher: Matcher = otpPattern.matcher(message)
        if (matcher.find()) {
            otpView.setOTP(matcher.group(0))
        }
    }


    private fun verifyOTP() {
        progressBar.visibility=View.VISIBLE
        inventoryViewModel.verifyOTP(
            userMobile,
            otpView.otp.toString()
        )
            .observe(this) { otpVerifyResponse ->
                progressBar.visibility = View.GONE
                if (otpVerifyResponse.statusCode == SUCCESS) {
                    sharedPreferences.put(PREF_TOKEN, otpVerifyResponse.token)
                    sharedPreferences.put(USERNAME,otpVerifyResponse.userId)
                    getUserLocation()
                } else {
                    FancyToast.makeText(
                        this,
                        otpVerifyResponse.message,
                        FancyToast.LENGTH_SHORT,
                        FancyToast.ERROR,
                        false
                    ).show()
                }
            }
    }

    private fun getUserLocation() {
        inventoryViewModel.getUserLocation(userMobile)
            .observe(this) { result ->
                if (result.statuscode == SUCCESS) {
                    if (!result.data.isNullOrEmpty()) {
                        if (result.data.size > 1) {
                            displayLocations(result.data)
                        } else {
                            if (result.data.size == 1) {
                                mainCoroutines {
                                    CoroutineScope(Dispatchers.IO).async {
                                        Application.bookDao?.saveOffLocation(
                                            OffLocation(
                                                locationName = result.data[0].LocationName.toString()
                                                    .trim()
                                            )
                                        )
                                    }.await()
                                    sharedPreferences.put(Constants.IS_USER_LOGIN, true)
                                    Intent(this@OtpVerifyActivity, MainActivity::class.java).apply {
                                        startActivity(this)
                                        finishAffinity()
                                    }
                                }
                            } else {
                                FancyToast.makeText(
                                    this,
                                    "Sorry...You don't have assigned any location",
                                    FancyToast.LENGTH_LONG,
                                    FancyToast.ERROR,
                                    false
                                ).show()
                            }

                        }
                    } else {
                        FancyToast.makeText(
                            this,
                            "Sorry...You don't have assigned any location",
                            FancyToast.LENGTH_LONG,
                            FancyToast.ERROR,
                            false
                        ).show()
                    }

                }

            }
    }


    private fun displayLocations(locationData: List<GetUserLocationResponse.Data>) {

        val builder = MaterialAlertDialogBuilder(this, R.style.TransparentDilaog)

        // dialog title
        builder.setTitle("Please Select Office Location")

        val locations =
            locationData?.joinToString { it.LocationName.toString() }?.split(",")?.toTypedArray()

        // set single choice items
        builder.setSingleChoiceItems(
            locations, // array
            -1 // initial selection (-1 none)
        ) { dialog, i -> }


        // alert dialog positive button
        builder.setPositiveButton("Submit") { dialog, which ->
            val position = (dialog as AlertDialog).listView.checkedItemPosition
            // if selected, then get item text
            if (position != -1) {
                val selectedItem = locations!![position]

                //  Log.d("12334", "Favorite color : $selectedItem")
                mainCoroutines {
                    CoroutineScope(Dispatchers.IO).async {
                        Application.bookDao?.saveOffLocation(OffLocation(locationName = selectedItem.trim()))
                        //  inventoryViewModel.updateOffLocation(selectedItem)
                        //Application.bookDao?.saveAppTimeStamp(AppTimeStamp(Date()))
                    }.await()
                    Application.sharedPreferences.put(Constants.IS_USER_LOGIN, true)
                    Intent(this@OtpVerifyActivity, MainActivity::class.java).apply {
                        // putExtra(OTPVERIFYKEY,otpVerifyResponse)
                        startActivity(this)
                        finishAffinity()
                    }
                }

            }
        }

        // alert dialog other buttons
        //builder.setNegativeButton("No",null)
        //  builder.setNeutralButton("Cancel",null)

        // set dialog non cancelable
        builder.setCancelable(false)

        // finally, create the alert dialog and show it
        val dialog = builder.create()
        dialog.show()

        // initially disable the positive button
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false

        // dialog list item click listener
        dialog.listView.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                // enable positive button when user select an item
                dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    .isEnabled = position != -1
            }
    }
}