package asset.trak.views.module

import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import asset.trak.database.entity.BookAttributes
import asset.trak.model.CaptchaResponse
import asset.trak.model.GlobalRegisterCountModel
import asset.trak.modelsrrtrack.*
import asset.trak.repository.BookRepository
import asset.trak.utils.SingleLiveEvent
import asset.trak.utils.getFormattedDate
import asset.trak.utils.ioCoroutines
import com.markss.rfidtemplate.application.Application
import com.shashank.sony.fancytoastlib.FancyToast
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import okhttp3.RequestBody
import okhttp3.ResponseBody
import java.text.SimpleDateFormat
import javax.inject.Inject
import kotlin.collections.ArrayList

@HiltViewModel
class InventoryViewModel @Inject constructor(private val bookRepository: BookRepository) :
    ViewModel() {
    var mLastSyncData = MutableLiveData<LastSyncResponse>()
    var mLastSyncDataSearch = SingleLiveEvent<LastSyncResponse>()
    var mAssetSyncData = MutableLiveData<Int>()
    var listBookAttributes: ArrayList<BookAttributes> = ArrayList()
    var isFirstTime: Boolean = false
    var isSearchClicked: Boolean = false

    private val _timerVal = MutableLiveData("00:00")
    val timerVal: LiveData<String> get() = _timerVal

    private val _timerValTemp = MutableLiveData("0")

    private var timer = Handler(Looper.getMainLooper())

    private val _isStart = MutableLiveData(false)
    val isStart: LiveData<Boolean> get() = _isStart
    var dateLastSync: String? = null
    var dataSyncStatus = SingleLiveEvent<Boolean>()

    private val _sessionExpire = SingleLiveEvent<Boolean>()
    val sessionExpire: SingleLiveEvent<Boolean> get() = _sessionExpire


    init {
        _sessionExpire.postValue(false)
    }

    fun updateTime() {
        timer.postDelayed(t, 1000)
        _isStart.value = true
    }


    fun updateOffLocation(location: String) {
        viewModelScope.launch {
            CoroutineScope(Dispatchers.IO).async {
                Application.bookDao?.updateOffLocation(location)
            }.await()
            getLastSync("")
        }

    }

    val t = Runnable {
        CoroutineScope(Dispatchers.Main).launch {
            val lastVal = _timerValTemp.value.toString().toInt() + 1
            val min = lastVal / 60
            val sec = lastVal % 60
            _timerValTemp.value = lastVal.toString()
            _timerVal.value = getFormattedDate(
                SimpleDateFormat("hh:mm"),
                SimpleDateFormat("HH:mm"), "$min:$sec"
            )
            startAgain()
        }
    }

    fun startAgain() {
        timer.postDelayed(t, 1000)
    }

    fun resetTimer() {
        timer.removeCallbacks(t)
        _isStart.value = false
        _timerVal.value = "00:00"
        _timerValTemp.value = "0"
    }

    fun stopTime() {
        _isStart.value = false
        timer.removeCallbacks(t)
    }


    fun getLastSync(syncTime: String?): LiveData<LastSyncResponse> {
        viewModelScope.launch {
            sessionExpire.postValue(false)
            CoroutineScope(Dispatchers.IO).async {
                if (Application.bookDao?.checkTableIsEmpty() == 0) {
                    Application.bookDao?.saveOffLocation(OffLocation(locationName = "Bangalore Richmond"))
                }
            }.await()

            val result = CoroutineScope(Dispatchers.IO).async {
                Application.bookDao?.getOffLocation()
            }.await()

            val response = bookRepository.getLastSync(syncTime, result?.locationName.toString())
            response?.value?.let {
                if (it.statuscode == 200 && it.data != null) {

                    if (syncTime.isNullOrEmpty()) {
                        val job = CoroutineScope(Dispatchers.IO).async {
                            Application.roomDatabaseBuilder.getBookDao().deleteAssetMainTable()
                            Application.roomDatabaseBuilder.getBookDao().deleteInventoryScanTable()
                            Application.roomDatabaseBuilder.getBookDao()
                                .deleteMapRFIDLocationTable()
                            Application.roomDatabaseBuilder.getBookDao().deleteMasterLocationTable()
                            Application.roomDatabaseBuilder.getBookDao().deleteMasterVendorTable()
                            Application.roomDatabaseBuilder.getBookDao()
                                .deleteTblAssetCatalogueTable()
                            Application.roomDatabaseBuilder.getBookDao()
                                .deleteTblAssetClassCatMapTable()
                            Application.roomDatabaseBuilder.getBookDao()
                                .deleteTblAssetClassificationTable()
                            Application.roomDatabaseBuilder.getBookDao()
                                .deleteTblBookAttributesTable()
                            Application.roomDatabaseBuilder.getBookDao()
                                .deleteTblCatSubCatMapTable()
                            Application.roomDatabaseBuilder.getBookDao()
                                .deleteTblCategoryMasterTable()
                            Application.roomDatabaseBuilder.getBookDao()
                                .deleteTblInventoryMasterTable()
                            Application.roomDatabaseBuilder.getBookDao()
                                .deleteTblLocationMasterTable()
                            Application.roomDatabaseBuilder.getBookDao().deleteTblScanTagTable()
                            Application.roomDatabaseBuilder.getBookDao()
                                .deleteTblSubCategoryMasterTable()
                        }
                        job.await()
                    }
                    saveDataToDatabase(it.data)
                } else if (it.statuscode == 401) {
                    dataSyncStatus.value = false
                    sessionExpire.postValue(true)
                } else {
                    dataSyncStatus.value = false
                    Log.d("tag12122", "getLastSync:${it.statuscode} ")
                    FancyToast.makeText(
                        Application.context, "${it.errorMsg}",
                        FancyToast.LENGTH_SHORT,
                        FancyToast.ERROR,
                        false
                    ).show()
                }
            }
        }
        return mLastSyncData
    }

    fun clearDatabase() {
        viewModelScope.launch {
            val job = CoroutineScope(Dispatchers.IO).async {
                Application.roomDatabaseBuilder.getBookDao().deleteAssetMainTable()
                Application.roomDatabaseBuilder.getBookDao().deleteInventoryScanTable()
                Application.roomDatabaseBuilder.getBookDao()
                    .deleteMapRFIDLocationTable()
                Application.roomDatabaseBuilder.getBookDao().deleteMasterLocationTable()
                Application.roomDatabaseBuilder.getBookDao().deleteMasterVendorTable()
                Application.roomDatabaseBuilder.getBookDao()
                    .deleteTblAssetCatalogueTable()
                Application.roomDatabaseBuilder.getBookDao()
                    .deleteTblAssetClassCatMapTable()
                Application.roomDatabaseBuilder.getBookDao()
                    .deleteTblAssetClassificationTable()
                Application.roomDatabaseBuilder.getBookDao()
                    .deleteTblBookAttributesTable()
                Application.roomDatabaseBuilder.getBookDao()
                    .deleteTblCatSubCatMapTable()
                Application.roomDatabaseBuilder.getBookDao()
                    .deleteTblCategoryMasterTable()
                Application.roomDatabaseBuilder.getBookDao()
                    .deleteTblInventoryMasterTable()
                Application.roomDatabaseBuilder.getBookDao()
                    .deleteTblLocationMasterTable()
                Application.roomDatabaseBuilder.getBookDao().deleteTblScanTagTable()
                Application.roomDatabaseBuilder.getBookDao()
                    .deleteTblSubCategoryMasterTable()
                Application.roomDatabaseBuilder.getBookDao().deleteOffLocation()
                Application.roomDatabaseBuilder.getBookDao().deleteTimeStamp()
            }
            job.await()
        }
    }

    private fun saveDataToDatabase(data: LastSyncData) {
        viewModelScope.launch(Dispatchers.IO) {
            async {
                if (!data.AssetMain.isNullOrEmpty()) {
                    Application.bookDao?.addAssetMain(data.AssetMain)
                    Application.bookDao?.deleteOutwardRecords()
                }

                if (!data.InventoryScan.isNullOrEmpty()) {
                    Application.bookDao?.addInventoryScan(data.InventoryScan)
                }

                if (!data.MasterLocation.isNullOrEmpty()) {
                    Application.bookDao?.addMasterLocation(data.MasterLocation)
                }

                if (!data.MasterVendor.isNullOrEmpty()) {
                    Application.bookDao?.addMasterVendor(data.MasterVendor)
                }

                if (!data.Inventorymaster.isNullOrEmpty()) {
                    Application.bookDao?.addInventoryMaster(data.Inventorymaster)
                }
            }.await()

            dataSyncStatus.postValue(true)
        }

    }


    fun getLastSyncSearch(syncTime: String?, offLocation: String): LiveData<LastSyncResponse> {
        viewModelScope.launch {
            val data = bookRepository.getLastSync(syncTime, offLocation)
            data?.apply {
                mLastSyncDataSearch.value = this.value
            }
        }
        return mLastSyncDataSearch
    }

    fun postAssetSync(body: RequestBody): LiveData<Int> {
        viewModelScope.launch {
            sessionExpire.postValue(false)
            val data = bookRepository.postAssetSync(body)
            data?.apply {
                mAssetSyncData.value = this.value
            }
            if (data?.value == 401) {
                sessionExpire.postValue(true)
            }

        }
        return mAssetSyncData
    }


    fun updateMapLocation(body: RequestBody): LiveData<Int> {
        viewModelScope.launch {
            sessionExpire.postValue(false)
            val data = bookRepository.updateMapLocation(body)
            data?.apply {
                mAssetSyncData.value = this.value
            }

            if (data?.value == 401) {
                sessionExpire.postValue(true)
            }
        }
        return mAssetSyncData
    }

    fun getOTP(mNumber: String): LiveData<ResponseGeneral> {
        val otpVerifyLiveData = SingleLiveEvent<ResponseGeneral>()
        viewModelScope.launch {
            sessionExpire.postValue(false)
            val data = bookRepository.getOtp(mNumber)
            data?.apply {
                otpVerifyLiveData.value = this.value
            }

            if (data?.value?.statusCode == 401) {
                sessionExpire.postValue(true)
            }
        }
        return otpVerifyLiveData
    }

    fun verifyOTP(mNumber: String, otp: String): LiveData<OTPVerifyResponse> {
        val otpVerifyLiveData = SingleLiveEvent<OTPVerifyResponse>()
        viewModelScope.launch {
            sessionExpire.postValue(false)
            val data = bookRepository.verifyOtp(mNumber, otp)
            data?.apply {
                otpVerifyLiveData.value = this.value
            }
        }
        return otpVerifyLiveData
    }

    fun getUserLocation(mNumber: String): LiveData<GetUserLocationResponse> {
        val resultLiveData = SingleLiveEvent<GetUserLocationResponse>()
        viewModelScope.launch {
            sessionExpire.postValue(false)
            val data = bookRepository.getUserLocation(mNumber)
            data?.apply {
                resultLiveData.value = this.value
            }
        }
        return resultLiveData
    }


    fun captchaVerify(jsonObject: Map<String, String>): LiveData<CaptchaResponse> {
        val result = SingleLiveEvent<CaptchaResponse>()
        viewModelScope.launch {
            val data = bookRepository.captchaVerify(jsonObject)
            data?.apply {
                result.value = this.value
            }
        }
        return result
    }

    fun postOldInventory(body: RequestBody): LiveData<Int> {
        viewModelScope.launch {
            sessionExpire.postValue(false)
            val data = bookRepository.postOldInventory(body)
            data?.apply {
                mAssetSyncData.value = this.value
            }
            if (data?.value == 401) {
                sessionExpire.postValue(true)
            }

        }
        return mAssetSyncData
    }

    fun getEmails(locId: Int): LiveData<GetEmailsModel> {
        val result = MutableLiveData<GetEmailsModel>()
        viewModelScope.launch {
            sessionExpire.postValue(false)
            val data = bookRepository.getEmails(locId)
            data?.apply {
                result.value = this.value
                if (this.value?.statuscode == 401) {
                    sessionExpire.postValue(true)
                }
            }
        }
        return result
    }

    fun gettableGlobal(scanID: String): LiveData<List<GlobalRegisterCountModel>> {
        val result = MutableLiveData<List<GlobalRegisterCountModel>>()
        ioCoroutines {
            val data = Application.bookDao.gettableGlobal(scanID)
            result.postValue(data)
        }
        return result
    }
}