package asset.trak.views.adapter

import androidx.fragment.app.Fragment
import asset.trak.views.inventory.NotFoundFragment
import asset.trak.views.inventory.DifferentLoactionFragment
import asset.trak.views.inventory.NotRegisteredFragment
import asset.trak.views.inventory.ReconcileAssetsFragment
import android.view.ViewGroup
import androidx.fragment.app.FragmentStatePagerAdapter


class ReconcileAssetsPagerAdapter (private val fm: ReconcileAssetsFragment) : FragmentStatePagerAdapter(fm.childFragmentManager) {


    private var mCurrentFragment: Fragment? = null

    override fun getItem(position: Int): Fragment {
        return when (position) {

            0 -> NotFoundFragment(fm.locationId,fm.currentScanId)
            1 -> DifferentLoactionFragment(fm.locationId,fm.currentScanId)
            2 -> NotRegisteredFragment(fm.locationId,fm.currentScanId)
            else -> NotFoundFragment(fm.locationId,fm.currentScanId)
        }
    }


    fun getCurrentFragment(): Fragment? {

        return mCurrentFragment
    }


   override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        if (getCurrentFragment() !== `object`) {
            mCurrentFragment = `object` as Fragment
        }
        super.setPrimaryItem(container!!, position, `object`)
    }



    override fun getCount(): Int {
        return 3
    }


    
}