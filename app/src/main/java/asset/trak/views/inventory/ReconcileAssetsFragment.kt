package asset.trak.views.inventory

import android.app.Dialog
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import asset.trak.database.daoModel.BookAndAssetData
import asset.trak.database.entity.Inventorymaster
import asset.trak.database.entity.LocationMaster
import asset.trak.database.entity.ScanTag
import asset.trak.model.LocationUpdate
import asset.trak.modelsrrtrack.AssetMain
import asset.trak.modelsrrtrack.MasterLocation
import asset.trak.utils.Constants
import asset.trak.utils.Constants.disableUserInteraction
import asset.trak.utils.Constants.enableUserInteraction
import asset.trak.utils.inter.UpdateItemInterface
import asset.trak.utils.mainCoroutines
import asset.trak.views.adapter.ReconcileAssetsPagerAdapter
import asset.trak.views.adapter.UpdateLocationAdapter
import asset.trak.views.baseclasses.BaseFragment
import asset.trak.views.listener.RapidReadCallback
import com.google.android.material.tabs.TabLayout
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.application.Application.bookDao
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces
import com.markss.rfidtemplate.home.MainActivity
import com.markss.rfidtemplate.inventory.InventoryListItem
import com.markss.rfidtemplate.locate_tag.LocateOperationsFragment
import com.markss.rfidtemplate.rfid.RFIDController
import com.shashank.sony.fancytoastlib.FancyToast
import com.zebra.rfid.api3.RFIDResults
import kotlinx.android.synthetic.main.custom_app_bar.*
import kotlinx.android.synthetic.main.fragment_reconcile_assets.*
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*


class ReconcileAssetsFragment : BaseFragment(R.layout.fragment_reconcile_assets),
    UpdateItemInterface,
    ResponseHandlerInterfaces.ResponseTagHandler, ResponseHandlerInterfaces.TriggerEventHandler,
    ResponseHandlerInterfaces.ResponseStatusHandler {
    private var whichInventory: String = ""
    private var inventoryMasterList: List<Inventorymaster> = ArrayList()
    private var loginErrorDialog: Dialog? = null
    private var inventorymaster: Inventorymaster? = null
    private lateinit var updateLocationAdapter: UpdateLocationAdapter
    var listBook = ArrayList<BookAndAssetData>()
    private lateinit var adapter: ReconcileAssetsPagerAdapter
    var locationId = 0
    var currentScanId:String=""

    private var listOfLocations = ArrayList<LocationMaster>()

    companion object {
        var selectedPosition12: Int = 0
        lateinit var fragmentCallback1: RapidReadCallback
        fun setFragmentCallback(callback1: RapidReadCallback) {
            fragmentCallback1 = callback1
        }
    }

    val listInventoryList = HashSet<String>()
    override fun handleTagResponse(inventoryListItem: InventoryListItem?, isAddedToList: Boolean) {
        listInventoryList.clear()
        listInventoryList.add(inventoryListItem?.tagID ?: "")
        addScan()

        if (isAddedToList) {
            Log.d("test", "testresponse${isAddedToList}")
            Log.d("test", "testresponse${inventoryListItem!!.tagID}")
            if (!Application.TAG_LIST_MATCH_MODE) {

            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    override fun handleStatusResponse(results: RFIDResults?) {

    }


    override fun triggerPressEventRecieved() {
        if (!RFIDController.mIsInventoryRunning && activity != null) {

//            lifecycleScope.launch {
//                val activity = activity as MainActivity?
//                activity?.inventoryStartOrStop()
//            }
        }
    }

    override fun triggerReleaseEventRecieved() {
        if (RFIDController.mIsInventoryRunning == true && activity != null) {
            //RFIDController.mInventoryStartPending = false;
            lifecycleScope.launch {
                val activity = activity as MainActivity?
                activity?.inventoryStartOrStop()
            }
        }
    }

    private fun notFoundTab(textValue: String) {
        if(isAdded)
        {
            val tabOne = LayoutInflater.from(requireActivity())
                .inflate(R.layout.custom_tab, null) as TextView
            tabOne.text = textValue
            if (tablayout.getTabAt(0)?.customView==null) {
                tablayout.getTabAt(0)?.customView = tabOne
            }else{
                tablayout.getTabAt(0)?.customView = null
                tablayout.getTabAt(0)?.customView = tabOne
            }
        }

    }

    private fun differentLocationTab(textValue: String) {
        val tabOne = LayoutInflater.from(requireActivity())
            .inflate(R.layout.custom_tab, null) as TextView
        tabOne.text = textValue
        if (tablayout.getTabAt(1)?.customView==null) {
            tablayout.getTabAt(1)?.customView = tabOne
        }else{
            tablayout.getTabAt(1)?.customView = null
            tablayout.getTabAt(1)?.customView = tabOne
        }
    }

    private fun notRegisterTab(textValue: String) {
        val tabOne = LayoutInflater.from(requireActivity())
            .inflate(R.layout.custom_tab, null) as TextView
        tabOne.text = textValue
        if (tablayout.getTabAt(2)?.customView==null) {
            tablayout.getTabAt(2)?.customView = tabOne
        }else{
            tablayout.getTabAt(2)?.customView = null
            tablayout.getTabAt(2)?.customView = tabOne
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar24.visibility=View.VISIBLE
        backButton.setOnClickListener {
            getBackToPreviousFragment()
        }

        pageTitle.text=getString(R.string.lblReconcileAssetsSmall)

        locationId = arguments?.getInt("locationId") ?: 0
        currentScanId= arguments?.getString("scanId").toString()
        whichInventory = arguments?.getString("INVENTORY_NAME") ?: ""
        val locationData = arguments?.getParcelable<MasterLocation>("LocationData")

        tvFloorTitle.text = locationData?.Name

        mainCoroutines {
            disableUserInteraction(requireActivity())
            var locationRegsiterCount = CoroutineScope(Dispatchers.IO).async {
                bookDao.getCountLocationId(locationId,currentScanId)
            }.await()
            val totalcount = "Total Assets : $locationRegsiterCount"
            tvTotalRegisteredAssets.text = totalcount
            var inventoryMasterList= CoroutineScope(Dispatchers.IO).async {
                bookDao.getPendingInventoryScanReconsile(currentScanId)
            }.await()
            if (inventoryMasterList.isEmpty() || inventoryMasterList == null) {
                var notFoundCount = 0
                val notFound = "Not\nFound ($notFoundCount)"
                var differntLocationCount = 0
                val differentLocation = "Different\nLocation ($differntLocationCount)"
                var countofNotRegistered = 0
                val notRegistered = "Not\nRegistered ($countofNotRegistered)"
/*
//            tablayout.addTab(tablayout.newTab().setText(notFound));
                notFoundTab(notFound)

                // pendingInventoryScan = bookDao.getPendingInventoryScan(locationData.getId());
                if (!whichInventory.equals("global")) {
//                tablayout.addTab(tablayout.newTab().setText(differentLocation))
                    differentLocationTab(differentLocation)
                }
//            tablayout.addTab(tablayout.newTab().setText(notRegistered))
                notRegisterTab(notRegistered)*/

                tablayout.addTab(tablayout.newTab().setText(notFound));
                tablayout.addTab(tablayout.newTab().setText(differentLocation))
                tablayout.addTab(tablayout.newTab().setText(notRegistered))
            } else {
                inventorymaster = inventoryMasterList.get(0)
                var notFoundCount=0
                CoroutineScope(Dispatchers.IO).async {
                     notFoundCount = bookDao.getCountOfTagsNotFound(inventorymaster!!.scanID)
                }.await()
                val notFound = "Not\nFound ($notFoundCount)"
                var differntLocationCount =CoroutineScope(Dispatchers.IO).async {
                    bookDao.getCountFoundDifferentLoc(locationId,inventorymaster!!.scanID)
                }.await()
                val differentLocation = "Different\nLocation ($differntLocationCount)"
                var countofNotRegistered = CoroutineScope(Dispatchers.IO).async {
                    bookDao.getCountNotRegistered(inventorymaster!!.scanID)
                }.await()
                val notRegistered = "Not\nRegistered ($countofNotRegistered)"
           /*
//            tablayout.addTab(tablayout.newTab().setText(notFound));
                notFoundTab(notFound)
                if (!whichInventory.equals("global")) {
//                tablayout.addTab(tablayout.newTab().setText(differentLocation))
                    differentLocationTab(differentLocation)
                }
//            tablayout.addTab(tablayout.newTab().setText(notRegistered))
                notRegisterTab(notRegistered)*/

                tablayout.addTab(tablayout.newTab().setText(notFound));
                tablayout.addTab(tablayout.newTab().setText(differentLocation))
                tablayout.addTab(tablayout.newTab().setText(notRegistered))
            }

        }
        tablayout.tabGravity = TabLayout.GRAVITY_FILL
        viewPager.offscreenPageLimit = 3

        adapter = ReconcileAssetsPagerAdapter(this)
        viewPager.adapter = adapter
        viewPager.currentItem = selectedPosition12
        Log.d("newval", "onViewCreated: ${viewPager.currentItem}")

        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tablayout))

        buttonScan.setOnClickListener {
            if (viewPager.currentItem == 0) { // Not Found
//                lifecycleScope.launch {
//                    val activity = activity as MainActivity?
//                    activity?.inventoryStartOrStop()
//                }
//                addScan()

            } else if (viewPager.currentItem == 1) { // Different Loc
                if (adapter.getCurrentFragment() is DifferentLoactionFragment) {
                 //   progressBar2.visibility=View.VISIBLE
                    Application.isReconsiled = true
                    val listBook = ArrayList<ScanTag>()
                    var totalItemCount = 0
                    (adapter.getCurrentFragment() as DifferentLoactionFragment).listBook.forEach {
                        if (it.isSelected) {
                            val assetCatalog = ScanTag()
                            assetCatalog.scanId=it.ScanID
                            assetCatalog.locationId = locationId
                            assetCatalog.rfidTag=it.AssetRFID
                            assetCatalog.assetId=it.AssetID
                            assetCatalog.inventorySyncFlag=1
                            assetCatalog.isSelected=it.isSelected
                            listBook.add(assetCatalog)
                            totalItemCount += 1
                        }
                    }
                    if (listBook.isEmpty()) {
                        FancyToast.makeText(
                            requireActivity(),
                            "No Item Selected.",
                            FancyToast.LENGTH_LONG,
                            FancyToast.WARNING,
                            false
                        ).show()
                    } else {
                        mainCoroutines {
                            disableUserInteraction(requireActivity())
                            progressBar23.visibility=View.VISIBLE
                            listBook.forEach {
                                if (it.isSelected) {
                                    CoroutineScope(Dispatchers.IO).async {
                                        bookDao.deleteBookAndAssetData(
                                            inventorymaster!!.scanID,
                                            it.rfidTag.toString()
                                        )
                                    }.await()

                                }
                            }
                            refreshRegisteredAssetCount()
                            (adapter.getCurrentFragment() as DifferentLoactionFragment).updateList()

                            if (inventorymaster == null) {
                                var countofNotRegistered = 0
                                val differentLocation = "Different\nLocation ($countofNotRegistered)"
//                        tablayout.getTabAt(1)?.text = notRegistered
//                                differentLocationTab(notRegistered)
                                tablayout.getTabAt(1)?.text = differentLocation
                            }
                            else {
                                var countofDiffLocation = CoroutineScope(Dispatchers.IO).async {
                                    bookDao.getCountFoundDifferentLoc(locationId,inventorymaster!!.scanID)
                                }.await()
                                val diffLoc = "Different\nLocation ($countofDiffLocation)"
//                        tablayout.getTabAt(1)?.text = diffLoc
//                                differentLocationTab(diffLoc)
                                tablayout.getTabAt(1)?.text = diffLoc
                            }
                            // progressBar2.visibility=View.GONE
                            //   buttonScan.isClickable=true
                            // tvUpdate.isClickable=true
                            progressBar23.visibility=View.GONE
                            enableUserInteraction(requireActivity())
                        }
                    }
                }

            } else if (viewPager.currentItem == 2) {


            }

        }



        tvUpdate.setOnClickListener {
            when (viewPager.currentItem) {
                0 -> {
                    if (adapter.getCurrentFragment() is NotFoundFragment) {
                        mainCoroutines {

                        Application.isReconsiled = true
                        Log.d("UpdateLocation", "onViewCreated: ")
                        val inventoryMasterList = CoroutineScope(Dispatchers.IO).async {
                            bookDao.getPendingInventoryScanReconsile(currentScanId)
                        }.await()
                        var totalItemCount = 0

                     //   tvUpdate.isClickable=false
                        //   val listBook = (adapter.getCurrentFragment() as NotFoundFragment).listBook
                        (adapter.getCurrentFragment() as NotFoundFragment).listBook.forEach {
                            if (it.isSelected) {
                                val assetCatalog = it
                                assetCatalog.LocationId = locationId
                                assetCatalog.inventorySyncFlag = 1
                                // listBook.add(assetCatalog)
                                totalItemCount += 1
                            }
                        }
                        if (totalItemCount == 0) {
//                            Toast.makeText(
//                                requireContext(), "No Item Selected",
//                                Toast.LENGTH_LONG
//                            ).show()
                            FancyToast.makeText(
                                requireActivity(),
                                "No Item Selected.",
                                FancyToast.LENGTH_LONG,
                                FancyToast.WARNING,
                                false
                            ).show()
                        }
                        else {
                            disableUserInteraction(requireActivity())
                            progressBar2.visibility=View.VISIBLE
                            //   val listRfids = ArrayList<ScanTag>()

                            //     val scanList = bookDao.getScanTagAll()
                            val listBook =
                                (adapter.getCurrentFragment() as NotFoundFragment).listBook
                            val pendingInventoryScan = bookDao.getPendingInventoryScanReconsile(
                                currentScanId
                            )
                            val lastItem = pendingInventoryScan.get(0)
                                CoroutineScope(Dispatchers.Main).async {
                                    listBook.forEach {
                                        if (it.isSelected) {
                                            val assetCatalogue = it
                                            if (it.AssetRFID.isNullOrEmpty()) {
                                                //    progressBar2.visibility=View.GONE
                                                FancyToast.makeText(
                                                    requireActivity(),
                                                    "Asset RFID Tag Not Found",
                                                    FancyToast.LENGTH_LONG,
                                                    FancyToast.WARNING,
                                                    false
                                                ).show()
                                                return@async
                                            }
                                            Log.e("DATA","LOC :$locationId NEW_LOC: ${it.LocationId} SCAN_ID: ${lastItem.scanID} RFID: ${it.AssetRFID}")
                                            /*Update the LastScannedLocID to 'F' in ScanTag table for Ignored Records.*/
                                         CoroutineScope(Dispatchers.IO).async {
                                             bookDao.updateLocationAssetMain(
                                                 it.AssetRFID,
                                                 lastItem.scanID
                                             )
                                         }.await()

                                        }
                                    }
                                    if (inventorymaster == null) {
                                        var notFoundCount = 0
                                        val notFound = "Not\nFound ($notFoundCount)"
                                tablayout.getTabAt(0)?.text = notFound
//                                        notFoundTab(notFound)
                                    }
                                    else {
                                        refreshRegisteredAssetCount()
                                        (adapter.getCurrentFragment() as NotFoundFragment).updateList()
                                        inventorymaster = inventoryMasterList[inventoryMasterList.size - 1]
                                        var notFoundCount = bookDao.getCountOfTagsNotFound(inventorymaster!!.scanID)
                                        val notFound = "Not\nFound ($notFoundCount)"
                                tablayout.getTabAt(0)?.text = notFound
//                                        notFoundTab(notFound)
                                    }
                                }.await()
                                progressBar2.visibility=View.GONE
                                //   tvUpdate.isClickable=true
                                enableUserInteraction(requireActivity())
                            }
                        }
                    }
                }
                1 -> {
                    if (adapter.getCurrentFragment() is DifferentLoactionFragment) {
                        mainCoroutines {
                            Application.isReconsiled = true
                            val listBook = ArrayList<ScanTag>()
                            var totalItemCount = 0
                            (adapter.getCurrentFragment() as DifferentLoactionFragment).listBook.forEach {
                                if (it.isSelected) {
                                    val assetCatalog = ScanTag()
                                    assetCatalog.scanId = it.ScanID
                                    assetCatalog.locationId = locationId
                                    assetCatalog.rfidTag = it.AssetRFID
                                    assetCatalog.assetId = it.AssetID
                                    assetCatalog.inventorySyncFlag = 1
                                    assetCatalog.isSelected = it.isSelected
                                    listBook.add(assetCatalog)
                                    totalItemCount += 1
                                }
                            }
                            if (listBook.isEmpty()) {
                                FancyToast.makeText(
                                    requireActivity(),
                                    "No Item Selected.",
                                    FancyToast.LENGTH_LONG,
                                    FancyToast.WARNING,
                                    false
                                ).show()
                            } else {
                                disableUserInteraction(requireActivity())
                                progressBar23.visibility = View.VISIBLE
                                CoroutineScope(Dispatchers.IO).async {
                                    listBook.forEach {
                                        if (it.isSelected) {
                                            bookDao.updateBookAndAssetData(
                                                locationId,
                                                inventorymaster!!.scanID,
                                                it.rfidTag.toString()
                                            )
                                        }
                                    }
                                }.await()

                                progressBar23.visibility = View.GONE
                                enableUserInteraction(requireActivity())

                                refreshRegisteredAssetCount()
                                (adapter.getCurrentFragment() as DifferentLoactionFragment).updateList()

                                if (inventorymaster == null) {
                                    var countofNotRegistered = 0
                                    val notRegistered =
                                        "Different\nLocation ($countofNotRegistered)"
//                                    differentLocationTab(notRegistered)
                                    tablayout.getTabAt(1)?.text = notRegistered
                                } else {
                                    var countofDiffLocation =
                                        bookDao.getCountFoundDifferentLoc(
                                            locationId,
                                            inventorymaster!!.scanID
                                        )
                                    val diffLoc = "Different\nLocation ($countofDiffLocation)"
//                                    differentLocationTab(diffLoc)
                                    tablayout.getTabAt(1)?.text = diffLoc
                                }
                            }
                        }
                    }
                }
                2 -> {
                    if (adapter.getCurrentFragment() is NotRegisteredFragment) {
                        val listRfids = ArrayList<ScanTag>()
                        (adapter.getCurrentFragment() as NotRegisteredFragment).rfidTags.forEach {
                            if (it.isSelected) {
                                listRfids.add(it)
                            }
                        }
                        mainCoroutines {
                            if (listRfids.isNotEmpty()) {
                                /* listRfids.forEach {
                                     bookDao.deleteScanTagNotReg(it.scanId!!, it!!.rfidTag!!)
                                 }*/
                                     disableUserInteraction(requireActivity())
                                progressBar21.visibility = View.VISIBLE
                                CoroutineScope(Dispatchers.IO).async {
                                    bookDao.deleteScanTag(listRfids)
                                }.await()
                                progressBar21.visibility = View.GONE
                                enableUserInteraction(requireActivity())
                                (adapter.getCurrentFragment() as NotRegisteredFragment).updateList()
                                if (inventorymaster == null) {
                                    val countofNotRegistered = 0
                                    val notRegistered = "Not\nRegistered ($countofNotRegistered)"
//                                    notRegisterTab(notRegistered)
                                    tablayout.getTabAt(2)?.text = notRegistered
                                } else {
                                    val countofNotRegistered =
                                        bookDao.getCountNotRegistered(inventorymaster!!.scanID)
                                    val notRegistered = "Not\nRegistered ($countofNotRegistered)"
//                                    notRegisterTab(notRegistered)
                                    tablayout.getTabAt(2)?.text = notRegistered
                                }
                            } else {
                                FancyToast.makeText(
                                    requireActivity(),
                                    "No Item Selected.",
                                    FancyToast.LENGTH_LONG,
                                    FancyToast.WARNING,
                                    false
                                ).show()
                            }
                        }

                    }
                }
            }
        }


        tablayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                selectedPosition12 = tab.position
                Log.d("test", "onTabSelected:${selectedPosition12} ")
                viewPager.currentItem = selectedPosition12
                adapter.notifyDataSetChanged()
//                Handler().postDelayed(
//                    { tablayout.getTabAt(selectedPosition12)?.select() }, 100
//                )


                if (viewPager.currentItem == 0) {
                    buttonScan.visibility=View.GONE
                    //buttonScan.text = "Scan"
                    tvUpdate.text = "Ignore"

                } else if (viewPager.currentItem == 1) {
                     buttonScan.visibility=View.VISIBLE
                    // buttonScan.text = "Update to Current Location"
                    buttonScan.text = "Ignore"
                    tvUpdate.text = "Update to Current Location"

                } else if (viewPager.currentItem == 2) {
                     buttonScan.visibility=View.GONE
                    tvUpdate.text = "Ignore"

                }

            }


            override fun onTabUnselected(tab: TabLayout.Tab) {
                Log.d("location", "onTabUnselected: ${tab.position}")
                if (adapter.getCurrentFragment() is DifferentLoactionFragment) {
                    if (!(adapter.getCurrentFragment() as DifferentLoactionFragment).listBook.isNullOrEmpty()) {
                        (adapter.getCurrentFragment() as DifferentLoactionFragment).listBook.forEach {
                            it.isSelected = false
                        }
                        adapter = ReconcileAssetsPagerAdapter(this@ReconcileAssetsFragment)
                        viewPager.adapter = adapter
                    }
                } else if (adapter.getCurrentFragment() is NotFoundFragment) {
                    if (!(adapter.getCurrentFragment() as NotFoundFragment).listBook.isNullOrEmpty()) {

                        (adapter.getCurrentFragment() as NotFoundFragment).listBook.forEach {
                            it.isSelected = false
                        }
                        adapter = ReconcileAssetsPagerAdapter(this@ReconcileAssetsFragment)
                        viewPager.adapter = adapter
                    }
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                Log.d("location", "onTabReselected: ")

                if (adapter.getCurrentFragment() is NotFoundFragment) {
                    if (!(adapter.getCurrentFragment() as NotFoundFragment).listBook.isNullOrEmpty()) {

                        (adapter.getCurrentFragment() as NotFoundFragment).listBook.forEach {
                            it.isSelected = false
                        }
                        adapter = ReconcileAssetsPagerAdapter(this@ReconcileAssetsFragment)
                        viewPager.adapter = adapter
                    }
                }
            }
        })

        progressBar24.visibility=View.GONE
        enableUserInteraction(requireActivity())
    }

    fun refreshRegisteredAssetCount() {
        mainCoroutines {
            var count = CoroutineScope(Dispatchers.IO).async {
                bookDao.getCountLocationId(locationId,currentScanId)
            }.await()
            val totalcount = "Total Assets : $count"
            tvTotalRegisteredAssets.text = totalcount
         //   progressBar2.visibility=View.GONE
        }
    }

    fun addScan() {
        mainCoroutines {
            val inventoryMasterList: List<Inventorymaster>? = CoroutineScope(Dispatchers.IO).async {
                bookDao.getPendingInventoryScanReconsile(currentScanId)
            }.await()

            if (inventoryMasterList == null || inventoryMasterList.isEmpty()) {

            } else {
                val inventorymaster: Inventorymaster? =
                    inventoryMasterList[inventoryMasterList.size - 1]
                if (inventorymaster == null) {

                } else {
                    for (inventoryTag in listInventoryList) {
                        val scanTag = ScanTag()
                        scanTag.scanId = inventorymaster.scanID
                        scanTag.locationId = locationId
                        scanTag.rfidTag = inventoryTag
                        val getCountOfTagAlready = bookDao.getCountOfTagAlready(
                            scanTag.rfidTag!!, scanTag.scanId!!
                        )
                        if (getCountOfTagAlready == 0) {
                            bookDao.addScanTag(scanTag)
                            if (adapter.getCurrentFragment() is NotFoundFragment)
                                (adapter.getCurrentFragment() as NotFoundFragment).updateList()
                            var notFoundCount = 0
                            CoroutineScope(Dispatchers.IO).async {

                                bookDao.getCountOfTagsNotFound(inventorymaster!!.scanID)
                            }.await()

                            val notFound = "Not\nFound ($notFoundCount)"
//                        tablayout.getTabAt(0)?.text = notFound
//                        notFoundTab(notFound)
                            tablayout.getTabAt(0)?.text = notFound
                        }
                    }

                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (viewPager != null) {

            Handler().postDelayed(
                { tablayout.getTabAt(selectedPosition12)?.select() }, 100
            )
            try {
                viewPager.offscreenPageLimit = 3
                adapter = ReconcileAssetsPagerAdapter(this)
                viewPager.adapter = adapter
                viewPager.setCurrentItem(selectedPosition12)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()
        requireView().setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                return if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    Log.d("backpress", "onKey:Back ")
                    fragmentCallback1.onDataSent(Application.isReconsiled)
                    //  requireActivity().supportFragmentManager.popBackStackImmediate()
                    requireActivity().onBackPressed()
                    true
                } else false
            }
        })

        //   viewPager.adapter?.notifyDataSetChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        selectedPosition12 = 0
    }

    private fun showUpdateLocationDialog() {
        progressBar2.visibility = View.VISIBLE
        initialisation()
        loginErrorDialog = Dialog(requireContext())
        if (loginErrorDialog?.window != null) loginErrorDialog?.window!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        loginErrorDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)

        //set window params and attributes
        val window: Window? = loginErrorDialog?.window
        window?.setGravity(Gravity.CENTER)
        val params: WindowManager.LayoutParams = window?.attributes!!
        params.y = 60
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        params.dimAmount = 0.50f
        params.flags = params.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND
        window.attributes = params

//        val binding: DeleteQueryPopupBinding =
//            DeleteQueryPopupBinding.inflate(LayoutInflater.from(activity))
//        binding.viewmodel = DynamicString
        loginErrorDialog?.setContentView(R.layout.layout_list)

        val reCycleview: RecyclerView =
            loginErrorDialog!!.findViewById(R.id.rvUpdateLocation)
        setAdaptor(reCycleview, loginErrorDialog!!)
        val proceed_btn: AppCompatButton =
            loginErrorDialog!!.findViewById(R.id.proceed_btn)

        //    setAdaptor(reCycleview,loginErrorDialog!!)
        //setAdaptor(reCycleview)


        loginErrorDialog?.setCanceledOnTouchOutside(false)
        loginErrorDialog?.setCancelable(false)
        setWidthPercentOfDialog(loginErrorDialog!!)
        loginErrorDialog!!.show()
    }


    private fun setWidthPercentOfDialog(dialog: Dialog) {
        val percentage = 90
        val percent = percentage.toFloat() / 100
        val dm = Resources.getSystem().displayMetrics
        val rect = dm.run { Rect(0, 0, widthPixels, heightPixels) }
        val percentWidth = rect.width() * percent
        dialog.window?.setLayout(percentWidth.toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
    }


    private fun setAdaptor(reCycleview: RecyclerView, loginErrorDialog: Dialog) {
        val listOfItems = ArrayList<LocationUpdate>()

        listOfLocations.forEach {
            if (!it.locationName.equals(tvFloorTitle.text.toString()))
            //  listOfItems.add(LocationUpdate(it.locationName))
                listOfItems.add(LocationUpdate(it.locationName, false, it.id))
        }

        updateLocationAdapter = UpdateLocationAdapter(
            requireContext(),
            requireActivity().supportFragmentManager,
            listOfItems,
            this,
            loginErrorDialog
        )
        reCycleview.adapter = updateLocationAdapter

    }

    private fun initialisation() {
        mainCoroutines {
            listOfLocations.clear()
            listOfLocations.addAll(CoroutineScope(Dispatchers.IO).async {
                Application.roomDatabaseBuilder.getBookDao().getLocationMasterList()
            }.await())
        }
    }

    override fun onUpdateItemCallback(locationData: LocationUpdate) {
        Log.d("locationData", locationData.title.toString());
//        val listBook = ArrayList<AssetMain>()
//        (adapter.getCurrentFragment() as NotFoundFragment).listBook.forEach {
//            if (it.isSelected) {
//                val assetCatalog = it
//                assetCatalog.LocationId = locationId
//                listBook.add(assetCatalog)
//            }
//        }
//        if (listBook.isNotEmpty()) {
//            listBook.forEach {
//                bookDao.updateLocationIdOfAssets(locationData.id!!,it.id)
//            }
//            refreshRegisteredAssetCount()
//            (adapter.getCurrentFragment() as NotFoundFragment).updateList()
//            progressBar2.visibility=View.INVISIBLE
//            FancyToast.makeText(
//                requireActivity(),
//                "Asset(s) Location Updated Successfully.",
//                FancyToast.LENGTH_LONG,
//                FancyToast.SUCCESS,
//                false
//            ).show()
//
//
//
//        } else
//        {
////            Toast.makeText(
////                requireContext(), "No Item Selected",
////                Toast.LENGTH_LONG
////            ).show()
//
//            FancyToast.makeText(
//                requireActivity(),
//                "No Item Selected.",
//                FancyToast.LENGTH_LONG,
//                FancyToast.WARNING,
//                false
//            ).show()
//            progressBar2.visibility=View.INVISIBLE
//        }
//        inventorymaster = inventoryMasterList[inventoryMasterList.size - 1]
//        var notFoundCount = bookDao.getCountOfTagsNotFound(locationId,inventorymaster!!.scanID)
//        val notFound = "Not Found ($notFoundCount)"
//        tablayout.getTabAt(0)?.text = notFound
//        loginErrorDialog?.cancel()

    }


}