package asset.trak.views.inventory

import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import asset.trak.modelsrrtrack.AssetMain
import asset.trak.utils.Constants
import asset.trak.utils.Constants.disableUserInteraction
import asset.trak.utils.Constants.enableUserInteraction
import asset.trak.utils.getFormattedDate
import asset.trak.utils.showDialogBinding
import asset.trak.views.adapter.DialogOpen
import asset.trak.views.adapter.NotFoundAdapter
import asset.trak.views.baseclasses.BaseFragment
import asset.trak.views.module.InventoryViewModel
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.application.Application.bookDao
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_my_library_search.*
import kotlinx.android.synthetic.main.fragment_not_found.*
import kotlinx.android.synthetic.main.fragment_not_found.searchView
import kotlinx.coroutines.*
import java.text.SimpleDateFormat

@AndroidEntryPoint
class NotFoundFragment(private val locationId: Int, val currentScanId: String) :
    BaseFragment(R.layout.fragment_not_found),
    DialogOpen {
    private lateinit var notFoundAdapter: NotFoundAdapter
    private val inventoryViewModel: InventoryViewModel by activityViewModels()

    var listBook = ArrayList<AssetMain>()
    override fun onResume() {
        super.onResume()
      //  setAdaptor()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            setAdaptor()
        }

        Log.d("NotFoundFragment", "setAdaptor: ${locationId}")
        searchView.queryHint =
            Html.fromHtml("<font color = #D3D3D3>" + getResources().getString(R.string.search) + "</font>");
        tvSelectAll.setOnClickListener {
            lifecycleScope.launch(Dispatchers.Main) {
                progressBarNotFound.visibility = View.VISIBLE
                disableUserInteraction(requireActivity())
                if (tvSelectAll.getTag().equals("0")) {
                    if (!listBook.isEmpty()) {
                        tvSelectAll.visibility = View.VISIBLE
                        tvSelectAll.isClickable=false
                        tvSelectAll.setTag("1")
                        tvSelectAll.text = getString(R.string.deselect_all)

                        CoroutineScope(Dispatchers.IO).async {
                            for (i in 0 until listBook.size) {
                                var locationName = Application.roomDatabaseBuilder?.getBookDao()
                                    ?.getLocationName(listBook.get(i).LocationId ?: 0)
                                listBook[i].Location = locationName?.Name ?: ""
                                listBook[i].isSelected = true
                            }
                        }.await()

                        notFoundAdapter = NotFoundAdapter(
                            requireActivity(),
                            requireActivity().supportFragmentManager,
                            listBook,
                            this@NotFoundFragment
                        )
                        rvNotFound.adapter = notFoundAdapter
                        notFoundAdapter?.notifyDataSetChanged()
                        tvSelectAll.isClickable=true
                    } else {
                        tvSelectAll.visibility = View.GONE
                    }
                } else {
                    tvSelectAll.setTag("0")
                    tvSelectAll.isClickable=false
                    tvSelectAll.text = getString(R.string.select_all)
                    for (i in 0 until listBook.size) {
                        var locationName = Application.roomDatabaseBuilder?.getBookDao()
                            ?.getLocationName(listBook.get(i).LocationId ?: 0)
                        listBook[i].Location = locationName?.Name ?: ""
//                    var category= Application.roomDatabaseBuilder?.getBookDao()?.getCatgeoryName(listBook.get(i).assetCatalogue.categoryId?:0)
//                    listBook[i].assetCatalogue?.categoryName= category?.categoryName?:""
//                    var subcategory= Application.roomDatabaseBuilder?.getBookDao()?.getSubCatgeoryName(listBook.get(i).assetCatalogue.subCategoryId?:0)
//                    listBook[i].assetCatalogue?.categoryName= subcategory?.subCategoryName?:""
                        listBook[i]?.isSelected = false
                    }
                    notFoundAdapter = NotFoundAdapter(
                        requireActivity(),
                        requireActivity().supportFragmentManager,
                        listBook,
                        this@NotFoundFragment
                    )
                    rvNotFound.adapter = notFoundAdapter
                    notFoundAdapter?.notifyDataSetChanged()
                    tvSelectAll.isClickable=true
                }
                enableUserInteraction(requireActivity())
                progressBarNotFound.visibility = View.GONE
            }
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if(::notFoundAdapter.isInitialized)
                    notFoundAdapter.filter.filter(newText)
                return true
            }
        }
        )

        searchView.setOnQueryTextFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                inventoryViewModel.isSearchClicked = true
            }

        }

    }


    suspend fun setAdaptor() {
        lifecycleScope.launch {
            listBook.clear()
            tvSelectAll.isClickable = false
            progressBarNotFound.visibility = View.VISIBLE
            Log.d("NotFoundFrag", "setAdaptor: ${locationId}")
            val inventoryMasterList = bookDao.getPendingInventoryScan(currentScanId)
            if (inventoryMasterList.isEmpty()) {

            } else {
                listBook.addAll(bookDao?.getAssetNotFound(currentScanId) ?: arrayListOf())
                if (listBook.isEmpty()) {
                    tvSelectAll.visibility = View.GONE
                }
                CoroutineScope(Dispatchers.IO).async {
                for (i in 0 until listBook.size) {
                    var locationName = Application.roomDatabaseBuilder?.getBookDao()
                            ?.getLocationName(listBook.get(i).LocationId ?: 0)
                        listBook[i]?.Location = locationName?.Name ?: ""
                } }.await()

//            var category= Application.roomDatabaseBuilder?.getBookDao()?.getCatgeoryName(listBook.get(i).Supplier?:0)
//            listBook[i].assetCatalogue?.categoryName= category?.categoryName?:""
//            var subcategory= Application.roomDatabaseBuilder?.getBookDao()?.getSubCatgeoryName(listBook.get(i).assetCatalogue.subCategoryId?:0)
//            listBook[i].assetCatalogue?.categoryName= subcategory?.subCategoryName?:""


//                listBook.map {
//                    it.isSelected=false
//                }
                notFoundAdapter = NotFoundAdapter(
                    requireActivity(),
                    requireActivity().supportFragmentManager,
                    listBook,
                    this@NotFoundFragment
                )
                rvNotFound?.adapter = notFoundAdapter
                notFoundAdapter?.notifyDataSetChanged()
                progressBarNotFound.visibility = View.GONE
                tvSelectAll.isClickable = true
            }
        }
    }

   suspend fun updateList() {
        setAdaptor()
    }

    override fun openDialogClick(assetMain: AssetMain) {
        requireActivity().showDialogBinding(R.layout.dialog_search_item_layout) { dialog, view ->
            dialog.show()
            val tvTitle = view.findViewById<TextView>(R.id.tvTitle)
            val tvAuthor = view.findViewById<TextView>(R.id.tvAuthor)
            val tvCategory = view.findViewById<TextView>(R.id.tvCategory)
            val tvTag = view.findViewById<TextView>(R.id.tvTag)
            val closeButton = view.findViewById<ImageView>(R.id.closeButton)

            tvTitle.text = assetMain.Supplier
            if (assetMain.SampleType.isNullOrEmpty()) {
                tvAuthor.text = "-"
            } else {
                tvAuthor.text = assetMain.SampleType
            }
            tvCategory.text = assetMain.SampleNature + " | " + assetMain.Season

            tvTag.text =
                assetMain.Location + if (assetMain.ScanDate.isNullOrEmpty()) {
                    ""
                } else {
                    if (assetMain.Location.isNullOrEmpty()) {
                        ""
                    } else {
                        " | "
                    } + getFormattedDate(
                        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"),
                        SimpleDateFormat("dd-MM-yyyy"), assetMain.ScanDate.toString()
                    )
                }


            closeButton.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

}