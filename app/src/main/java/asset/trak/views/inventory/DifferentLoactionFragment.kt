package asset.trak.views.inventory

import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import asset.trak.database.daoModel.BookAndAssetData
import asset.trak.database.entity.AssetCatalogue
import asset.trak.modelsrrtrack.AssetMain
import asset.trak.utils.getFormattedDate
import asset.trak.utils.showDialogBinding
import asset.trak.views.adapter.DialogOpenDifferent
import asset.trak.views.adapter.DifferntLocationAdapter
import asset.trak.views.module.InventoryViewModel
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.application.Application.bookDao
import com.markss.rfidtemplate.application.Application.roomDatabaseBuilder
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_different_loaction.*
import kotlinx.android.synthetic.main.fragment_different_loaction.rvNotFound
import kotlinx.android.synthetic.main.fragment_different_loaction.searchView
import kotlinx.android.synthetic.main.fragment_different_loaction.tvSelectAll
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat

@AndroidEntryPoint
class DifferentLoactionFragment(val locationId: Int, val currentScanId: String) :
    Fragment(R.layout.fragment_different_loaction), DialogOpenDifferent {
    private lateinit var notFoundAdapter: DifferntLocationAdapter
    private val inventoryViewModel: InventoryViewModel by activityViewModels()

    var listBook = ArrayList<AssetMain>()
    override fun onResume() {
        super.onResume()
        //  setAdaptor()
    }

    override fun onPause() {
        super.onPause()
        Log.d("DifferentLoactionFragment", "onPause: ")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdaptor()

        searchView.queryHint =
            Html.fromHtml("<font color = #D3D3D3>" + getResources().getString(R.string.search) + "</font>");

        tvSelectAll.setOnClickListener {
            lifecycleScope.launch(Dispatchers.Main) {
                if (tvSelectAll.getTag().equals("0")) {
                    if (!listBook.isEmpty()) {
                        tvSelectAll.isClickable = false
                        tvSelectAll.visibility = View.VISIBLE
                        tvSelectAll.setTag("1")
                        tvSelectAll.text = getString(R.string.deselect_all)
                        CoroutineScope(Dispatchers.IO).async {
                            for (i in 0 until listBook.size) {
                                var locationName = roomDatabaseBuilder?.getBookDao()
                                    ?.getLocationName(listBook.get(i).LocationId ?: 0)
                                listBook[i].Location = locationName?.Name ?: ""
                                listBook[i].isSelected = true
                            }
                        }.await()

                        notFoundAdapter = DifferntLocationAdapter(
                            requireActivity(),
                            requireActivity().supportFragmentManager,
                            listBook,
                            this@DifferentLoactionFragment
                        )
                        rvNotFound.adapter = notFoundAdapter
                        notFoundAdapter?.notifyDataSetChanged()
                        tvSelectAll.isClickable = true
                    } else {
                        tvSelectAll.visibility = View.GONE
                    }
                } else {
                    tvSelectAll.isClickable = false
                    tvSelectAll.setTag("0")
                    tvSelectAll.text = getString(R.string.select_all)
                    CoroutineScope(Dispatchers.IO).async {
                        for (i in 0 until listBook.size) {
                            var locationName = roomDatabaseBuilder?.getBookDao()
                                ?.getLocationName(listBook.get(i).LocationId ?: 0)
                            listBook[i].Location = locationName?.Name ?: ""
                            listBook[i].isSelected = false
                        }
                    }.await()

                    notFoundAdapter = DifferntLocationAdapter(
                        requireActivity(),
                        requireActivity().supportFragmentManager,
                        listBook,
                        this@DifferentLoactionFragment
                    )
                    rvNotFound.adapter = notFoundAdapter
                    notFoundAdapter?.notifyDataSetChanged()
                    tvSelectAll.isClickable = true
                }
            }
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                notFoundAdapter.filter.filter(newText)
                return true
            }
        })

        searchView.setOnQueryTextFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                inventoryViewModel.isSearchClicked = true
            }

        }
    }

    private fun setAdaptor() {
        lifecycleScope.launch(Dispatchers.Main) {
            tvSelectAll.isClickable = false
            progressBarDiffLoc.visibility = View.VISIBLE
            listBook.clear()
            val inventoryMasterList = bookDao.getPendingInventoryScan(currentScanId)

            if (inventoryMasterList.isEmpty() || inventoryMasterList == null) {
            } else {
                val inventorymaster = inventoryMasterList.get(0)

                CoroutineScope(Dispatchers.IO).async {
                    listBook.addAll(
                        bookDao?.getAssetDifferentLoc(inventorymaster.scanID, locationId)
                            ?: emptyList()
                    )
                }.await()

                if (listBook.isEmpty()) {
                    tvSelectAll.visibility = View.GONE
                }

            }
            CoroutineScope(Dispatchers.IO).async {
                for (i in 0 until listBook.size) {
                    var locationName = roomDatabaseBuilder?.getBookDao()
                        ?.getLocationName(listBook.get(i).LocationId ?: 0)
                    listBook[i].Location = locationName?.Name ?: ""
                    listBook[i].isSelected = false
                }
            }.await()
            notFoundAdapter = DifferntLocationAdapter(
                requireContext(),
                requireActivity().supportFragmentManager,
                listBook,
                this@DifferentLoactionFragment
            )

            rvNotFound.adapter = notFoundAdapter
            progressBarDiffLoc.visibility = View.GONE
            tvSelectAll.isClickable = true
        }

    }

    fun updateList() {
        setAdaptor()
    }

    override fun openDialogClick(assetMain: AssetMain) {
        requireActivity().showDialogBinding(R.layout.dialog_search_item_layout) { dialog, view ->
            dialog.show()
            val tvTitle = view.findViewById<TextView>(R.id.tvTitle)
            val tvAuthor = view.findViewById<TextView>(R.id.tvAuthor)
            val tvCategory = view.findViewById<TextView>(R.id.tvCategory)
            val tvTag = view.findViewById<TextView>(R.id.tvTag)
            val closeButton = view.findViewById<ImageView>(R.id.closeButton)

            tvTitle.text = assetMain.Supplier
            if (assetMain.SampleType.isNullOrEmpty()) {
                tvAuthor.text = "-"
            } else {
                tvAuthor.text = assetMain.SampleType
            }
            tvCategory.text = assetMain.SampleNature + " | " + assetMain.Season

            tvTag.text =
                assetMain.Location + if (assetMain.ScanDate.isNullOrEmpty()) {
                    ""
                } else {
                    if (assetMain.Location.isNullOrEmpty()) {
                        ""
                    } else {
                        " | "
                    } + getFormattedDate(
                        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"),
                        SimpleDateFormat("dd-MM-yyyy"), assetMain.ScanDate.toString()
                    )
                }


            closeButton.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

}