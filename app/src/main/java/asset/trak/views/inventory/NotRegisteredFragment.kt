package asset.trak.views.inventory

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import asset.trak.database.entity.ScanTag
import asset.trak.utils.Constants
import asset.trak.utils.Constants.disableUserInteraction
import asset.trak.utils.Constants.enableUserInteraction
import asset.trak.views.adapter.NotRegsiteredAdapter
import asset.trak.views.baseclasses.BaseFragment
import asset.trak.views.module.InventoryViewModel
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application.bookDao
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_not_registered.*
import kotlinx.android.synthetic.main.fragment_not_registered.searchView
import kotlinx.android.synthetic.main.fragment_not_registered.tvSelectAll
import kotlinx.android.synthetic.main.fragment_reconcile_assets.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NotRegisteredFragment(val locationId: Int, val currentScanId: String) : BaseFragment(R.layout.fragment_not_registered) {
    private lateinit var notFoundAdapter: NotRegsiteredAdapter
     var rfidTags = ArrayList<ScanTag>()
    private val inventoryViewModel: InventoryViewModel by activityViewModels()

//    override fun onResume() {
//        super.onResume()
//        setAdaptor()
//    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdaptor()
        tvSelectAll.setOnClickListener {
            if(tvSelectAll.getTag().equals("0"))
            {
                if(!rfidTags.isNullOrEmpty())
                {
                    tvSelectAll.isClickable=false
                    tvSelectAll.visibility=View.VISIBLE
                    tvSelectAll.setTag("1")
                    tvSelectAll.text=getString(R.string.deselect_all)
                    for (i in 0 until rfidTags.size)  {
                        rfidTags[i].isSelected=true
                    }
                    notFoundAdapter = NotRegsiteredAdapter(requireContext(),requireActivity().supportFragmentManager, rfidTags)
                    rvRegistered.adapter = notFoundAdapter
                    notFoundAdapter?.notifyDataSetChanged()
                    tvSelectAll.isClickable=true
                }
                else
                {
                    tvSelectAll.visibility=View.GONE
                }
            }
            else
            {
                tvSelectAll.isClickable=false
                tvSelectAll.setTag("0")
                tvSelectAll.text=getString(R.string.select_all)
                for (i in 0 until rfidTags.size)  {
                    rfidTags[i].isSelected=false
                }
                notFoundAdapter = NotRegsiteredAdapter(requireContext(),requireActivity().supportFragmentManager, rfidTags)
                rvRegistered.adapter = notFoundAdapter
                notFoundAdapter?.notifyDataSetChanged()
                tvSelectAll.isClickable=true
            }
        }
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                notFoundAdapter.filter.filter(newText)
                return true
            }
        })
        searchView.setOnQueryTextFocusChangeListener { v, hasFocus ->
            if(!hasFocus)
            {
                inventoryViewModel.isSearchClicked=true
            }

        }
    }

    private fun setAdaptor() {
        lifecycleScope.launch(Dispatchers.Main) {
            rfidTags.clear()
            val inventoryMasterList = CoroutineScope(Dispatchers.IO).async {
                bookDao.getPendingInventoryScanNotRegistered(currentScanId)
            }.await()
            if(inventoryMasterList.isEmpty() || inventoryMasterList==null)
            {

            }
            else{
                progressBarNotReg.visibility=View.VISIBLE
                val inventorymaster = inventoryMasterList.get(0)
              CoroutineScope(Dispatchers.IO).async {
                  val tempList =  bookDao?.getAssetNotRegistered(inventorymaster.scanID) ?: arrayListOf()
                  rfidTags.addAll(tempList.filter { !it.rfidTag.isNullOrEmpty() })
                }.await()
                if(rfidTags.isEmpty())
                {
                    tvSelectAll.visibility=View.GONE
                }
                notFoundAdapter = NotRegsiteredAdapter(requireContext(),requireActivity().supportFragmentManager, rfidTags)
                rvRegistered.adapter = notFoundAdapter
                notFoundAdapter?.notifyDataSetChanged()
                       progressBarNotReg.visibility=View.GONE
        }
        }
    }

    fun updateList(){
        setAdaptor()
    }

}