package asset.trak.views.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.annotation.FontRes
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import asset.trak.modelsrrtrack.AppTimeStamp
import asset.trak.modelsrrtrack.AssetMain
import asset.trak.utils.Constants
import asset.trak.utils.apiDateFormat
import asset.trak.utils.ioCoroutines
import asset.trak.utils.mainCoroutines
import asset.trak.views.adapter.OnResultClickListener
import asset.trak.views.adapter.ResultAdapter
import asset.trak.views.baseclasses.BaseFragment
import asset.trak.views.module.InventoryViewModel
import com.google.gson.Gson
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.locate_tag.LocateOperationsFragment
import com.markss.rfidtemplate.rfid.RFIDController
import kotlinx.android.synthetic.main.custom_app_bar.*
import kotlinx.android.synthetic.main.fragment_app_configuration.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import java.util.*
import kotlin.collections.ArrayList


class AppConfigurationFragment : BaseFragment(R.layout.fragment_app_configuration){

    private val inventoryViewModel: InventoryViewModel by activityViewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainCoroutines {
            val namesList = CoroutineScope(Dispatchers.IO).async {
                Application.bookDao?.appConfigLocationNames()
            }.await()
            Log.e("DATA", Gson().toJson(namesList))

            namesList?.let {
                val arrayAdapter: ArrayAdapter<String> = object : ArrayAdapter<String>(
                    requireActivity(),
                    android.R.layout.simple_spinner_dropdown_item,
                    it
                ) {
                    override fun getDropDownView(
                        position: Int,
                        convertView: View?,
                        parent: ViewGroup
                    ): View {
                        var viewIs =
                            requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

                        val v =
                            viewIs.inflate(
                                android.R.layout.simple_spinner_dropdown_item,
                                parent,
                                false
                            )
                        (v as TextView).text = it[position]
                        v.textSize = 14f
                        v.setTextColor(ContextCompat.getColor(requireActivity(),R.color.black))
                        v.typeface= ResourcesCompat.getFont(requireActivity(),R.font.jio_type_medium)
                        return v
                    }

                    @SuppressLint("ViewHolder")
                    @NonNull
                    override fun getView(
                        position: Int,
                        convertView: View?,
                        parent: ViewGroup
                    ): View {
                        var viewIs =
                            requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                        val v =
                            viewIs.inflate(
                                android.R.layout.simple_spinner_dropdown_item,
                                parent,
                                false
                            )
                        (v as TextView).text = it[position]
                        v.textSize = 14f
                        v.setTextColor(ContextCompat.getColor(requireActivity(),R.color.black))
                        v.typeface= ResourcesCompat.getFont(requireActivity(),R.font.jio_type_medium)
                        return v
                    }
                }
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                locationNames.adapter = arrayAdapter


                ioCoroutines {
                    if (Application.bookDao?.checkTableIsEmpty() != 0) {
                        val result = CoroutineScope(Dispatchers.IO).async {
                            Application.bookDao?.getOffLocation()
                        }.await()
                        mainCoroutines {
                            locationNames.setSelection(
                                it.indexOf(
                                    result?.locationName.toString().trim()
                                )
                            )
                        }
                    }

                    mainCoroutines {
                        locationNames.onItemSelectedListener =
                            object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(
                                    p0: AdapterView<*>?,
                                    p1: View?,
                                    p2: Int,
                                    p3: Long
                                ) {
                                    Constants.disableUserInteraction(requireActivity())
                                    progressBar1.visibility = View.VISIBLE
                                    inventoryViewModel.updateOffLocation(it[p2])
                                    mainCoroutines {
                                        Application.bookDao?.saveAppTimeStamp(AppTimeStamp(Date()))
                                    }

                                }

                                override fun onNothingSelected(p0: AdapterView<*>?) {
                                }
                            }

                    }
                }


            }

        }

        backButton.setOnClickListener {
            requireActivity().onBackPressed()
        }

        pageTitle.text = "App Configuration"

        inventoryViewModel.dataSyncStatus.observe(viewLifecycleOwner) { isDataSynced ->
            progressBar1.visibility = View.INVISIBLE
            if (isDataSynced) {
                Constants.enableUserInteraction(requireActivity())
            } else {
                Log.d("tag123", "onViewCreated: ")
            }


        }
    }
}