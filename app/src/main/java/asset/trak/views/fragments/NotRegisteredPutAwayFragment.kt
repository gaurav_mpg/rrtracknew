package asset.trak.views.fragments

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import asset.trak.database.entity.Inventorymaster
import asset.trak.database.entity.ScanTag
import asset.trak.views.adapter.NotRegsiteredAdapter
import asset.trak.views.baseclasses.BaseFragment
import asset.trak.views.inventory.ReconcileAssetsFragment
import asset.trak.views.listener.RapidReadCallback
import asset.trak.views.module.InventoryViewModel
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.application.Application.bookDao
import com.shashank.sony.fancytoastlib.FancyToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.custom_app_bar.*
import kotlinx.android.synthetic.main.fragment_putawaynot_registered.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NotRegisteredPutAwayFragment(val locationId: Int, val currentScanId: String) : BaseFragment(R.layout.fragment_putawaynot_registered) {
    private lateinit var notFoundAdapter: NotRegsiteredAdapter
    var rfidTags = ArrayList<ScanTag>()
    private var inventoryMasterList: List<Inventorymaster> = java.util.ArrayList()
    private var inventorymaster: Inventorymaster? = null
    private val inventoryViewModel: InventoryViewModel by activityViewModels()

    companion object {
        lateinit var fragmentCallback1: RapidReadCallback
        fun setFragmentCallback(callback1: RapidReadCallback) {
            fragmentCallback1 = callback1
        }
    }
    override fun onResume() {
        super.onResume()
        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()
        requireView().setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                return if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    Log.d("backpress", "onKey:Back ")
                    fragmentCallback1.onDataSent(Application.isReconsiled)
                    //  requireActivity().supportFragmentManager.popBackStackImmediate()
                    requireActivity().onBackPressed()
                    true
                } else false
            }
        })
       // setAdaptor()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdaptor()
        inventoryMasterList = bookDao.getPendingInventoryScan(currentScanId)
        inventorymaster = inventoryMasterList.get(0)

        backButton.setImageResource(R.drawable.ic_back)
        backButton.setOnClickListener {
            requireActivity().onBackPressed()
        }

        pageTitle.text="Reconcile Not Registered"

        tvSelectAll.setOnClickListener {
            if(tvSelectAll.getTag().equals("0"))
            {
                if(!rfidTags.isNullOrEmpty())
                {
                    tvSelectAll.isClickable=false
                    tvNotRegistered.isClickable=false
                    tvSelectAll.visibility=View.VISIBLE
                    tvSelectAll.setTag("1")
                    tvSelectAll.text=getString(R.string.deselect_all)
                    for (i in 0 until rfidTags.size)  {
                        rfidTags[i].isSelected=true
                    }
                    notFoundAdapter = NotRegsiteredAdapter(requireContext(),requireActivity().supportFragmentManager, rfidTags)
                    rvRegistered.adapter = notFoundAdapter
                    notFoundAdapter?.notifyDataSetChanged()
                    tvSelectAll.isClickable=true
                    tvNotRegistered.isClickable=true
                }
                else
                {
                    tvSelectAll.visibility=View.GONE
                }
            }
            else
            {
                tvSelectAll.isClickable=false
                tvNotRegistered.isClickable=false
                tvSelectAll.setTag("0")
                tvSelectAll.text=getString(R.string.select_all)
                for (i in 0 until rfidTags.size)  {
                    rfidTags[i].isSelected=false
                }
                notFoundAdapter = NotRegsiteredAdapter(requireContext(),requireActivity().supportFragmentManager, rfidTags)
                rvRegistered.adapter = notFoundAdapter
                notFoundAdapter?.notifyDataSetChanged()
                tvSelectAll.isClickable=true
                tvNotRegistered.isClickable=true
            }
        }
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                notFoundAdapter.filter.filter(newText)
                return true
            }
        })
        searchView.setOnQueryTextFocusChangeListener { v, hasFocus ->
            if(!hasFocus)
            {
                inventoryViewModel.isSearchClicked=true
            }

        }

        tvNotRegistered.setOnClickListener {
            Application.isReconsiled=true
            val listRfids = java.util.ArrayList<ScanTag>()
            rfidTags.forEach {
                if (it.isSelected) {
                    listRfids.add(it)
                }
            }
            if (listRfids.isNotEmpty()) {
                /* listRfids.forEach {
                     bookDao.deleteScanTagNotReg(it.scanId!!, it!!.rfidTag!!)
                 }*/
                     lifecycleScope.launch {
                         CoroutineScope(Dispatchers.IO).async {
                             bookDao.deleteScanTag(listRfids)
                         }.await()
                     }


                updateList()
            } else {
                FancyToast.makeText(
                    requireActivity(),
                    "No Item Selected.",
                    FancyToast.LENGTH_LONG,
                    FancyToast.WARNING,
                    false
                ).show()
            }
        }
    }

    private fun setAdaptor() {
        lifecycleScope.launch(Dispatchers.Main) {
            rfidTags.clear()
            progressBar25.visibility=View.VISIBLE
            tvSelectAll.isClickable=false
            tvNotRegistered.isClickable=false
            val inventoryMasterList = bookDao.getPendingInventoryScan(currentScanId)
            if(inventoryMasterList.isEmpty() || inventoryMasterList==null)
            {

            }
            else{
                val inventorymaster = inventoryMasterList.get(0)

                val tempList = CoroutineScope(Dispatchers.IO).async {
                        bookDao?.getAssetNotRegistered(inventorymaster.scanID) ?: arrayListOf()
                    }.await()
                rfidTags.addAll(tempList.filter { !it.rfidTag.isNullOrEmpty() })
                //    rfidTags.addAll(bookDao?.getAssetNotRegistered(inventorymaster.scanID) ?: arrayListOf())
                if(rfidTags.isEmpty())
                {
                    tvSelectAll.visibility=View.GONE
                }
                //////

                notFoundAdapter = NotRegsiteredAdapter(requireContext(),requireActivity().supportFragmentManager, rfidTags)
                rvRegistered.adapter = notFoundAdapter
                notFoundAdapter?.notifyDataSetChanged()
                progressBar25.visibility=View.GONE
                tvSelectAll.isClickable=true
                tvNotRegistered.isClickable=true
            }



        }

    }

    fun updateList(){
        setAdaptor()
    }

}