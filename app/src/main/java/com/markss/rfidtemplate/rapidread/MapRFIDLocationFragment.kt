package com.markss.rfidtemplate.rapidread

import asset.trak.utils.showYesNoAlert
import asset.trak.utils.Constants.isInternetAvailable
import asset.trak.utils.PutAwayRequestToFirebase
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.ResponseTagHandler
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.TriggerEventHandler
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.BatchModeEventHandler
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.ResponseStatusHandler
import asset.trak.views.listener.RapidReadCallback
import com.google.android.material.floatingactionbutton.FloatingActionButton
import asset.trak.modelsrrtrack.MasterLocation
import asset.trak.database.entity.Inventorymaster
import asset.trak.views.module.InventoryViewModel
import androidx.constraintlayout.widget.ConstraintLayout
import android.os.Bundle
import android.app.Activity
import androidx.lifecycle.ViewModelProvider
import com.markss.rfidtemplate.home.MainActivity
import asset.trak.views.fragments.NotRegisteredPutAwayFragment
import asset.trak.utils.CommonAlertDialog.OnButtonClickListener
import asset.trak.views.inventory.ReconcileAssetsFragment
import com.markss.rfidtemplate.rfid.RFIDController
import asset.trak.views.fragments.HomeFragment
import com.shashank.sony.fancytoastlib.FancyToast
import com.zebra.rfid.api3.RFIDResults
import com.markss.rfidtemplate.inventory.InventoryListItem
import asset.trak.modelsrrtrack.AssetMain
import asset.trak.database.entity.ScanTag
import asset.trak.modelsrrtrack.PutAwayRequest
import com.google.gson.Gson
import okhttp3.RequestBody
import android.os.Looper
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import asset.trak.utils.mainCoroutines
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.common.Constants
import com.markss.rfidtemplate.rapidread.MapRFIDLocationFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MapRFIDLocationFragment : Fragment(), ResponseTagHandler, TriggerEventHandler,
    BatchModeEventHandler, ResponseStatusHandler, RapidReadCallback {
    private var uniqueTags: TextView? = null
    private var inventoryButton: FloatingActionButton? = null
    var batchModeEventReceived = false
    private val locationData: MasterLocation? = null
    private var tvNotRegisteredCount: TextView? = null
    private var tvILastRecordDate: TextView? = null
    private var btnReconcile: Button? = null
    private var buttonView: Button? = null
    private var btnInventoryRecord: Button? = null
    private var llBottomParent: LinearLayout? = null
    private var progressBar: ProgressBar? = null
    private var listInventoryList = HashSet<String>()
    private var scannedList = HashSet<String>()
    private var pendingInventoryScan: List<Inventorymaster>? = null
    private var inventoryViewModel: InventoryViewModel? = null
    private var countExistingItem = 0
    private var countNewItem = 0
    private var countNotRegistered = 0
    private var isFromReconsile = false
    private var imgIgnore: ImageView? = null
    private var tvRegisteredCount: TextView? = null
    private var tvLocation: TextView? = null
    private var tvExistingItemCount: TextView? = null
    private var tvNewItemCount: TextView? = null
    private var itemCountLay12: ConstraintLayout? = null
    private var currentScanId: String? = null
    private var isFirstOpen = true
    var ivBack: ImageView? = null
    private var scanTime = "Scan started on: NA"
    private var isScan = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_maplocation, container, false)
        imgIgnore = view.findViewById(R.id.closeButton)
        imgIgnore?.setVisibility(View.VISIBLE)
        val title = view.findViewById<TextView>(R.id.pageTitle)
        title.text = getString(R.string.lblRecordInventory)
        ivBack = view.findViewById(R.id.backButton)
        return view
    }

    override fun onStop() {
        super.onStop()
        isFromReconsile = false
        requireActivity().unregisterReceiver(receiver)
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_rr, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inventoryViewModel = ViewModelProvider(requireActivity()).get(
            InventoryViewModel::class.java
        )
        val mainActivity = activity as MainActivity?
        mainActivity!!.supportActionBar!!.navigationMode = ActionBar.NAVIGATION_MODE_STANDARD
        inventoryButton = mainActivity.findViewById(R.id.rr_inventoryButton)
        uniqueTags = mainActivity.findViewById(R.id.uniqueTagContent)
        tvNotRegisteredCount = requireActivity().findViewById(R.id.notRegisteredCount)
        tvILastRecordDate = requireActivity().findViewById(R.id.tvILastRecord12)
        btnReconcile = requireActivity().findViewById(R.id.btnReconcile)
        buttonView = requireActivity().findViewById(R.id.buttonView)
        btnInventoryRecord = requireActivity().findViewById(R.id.btnInventoryRecord)
        llBottomParent = requireActivity().findViewById(R.id.llBottomParent2)
        progressBar = requireActivity().findViewById(R.id.progressBar3)
        tvRegisteredCount = requireActivity().findViewById(R.id.tvRegisteredCountrr)
        tvLocation = requireActivity().findViewById(R.id.tvLocation211)
        //  locationData = getArguments().getParcelable("LocationData");
        //  inventorymasterList = bookDao.getPendingInventoryScan(locationData.getLocID());
        pendingInventoryScan = Application.bookDao.getGlobalPendingInventoryScan()
        val (_, scanID) = pendingInventoryScan!![0]
        currentScanId = scanID
        itemCountLay12 = requireActivity().findViewById(R.id.itemCountLay12)
        tvExistingItemCount = requireActivity().findViewById(R.id.tvExistingItemCount)
        tvNewItemCount = requireActivity().findViewById(R.id.tvNewItemCount)
        // tvRegisteredCount.setText(String.valueOf(bookDao.getCountLocationIdPutAwayJava(locationData.getLocID())));
        NotRegisteredPutAwayFragment.setFragmentCallback(this)
        imgIgnore!!.setOnClickListener { v: View? ->
            showYesNoAlert(
                requireActivity(),
                "Are you sure you want to abandon this scan? Your data will be lost.",
                object : OnButtonClickListener {
                    override fun onPositiveButtonClicked() {
                        Application.isAbandoned = true
                        try {
                            if (pendingInventoryScan != null && !pendingInventoryScan!!.isEmpty()) {
                                val lastItem = pendingInventoryScan!![0]
                                Application.bookDao.deletemapRFIDLocationAll()
                                Application.bookDao.deleteScanTagSingle(lastItem.scanID)
                                Application.bookDao.deleteInventorySingle(lastItem.scanID)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        requireActivity().supportFragmentManager.popBackStackImmediate()
                    }

                    override fun onNegativeButtonClicked() {}
                })
        }
        listInventoryList = HashSet()
        scannedList = HashSet()

//        ExtensionKt.updateListToFirebase("PutAway",requireActivity(),listInventoryList);
        Log.d(Application.TAG, "onActivityCreated Put Away: $currentScanId")
        pendingInventoryScan = Application.bookDao.getPendingInventoryScan(
            currentScanId!!
        )
        ReconcileAssetsFragment.setFragmentCallback(this)
        if (!pendingInventoryScan!!.isEmpty()) {
            val tags = Application.bookDao.getScanRfid(
                pendingInventoryScan!![0].scanID
            )
            for (tag in tags) {
                scannedList.add(tag)
            }
        }
        if (scannedList.size < 10 && scannedList.size > 0) {
            uniqueTags?.setText("0" + scannedList.size)
        } else {
            uniqueTags?.setText(Integer.toString(scannedList.size))
        }
        if (RFIDController.mIsInventoryRunning) {
            inventoryButton?.setBackgroundResource(android.R.drawable.ic_media_pause)
        } else {
            inventoryButton?.setBackgroundResource(android.R.drawable.ic_media_play)
        }
        if (RFIDController.mRRStartedTime == 0L) Application.TAG_READ_RATE =
            0 else Application.TAG_READ_RATE =
            (Application.TOTAL_TAGS / (RFIDController.mRRStartedTime / 1000.toFloat())).toInt()
        if (Application.missedTags > 9999) {
            uniqueTags?.setTextSize(45f)
        }
        //   tvLocation.setText(locationData.getName());
        updateTexts()
        ivBack!!.setOnClickListener { v: View? ->
            requireActivity().supportFragmentManager.popBackStack(
                null,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            )
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.content_frame, HomeFragment()).commit()
        }
        btnReconcile?.setOnClickListener(View.OnClickListener { v: View? ->
            val fragInfo = NotRegisteredPutAwayFragment(0, currentScanId!!)
            requireActivity().supportFragmentManager.beginTransaction().replace(
                R.id.content_frame,
                fragInfo, MainActivity.TAG_CONTENT_FRAGMENT
            ).addToBackStack(null).commit()
        })
        if (isFromReconsile) {
            inventoryButton?.setTag("1")
            llBottomParent?.setVisibility(View.VISIBLE)
            inventoryButton?.setImageResource(android.R.drawable.ic_media_play)
            mainCoroutines {
                showCountFound()
            }
        } else {
            if (isFirstOpen) {
                //    updateScanDateAndTime();
                // inventoryButton.performClick();
                isFirstOpen = false
            } else {
                inventoryButton?.setTag("1")
                llBottomParent?.setVisibility(View.VISIBLE)
                inventoryButton?.setImageResource(android.R.drawable.ic_media_play)
                mainCoroutines {
                    showCountFound()
                }
            }
        }

        /*   inventoryButton.setTag("1");
        llBottomParent.setVisibility(View.VISIBLE);
        inventoryButton.setImageResource(android.R.drawable.ic_media_play);
        addDataToScanTag();
        showCountFound();
        updateCountInDb();*/
        btnInventoryRecord?.setOnClickListener(View.OnClickListener { v: View? ->
            if (isInternetAvailable(requireContext())) {
                if (countNotRegistered == 0 && countNewItem > 0) {
                    postAssetSync()
                } else if (countNewItem == 0) {
                    FancyToast.makeText(
                        requireActivity(),
                        "You have not any RFID Tags for Put Away.",
                        FancyToast.LENGTH_LONG,
                        FancyToast.WARNING,
                        false
                    ).show()
                } else if (countNotRegistered > 0) {
                    FancyToast.makeText(
                        requireActivity(),
                        "Please reconcile Not Registered RFID(s) to proceed.",
                        FancyToast.LENGTH_LONG,
                        FancyToast.WARNING,
                        false
                    ).show()
                } else {
                    FancyToast.makeText(
                        requireActivity(),
                        getString(R.string.something_went_wrong),
                        FancyToast.LENGTH_LONG,
                        FancyToast.WARNING,
                        false
                    ).show()
                }
            }
        })


        /* inventoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (inventoryButton.getTag().equals("1")) {
                    inventoryButton.setTag("0");
                    llBottomParent.setVisibility(View.GONE);
                    inventoryButton.setImageResource(android.R.drawable.ic_media_pause);
                    listInventoryList = new HashSet<>();

                    //new requirement
                    itemCountLay12.setVisibility(View.GONE);
                    //inventoryViewModel.updateTime();
                } else {
                    //inventoryViewModel.stopTime();
                    inventoryButton.setTag("1");
                    llBottomParent.setVisibility(View.VISIBLE);
                    inventoryButton.setImageResource(android.R.drawable.ic_media_play);
                    addDataToScanTag();
                    showCountFound();
                    updateCountInDb();
                }
            }

        });*/
        tvILastRecordDate?.setText(scanTime)
    }

    private fun updateScanDateAndTime() {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
        val cal = Calendar.getInstance()
        val dateFormat = sdf.format(cal.time)
        try {
            val latestDate = sdf.parse(dateFormat)
            val changedFormat = SimpleDateFormat("dd-MM-yyyy | hh:mm a")
            scanTime =
                getString(R.string.last_recorded_02_02_2022_10_43_am) + " " + changedFormat.format(
                    latestDate
                )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun updateTexts() {}
    override fun onDetach() {
        super.onDetach()
        if (RFIDController.mIsInventoryRunning) {
            inventoryButton!!.performClick()
        }
    }

    /**
     * method to reset tags info on the screen before starting inventory operation
     */
    fun resetTagsInfo() {
        updateTexts()
    }

    /**
     * method to start inventory operation on trigger press event received
     */
    override fun triggerPressEventRecieved() {
        if (!RFIDController.mIsInventoryRunning && activity != null) {
            requireActivity().runOnUiThread {
                val activity = activity as MainActivity?
                activity?.inventoryStartOrStop()
            }
        }
    }

    /**
     * method to stop inventory operation on trigger release event received
     */
    override fun triggerReleaseEventRecieved() {
        if (RFIDController.mIsInventoryRunning == true && activity != null) {
            requireActivity().runOnUiThread {
                val activity = activity as MainActivity?
                activity?.inventoryStartOrStop()
            }
        }
    }

    override fun handleStatusResponse(results: RFIDResults) {
        requireActivity().runOnUiThread {
            if (results == RFIDResults.RFID_BATCHMODE_IN_PROGRESS) {
            } else if (results != RFIDResults.RFID_API_SUCCESS) {
                RFIDController.mIsInventoryRunning = false
                if (inventoryButton != null) {
                    inventoryButton!!.setBackgroundResource(android.R.drawable.ic_media_play)
                }
                RFIDController.isBatchModeInventoryRunning = false
            }
        }
    }

    /**
     * method to update inventory details on the screen on operation end summary received
     */
    fun updateInventoryDetails() {
        updateTexts()
    }

    /**
     * method to reset inventory operation status on the screen
     */
    fun resetInventoryDetail() {
        if (activity != null) requireActivity().runOnUiThread {
            if (RFIDController.ActiveProfile.id != "1") {
                if (inventoryButton != null && !RFIDController.mIsInventoryRunning &&
                    (RFIDController.isBatchModeInventoryRunning == null || !RFIDController.isBatchModeInventoryRunning)
                ) {
                    inventoryButton!!.setBackgroundResource(android.R.drawable.ic_media_play)
                }
            }
        }
    }

    override fun batchModeEventReceived() {
        batchModeEventReceived = true
        if (inventoryButton != null) inventoryButton!!.setBackgroundResource(android.R.drawable.ic_media_pause)
    }

    override fun handleTagResponse(inventoryListItem: InventoryListItem, isAddedToList: Boolean) {
        updateTexts()
        if (isScan) {
            listInventoryList.add(inventoryListItem.tagID)
            scannedList.add(inventoryListItem.tagID)
            uniqueTags!!.text = Integer.toString(scannedList.size)
        }
    }

    private fun addDataToScanTag() {
        if (pendingInventoryScan!!.isEmpty()) {
        } else {
            val (_, scanID) = pendingInventoryScan!![0]
            //                listInventoryList.add("0000000000000000067D0E4B");
//                listInventoryList.add("30361F8A405EB0D74876E83D");
//                listInventoryList.add("30361F8A400E4E174876E86A");
//                listInventoryList.add("30361F8A40157BD74876E80A");
//                listInventoryList.add("000000000000000006844C51");
//                listInventoryList.add("000000000000000000001271");
//                listInventoryList.add("E2801190200077BCB26B031B");
//                listInventoryList.add("E2801190200068DDB25F0388");
//                listInventoryList.add("E2801190200077BCB26B031A");
//                listInventoryList.add("AD72120544AE85B55D000080");
//                listInventoryList.add("30361F861845CA574876E893");
//                listInventoryList.add("30361F8A405A44174876E818");
//                listInventoryList.add("30361F8A4030EE174876E83F");
////
//                scannedList.add("0000000000000000067D0E4B");
////                scannedList.add("30361F8A405EB0D74876E83D");
//                scannedList.add("30361F8A400E4E174876E86A");
//                scannedList.add("30361F8A40157BD74876E80A");
//                scannedList.add("000000000000000006844C51");
//                scannedList.add("000000000000000000001271");
//                scannedList.add("E2801190200077BCB26B031B");
//                scannedList.add("E2801190200068DDB25F0308");
//                scannedList.add("E2801190200077BCB26B031A");
//                scannedList.add("AD72120544AE85B55D000080");
//                scannedList.add("30361F861845CA574876E893");
//                scannedList.add("30361F8A405A44174876E818");
//                scannedList.add("30361F8A4030EE174876E83F");

            mainCoroutines {
                progressBar!!.visibility = View.VISIBLE
                CoroutineScope(Dispatchers.IO).async {
                    for (inventoryTag in listInventoryList) {
                        //code here
                        val assetMain = Application.bookDao.getRFIDTagDetails(inventoryTag)
                        val scanTag = ScanTag()
                        scanTag.scanId = scanID
                        //  scanTag.setLocationId(locationData.getLocID());
                        var locId = -1
                        var assetId: String? = null
                        if (assetMain != null && assetMain.LocationId != -1) {
                            locId = assetMain.LocationId
                            assetId = assetMain.AssetID
                        }
                        scanTag.locationId = locId
                        scanTag.assetId = assetId
                        scanTag.rfidTag = inventoryTag
                        val getCountOfTagAlreadyScan = Application.bookDao.getCountOfTagAlready(
                            scanTag.rfidTag!!, scanTag.scanId!!
                        )
                        if (getCountOfTagAlreadyScan == 0) {
                            Application.bookDao.addScanTag(scanTag)
                        }
                    }
                }.await()
                CoroutineScope(Dispatchers.Main.immediate).async {
                    showCountFound()
                }.await()
                progressBar!!.visibility = View.GONE
                llBottomParent!!.visibility = View.VISIBLE
                enableUserInteraction(requireActivity())
            }

        }
    }

    private fun updateCountInDb() {
        val inventoryMaster = pendingInventoryScan!![0]
        inventoryMaster.notRegistered = countNotRegistered
    }

    suspend fun showCountFound() {
        val (_, scanID) = pendingInventoryScan!![0]
        itemCountLay12!!.visibility = View.VISIBLE
        llBottomParent!!.visibility = View.VISIBLE
        countNotRegistered = Application.bookDao.getCountNotRegistered(
            scanID
        )
        tvNotRegisteredCount!!.text = countNotRegistered.toString()
        countExistingItem = Application.bookDao.getScanTagTableDataPutAwayExistingItem(
            scanID
        )
        tvExistingItemCount!!.text = countExistingItem.toString()
        countNewItem = Application.bookDao.getScanTagTableDataPutAwayItem(
            scanID
        )
        tvNewItemCount!!.text = countNewItem.toString()
    }

    private fun postAssetSync() {
        showYesNoAlert(
            requireActivity(),
            "Are you sure you want to complete Scan?.",
            object : OnButtonClickListener {
                override fun onPositiveButtonClicked() {
                    progressBar!!.visibility = View.VISIBLE
                    btnInventoryRecord!!.isEnabled = false
                    btnInventoryRecord!!.isClickable = false
                    disableUserInteraction(activity)
                    val mapToLocationApiRequest = PutAwayRequest()
                    //new logic
                    val scanTagList = ArrayList<String?>()
                    val listScan = Application.bookDao.getScanTagTableDataPutAway(
                        currentScanId!!
                    )
                    for (n in listScan) {
                        if (n.rfidTag == null) n.rfidTag = ""
                        scanTagList.add(n.rfidTag)
                    }
                    mapToLocationApiRequest.rfidData = scanTagList
                    mapToLocationApiRequest.scannedBy =
                        Application.sharedPreferences.getString("username", "SYSTEM")
                    Log.e("data", "" + Gson().toJson(mapToLocationApiRequest))
                    //here
                    PutAwayRequestToFirebase("PutAway", requireActivity(), mapToLocationApiRequest)
                    val body: RequestBody = RequestBody.create(
                        "application/json".toMediaTypeOrNull(),
                        Gson().toJson(mapToLocationApiRequest),
                    )
                    inventoryViewModel!!.updateMapLocation(body)
                        .observe(viewLifecycleOwner) { response: Int ->
                            if (response == Constants.SUCCESS) {
                                btnInventoryRecord!!.isEnabled = true
                                btnInventoryRecord!!.isClickable = true
                                Application.isRecordInventory = true
                                val lastItem = pendingInventoryScan!![0]
                                Application.bookDao.deleteScanTagSingle(lastItem.scanID)
                                Application.bookDao.deleteInventorySingle(lastItem.scanID)
                                enableUserInteraction(activity)
                                FancyToast.makeText(
                                    requireActivity(),
                                    getString(R.string.data_sync_success),
                                    FancyToast.LENGTH_SHORT,
                                    FancyToast.SUCCESS,
                                    false
                                ).show()
                                requireActivity().runOnUiThread(object : Runnable {
                                    override fun run() {
                                        Handler((Looper.myLooper())!!).postDelayed(Runnable {
                                            Log.d("final", "postAssetSync: ")
                                            progressBar!!.visibility = View.GONE
                                            val activity: Activity? = activity
                                            if (isAdded && activity != null) {
                                                requireActivity().supportFragmentManager.popBackStackImmediate()
                                            }
                                        }, 500)
                                    }
                                })
                                //                        new CommonAlertDialog(requireActivity(), getString(R.string.data_sync_success), "Ok", "", new CommonAlertDialog.OnButtonClickListener() {
//                            @Override
//                            public void onPositiveButtonClicked() {
//                                requireActivity().getSupportFragmentManager().popBackStackImmediate();
//                            }
//
//                            @Override
//                            public void onNegativeButtonClicked() {
//
//                            }
//                        }).show();
                            } else {
                                Log.d("final", "Failure: ")
                                progressBar!!.visibility = View.GONE
                                btnInventoryRecord!!.isEnabled = true
                                btnInventoryRecord!!.isClickable = true
                                enableUserInteraction(activity)
                                if (response != 401) {
                                    FancyToast.makeText(
                                        requireActivity(),
                                        getString(R.string.error_data_sync),
                                        FancyToast.LENGTH_SHORT,
                                        FancyToast.ERROR,
                                        false
                                    ).show()
                                }
                            }
                        }
                }

                override fun onNegativeButtonClicked() {
                    btnInventoryRecord!!.isEnabled = true
                    btnInventoryRecord!!.isClickable = true
                    progressBar!!.visibility = View.GONE
                }
            })
    }

    var receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val isInventoryStartStop = intent.getBooleanExtra("istart", false)
            if (!isInventoryStartStop) {
                isScan = true
                inventoryButton!!.tag = "0"
                llBottomParent!!.visibility = View.GONE
                inventoryButton!!.setImageResource(android.R.drawable.ic_media_pause)
                listInventoryList = HashSet()

                //new requirement
                itemCountLay12!!.visibility = View.GONE
                updateScanDateAndTime()
                tvILastRecordDate!!.text = scanTime
            } else {
                mainCoroutines {
                    isScan = false
                    inventoryButton!!.tag = "1"
                    inventoryButton!!.setImageResource(android.R.drawable.ic_media_play)
                    disableUserInteraction(requireActivity())
                    addDataToScanTag()
                    updateCountInDb()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        requireActivity().registerReceiver(receiver, IntentFilter("INVENTORYSTART"))
        this.requireView().isFocusableInTouchMode = true
        this.requireView().requestFocus()
        this.requireView().setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                showYesNoAlert(
                    requireActivity(),
                    "Are you sure you want to abandon this scan? Your data will be lost.",
                    object : OnButtonClickListener {
                        override fun onPositiveButtonClicked() {
                            try {
                                if (pendingInventoryScan != null && !pendingInventoryScan!!.isEmpty()) {
                                    val (_, scanID) = pendingInventoryScan!![0]
                                    Application.bookDao.deletemapRFIDLocationAll()
                                    Application.bookDao.deleteScanTagSingle(
                                        scanID
                                    )
                                    Application.bookDao.deleteInventorySingle(
                                        scanID
                                    )
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            requireActivity().supportFragmentManager.popBackStackImmediate()
                        }

                        override fun onNegativeButtonClicked() {}
                    })
                return@OnKeyListener true
            }
            false
        })
    }

    fun enableUserInteraction(requireActivity: FragmentActivity?) {
        requireActivity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun disableUserInteraction(requireActivity: FragmentActivity?) {
        requireActivity!!.window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    override fun onDataSent(isBack: Boolean) {
        Log.d("tag12", "onDataSent: $isBack")
        isFromReconsile = isBack
        inventoryButton!!.tag = "1"
        llBottomParent!!.visibility = View.VISIBLE
        inventoryButton!!.setImageResource(android.R.drawable.ic_media_play)
        mainCoroutines {
            showCountFound()
        }
    }

    companion object {
        fun newInstance(): MapRFIDLocationFragment {
            return MapRFIDLocationFragment()
        }
    }
}