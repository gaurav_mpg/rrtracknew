package com.markss.rfidtemplate.rapidread

import asset.trak.utils.showYesNoAlert
import asset.trak.utils.Constants.isInternetAvailable
import asset.trak.utils.OldInventoryMomentRequestToFirebase
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.ResponseTagHandler
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.TriggerEventHandler
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.BatchModeEventHandler
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.ResponseStatusHandler
import com.google.android.material.floatingactionbutton.FloatingActionButton
import asset.trak.modelsrrtrack.MasterLocation
import androidx.constraintlayout.widget.ConstraintLayout
import asset.trak.database.entity.Inventorymaster
import asset.trak.views.module.InventoryViewModel
import android.os.Bundle
import android.app.Activity
import androidx.lifecycle.ViewModelProvider
import com.markss.rfidtemplate.home.MainActivity
import asset.trak.modelsrrtrack.GetEmailsModel
import asset.trak.utils.CommonAlertDialog.OnButtonClickListener
import com.markss.rfidtemplate.rfid.RFIDController
import asset.trak.views.fragments.HomeFragment
import com.shashank.sony.fancytoastlib.FancyToast
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.zebra.rfid.api3.RFIDResults
import com.markss.rfidtemplate.inventory.InventoryListItem
import asset.trak.database.entity.ScanTag
import asset.trak.modelsrrtrack.OldInventoryMomentRequest
import okhttp3.RequestBody
import com.google.gson.Gson
import android.os.Looper
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import asset.trak.utils.mainCoroutines
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.common.Constants
import com.markss.rfidtemplate.rapidread.OldInventoryMomentFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class OldInventoryMomentFragment : Fragment(), ResponseTagHandler, TriggerEventHandler,
    BatchModeEventHandler, ResponseStatusHandler {
    private var uniqueTags: TextView? = null
    private var inventoryButton: FloatingActionButton? = null
    var batchModeEventReceived = false
    private var locationData: MasterLocation? = null
    private var tvILastRecordDate: TextView? = null
    private var btnInventoryRecord: Button? = null
    private var llBottomParent: LinearLayout? = null
    private var itemCountLay12: CardView? = null
    private var progressBar: ProgressBar? = null
    private var listInventoryList = HashSet<String>()
    private var scannedList = HashSet<String>()
    private var pendingInventoryScan: List<Inventorymaster>? = null
    private var inventoryViewModel: InventoryViewModel? = null
    private var countAlredyRegistered = 0
    private var countNewItem = 0
    private var countNotRegistered = 0
    private var isFromReconsile = false
    private var imgIgnore: View? = null
    private var tvLocation: TextView? = null
    private var currentScanId: String? = null
    private var inventorymasterList: List<Inventorymaster> = ArrayList()
    private var isFirstOpen = true
    private var recipientEmails: Spinner? = null
    var ivBack: ImageView? = null
    private var scanTime = "Scan started on: NA"
    private var scannedTime: String? = null
    private var tvNewItemCount: TextView? = null
    private var tvAlreadyRegisteredTitleCount: TextView? = null
    private var isScan = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_oldinventorymoment, container, false)
        ivBack = view.findViewById(R.id.backButton)
        val title = view.findViewById<TextView>(R.id.pageTitle)
        title.text = getString(R.string.lblRecordInventory)
        imgIgnore = view.findViewById(R.id.closeButton)
        imgIgnore?.setVisibility(View.VISIBLE)
        return view
    }

    override fun onStop() {
        super.onStop()
        isFromReconsile = false
        requireActivity().unregisterReceiver(receiver)
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_rr, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inventoryViewModel = ViewModelProvider(requireActivity()).get(
            InventoryViewModel::class.java
        )
        val mainActivity = activity as MainActivity?
        mainActivity!!.supportActionBar!!.navigationMode = ActionBar.NAVIGATION_MODE_STANDARD
        inventoryButton = mainActivity.findViewById(R.id.rr_inventoryButton)
        uniqueTags = mainActivity.findViewById(R.id.uniqueTagContent)
        tvILastRecordDate = requireActivity().findViewById(R.id.tvILastRecord12)
        btnInventoryRecord = requireActivity().findViewById(R.id.btnInventoryRecord)
        llBottomParent = requireActivity().findViewById(R.id.llBottomParent2)
        progressBar = requireActivity().findViewById(R.id.progressBar3)
        tvLocation = requireActivity().findViewById(R.id.tvLocation211)
        recipientEmails = requireActivity().findViewById(R.id.recipientEmails)
        tvNewItemCount = requireActivity().findViewById(R.id.tvNewItemCount)
        tvAlreadyRegisteredTitleCount =
            requireActivity().findViewById(R.id.tvAlreadyRegisteredTitleCount)
        itemCountLay12 = requireActivity().findViewById(R.id.itemCountLay12)
        locationData = arguments?.getParcelable("LocationData")
        inventoryViewModel!!.getEmails(locationData!!.LocID)
            .observe(requireActivity()) { (data, _, statuscode) ->
                if (statuscode == Constants.SUCCESS) {
                    setSpinner(data as ArrayList<GetEmailsModel.Data>)
                }
            }
        inventorymasterList = Application.bookDao.getPendingInventoryScan(
            locationData!!.LocID
        )
        currentScanId = inventorymasterList[0].scanID
        imgIgnore!!.setOnClickListener { v: View? ->
            showYesNoAlert(
                requireActivity(),
                "Are you sure you want to abandon this scan? Your data will be lost.",
                object : OnButtonClickListener {
                    override fun onPositiveButtonClicked() {
                        Application.isAbandoned = true
                        try {
                            if (pendingInventoryScan != null && !pendingInventoryScan!!.isEmpty()) {
                                val lastItem = pendingInventoryScan!![0]
                                Application.bookDao.deletemapRFIDLocationAll()
                                Application.bookDao.deleteScanTagSingle(lastItem.scanID)
                                Application.bookDao.deleteInventorySingle(lastItem.scanID)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        requireActivity().supportFragmentManager.popBackStackImmediate()
                    }

                    override fun onNegativeButtonClicked() {}
                })
        }
        listInventoryList = HashSet()
        scannedList = HashSet()

//        ExtensionKt.updateListToFirebase("PutAway",requireActivity(),listInventoryList);
        pendingInventoryScan = Application.bookDao.getPendingInventoryScan(
            currentScanId!!
        )
        if (!pendingInventoryScan!!.isEmpty()) {
            val tags = Application.bookDao.getScanRfid(
                pendingInventoryScan!![0].scanID
            )
            for (tag in tags) {
                scannedList.add(tag)
            }
        }
        if (scannedList.size < 10 && scannedList.size > 0) {
            uniqueTags?.setText("0" + scannedList.size)
        } else {
            uniqueTags?.setText(Integer.toString(scannedList.size))
        }
        if (RFIDController.mIsInventoryRunning) {
            inventoryButton?.setBackgroundResource(android.R.drawable.ic_media_pause)
        } else {
            inventoryButton?.setBackgroundResource(android.R.drawable.ic_media_play)
        }
        if (RFIDController.mRRStartedTime == 0L) Application.TAG_READ_RATE =
            0 else Application.TAG_READ_RATE =
            (Application.TOTAL_TAGS / (RFIDController.mRRStartedTime / 1000.toFloat())).toInt()
        if (Application.missedTags > 9999) {
            uniqueTags?.setTextSize(45f)
        }
        tvLocation?.setText(locationData!!.Name)
        updateTexts()
        ivBack!!.setOnClickListener { v: View? ->
            requireActivity().supportFragmentManager.popBackStack(
                null,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            )
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.content_frame, HomeFragment()).commit()
        }
        if (isFromReconsile) {
            inventoryButton?.setTag("1")
            llBottomParent?.setVisibility(View.VISIBLE)
            inventoryButton?.setImageResource(android.R.drawable.ic_media_play)
            showCountFound()
        } else {
            if (isFirstOpen) {
                updateScanDateAndTime()
                inventoryButton?.performClick()
                isFirstOpen = false
            } else {
                inventoryButton?.setTag("1")
                llBottomParent?.setVisibility(View.VISIBLE)
                inventoryButton?.setImageResource(android.R.drawable.ic_media_play)
                showCountFound()
            }
        }

//        inventoryButton.setTag("1");
//        llBottomParent.setVisibility(View.VISIBLE);
//        inventoryButton.setImageResource(android.R.drawable.ic_media_play);
//        addDataToScanTag();
//        showCountFound();
//        updateCountInDb();
        btnInventoryRecord?.setOnClickListener(View.OnClickListener { v: View? ->
            if (isInternetAvailable(requireContext())) {
                if (recipientEmails?.getSelectedItemPosition() == 0) {
                    FancyToast.makeText(
                        requireActivity(),
                        "Please select valid Recipient",
                        FancyToast.LENGTH_LONG,
                        FancyToast.WARNING,
                        false
                    ).show()
                } else {
                    postAssetSync()
                }
            }
        })


        /*inventoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (inventoryButton.getTag().equals("1")) {
                    inventoryButton.setTag("0");
                    llBottomParent.setVisibility(View.GONE);
                    inventoryButton.setImageResource(android.R.drawable.ic_media_pause);
                    listInventoryList = new HashSet<>();
                    //inventoryViewModel.updateTime();
                } else {
                    //inventoryViewModel.stopTime();
                    inventoryButton.setTag("1");
                    llBottomParent.setVisibility(View.VISIBLE);
                    inventoryButton.setImageResource(android.R.drawable.ic_media_play);
                    addDataToScanTag();
                    showCountFound();
                    updateCountInDb();
                }
            }

        });*/tvILastRecordDate?.setText(scanTime)
    }

    private fun setSpinner(getEmailsModel: ArrayList<GetEmailsModel.Data>) {
        getEmailsModel.add(0, GetEmailsModel.Data("-Select-"))

        val adapter: ArrayAdapter<GetEmailsModel.Data> = object :
            ArrayAdapter<GetEmailsModel.Data>(
                requireActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                getEmailsModel!!
            ) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                (view as TextView).text = getEmailsModel!![position].Email
                view.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black))
                view.setTypeface(ResourcesCompat.getFont(requireActivity(), R.font.jio_type_medium))
                return view
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val view = super.getView(position, convertView, parent)
                (view as TextView).text = getEmailsModel!![position].Email
                view.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black))
                view.setTypeface(ResourcesCompat.getFont(requireActivity(), R.font.jio_type_medium))
                return view
            }
        }
        recipientEmails!!.adapter = adapter
    }

    private fun updateScanDateAndTime() {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
        val cal = Calendar.getInstance()
        val dateFormat = sdf.format(cal.time)
        try {
            val latestDate = sdf.parse(dateFormat)
            val changedFormat = SimpleDateFormat("dd-MM-yyyy | hh:mm a")
            val changedFormatApi = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            scannedTime = changedFormatApi.format(latestDate)
            scanTime =
                getString(R.string.last_recorded_02_02_2022_10_43_am) + " " + changedFormat.format(
                    latestDate
                )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun updateTexts() {}
    override fun onDetach() {
        super.onDetach()
        if (RFIDController.mIsInventoryRunning) {
            inventoryButton!!.performClick()
        }
    }

    /**
     * method to reset tags info on the screen before starting inventory operation
     */
    fun resetTagsInfo() {
        updateTexts()
    }

    /**
     * method to start inventory operation on trigger press event received
     */
    override fun triggerPressEventRecieved() {
        if (!RFIDController.mIsInventoryRunning && activity != null) {
            requireActivity().runOnUiThread {
                val activity = activity as MainActivity?
                activity?.inventoryStartOrStop()
            }
        }
    }

    /**
     * method to stop inventory operation on trigger release event received
     */
    override fun triggerReleaseEventRecieved() {
        if (RFIDController.mIsInventoryRunning == true && activity != null) {
            requireActivity().runOnUiThread {
                val activity = activity as MainActivity?
                activity?.inventoryStartOrStop()
            }
        }
    }

    override fun handleStatusResponse(results: RFIDResults) {
        requireActivity().runOnUiThread {
            if (results == RFIDResults.RFID_BATCHMODE_IN_PROGRESS) {
            } else if (results != RFIDResults.RFID_API_SUCCESS) {
                RFIDController.mIsInventoryRunning = false
                if (inventoryButton != null) {
                    inventoryButton!!.setBackgroundResource(android.R.drawable.ic_media_play)
                }
                RFIDController.isBatchModeInventoryRunning = false
            }
        }
    }

    /**
     * method to update inventory details on the screen on operation end summary received
     */
    fun updateInventoryDetails() {
        updateTexts()
    }

    /**
     * method to reset inventory operation status on the screen
     */
    fun resetInventoryDetail() {
        if (activity != null) requireActivity().runOnUiThread {
            if (RFIDController.ActiveProfile.id != "1") {
                if (inventoryButton != null && !RFIDController.mIsInventoryRunning &&
                    (RFIDController.isBatchModeInventoryRunning == null || !RFIDController.isBatchModeInventoryRunning)
                ) {
                    inventoryButton!!.setBackgroundResource(android.R.drawable.ic_media_play)
                }
            }
        }
    }

    override fun batchModeEventReceived() {
        batchModeEventReceived = true
        if (inventoryButton != null) inventoryButton!!.setBackgroundResource(android.R.drawable.ic_media_pause)
    }

    override fun handleTagResponse(inventoryListItem: InventoryListItem, isAddedToList: Boolean) {
        updateTexts()
        if (isScan) {
            listInventoryList.add(inventoryListItem.tagID)
            scannedList.add(inventoryListItem.tagID)
            uniqueTags!!.text = Integer.toString(scannedList.size)
        }
    }

    private fun addDataToScanTag() {
        if (locationData!!.LocID.toString() == null) {
        } else {
            if (pendingInventoryScan!!.isEmpty()) {
            } else {
                val (_, scanID) = pendingInventoryScan!![0]
                //
//                for(int i=2100;i<4500;i++)
//                {
//                    listInventoryList.add("0000000000000000067D0E4B"+i);
//                }

                mainCoroutines {
                    progressBar?.visibility=View.VISIBLE
                    CoroutineScope(Dispatchers.IO).async {
                        for (inventoryTag in listInventoryList) {
                            //code here
                            val scanTag = ScanTag()
                            scanTag.scanId = scanID
                            scanTag.locationId = locationData!!.LocID
                            scanTag.rfidTag = inventoryTag
                            val getCountOfTagAlreadyScan = Application.bookDao.getCountOfTagAlready(
                                scanTag.rfidTag!!, scanTag.scanId!!
                            )
                             if (getCountOfTagAlreadyScan == 0) {
                            Application.bookDao.addScanTag(scanTag)
                            }
                        }
                    }.await()

                    CoroutineScope(Dispatchers.IO).async {
                        showCountFound()
                    }.await()
                    llBottomParent!!.visibility = View.VISIBLE
                    itemCountLay12!!.visibility = View.VISIBLE
                    progressBar?.visibility=View.GONE
                    enableUserInteraction(requireActivity())
                }

            }
        }
    }

    private fun updateCountInDb() {
        val inventoryMaster = pendingInventoryScan!![0]
        inventoryMaster.notRegistered = countNotRegistered
    }

    private fun showCountFound() {
        val (_, scanID) = pendingInventoryScan!![0]
//        llBottomParent!!.visibility = View.VISIBLE
        countNotRegistered = Application.bookDao.getCountNotRegistered(
            scanID
        )
        countAlredyRegistered = Application.bookDao.getAlreadyRegisteredOldInventory(
            scanID
        )
        countNewItem = Application.bookDao.newItemOldInventory(
            scanID
        )
        tvNewItemCount!!.text = countNewItem.toString()
        tvAlreadyRegisteredTitleCount!!.text = countAlredyRegistered.toString()
    }

    private fun postAssetSync() {
        showYesNoAlert(
            requireActivity(),
            "Are you sure you want to complete Scan?.",
            object : OnButtonClickListener {
                override fun onPositiveButtonClicked() {
                    progressBar!!.visibility = View.VISIBLE
                    btnInventoryRecord!!.isEnabled = false
                    btnInventoryRecord!!.isClickable = false
                    disableUserInteraction(activity)
                    val oldInventoryMomentRequestModel = OldInventoryMomentRequest()

                    //new logic
                    val listScan = Application.bookDao.getFoundAtLocation(
                        currentScanId!!
                    )
                    oldInventoryMomentRequestModel.locID = locationData!!.LocID
                    oldInventoryMomentRequestModel.recipent =
                        (recipientEmails!!.selectedItem as GetEmailsModel.Data).Email
                    oldInventoryMomentRequestModel.scanID = currentScanId
                    oldInventoryMomentRequestModel.scannedBy =
                        Application.sharedPreferences.getString("username", "SYSTEM")
                    oldInventoryMomentRequestModel.scanDate = scannedTime
                    if (locationData!!.LocID.toString() == null) {
                        locationData!!.LocID = 0
                    }
                    val rfIdList = ArrayList<String?>()
                    for ((_, _, _, rfidTag) in listScan) {
                        rfIdList.add(rfidTag)
                    }
                    oldInventoryMomentRequestModel.rfidData = rfIdList
                    val body: RequestBody = RequestBody.create(
                        "application/json".toMediaTypeOrNull(),
                        Gson().toJson(oldInventoryMomentRequestModel)
                    )
                    Log.e("data", "" + Gson().toJson(oldInventoryMomentRequestModel))
                    OldInventoryMomentRequestToFirebase(
                        "OldInventory",
                        requireActivity(),
                        oldInventoryMomentRequestModel
                    )
                    inventoryViewModel!!.postOldInventory(body)
                        .observe(viewLifecycleOwner) { response: Int ->
                            if (response == Constants.SUCCESS) {
                                btnInventoryRecord!!.isEnabled = true
                                btnInventoryRecord!!.isClickable = true
                                Application.isRecordInventory = true
                                val lastItem = pendingInventoryScan!![0]
                                Application.bookDao.deleteScanTagSingle(lastItem.scanID)
                                Application.bookDao.deleteInventorySingle(lastItem.scanID)
                                enableUserInteraction(activity)
                                FancyToast.makeText(
                                    requireActivity(),
                                    getString(R.string.data_sync_success),
                                    FancyToast.LENGTH_SHORT,
                                    FancyToast.SUCCESS,
                                    false
                                ).show()
                                requireActivity().runOnUiThread(object : Runnable {
                                    override fun run() {
                                        Handler((Looper.myLooper())!!).postDelayed(Runnable {
                                            Log.d("final", "postAssetSync: ")
                                            progressBar!!.visibility = View.GONE
                                            val activity: Activity? = activity
                                            if (isAdded && activity != null) {
                                                requireActivity().supportFragmentManager.popBackStackImmediate()
                                            }
                                        }, 500)
                                    }
                                })
                            } else {
                                Log.d("final", "Failure: ")
                                progressBar!!.visibility = View.GONE
                                btnInventoryRecord!!.isEnabled = true
                                btnInventoryRecord!!.isClickable = true
                                enableUserInteraction(activity)
                                if (response != 401) {
                                    FancyToast.makeText(
                                        requireActivity(),
                                        getString(R.string.error_data_sync),
                                        FancyToast.LENGTH_SHORT,
                                        FancyToast.ERROR,
                                        false
                                    ).show()
                                }
                            }
                        }
                }

                override fun onNegativeButtonClicked() {
                    btnInventoryRecord!!.isEnabled = true
                    btnInventoryRecord!!.isClickable = true
                    progressBar!!.visibility = View.GONE
                }
            })
    }

    var receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val isInventoryStartStop = intent.getBooleanExtra("istart", false)
            if (!isInventoryStartStop) {
                isScan = true
                inventoryButton!!.tag = "0"
                llBottomParent!!.visibility = View.GONE
                itemCountLay12!!.visibility = View.GONE
                inventoryButton!!.setImageResource(android.R.drawable.ic_media_pause)
                listInventoryList = HashSet()
                updateScanDateAndTime()
                tvILastRecordDate!!.text = scanTime
            } else {
                mainCoroutines {
                 //   progressBar!!.visibility = View.VISIBLE
                        isScan = false
                        inventoryButton!!.tag = "1"
                        inventoryButton!!.setImageResource(android.R.drawable.ic_media_play)
                        disableUserInteraction(requireActivity())
                        addDataToScanTag()
                        updateCountInDb()
                   // progressBar!!.visibility = View.GONE

                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        requireActivity().registerReceiver(receiver, IntentFilter("INVENTORYSTART"))
        this.requireView().isFocusableInTouchMode = true
        this.requireView().requestFocus()
        this.requireView().setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                showYesNoAlert(
                    requireActivity(),
                    "Are you sure you want to abandon this scan? Your data will be lost.",
                    object : OnButtonClickListener {
                        override fun onPositiveButtonClicked() {
                            try {
                                if (pendingInventoryScan != null && !pendingInventoryScan!!.isEmpty()) {
                                    val (_, scanID) = pendingInventoryScan!![0]
                                    Application.bookDao.deletemapRFIDLocationAll()
                                    Application.bookDao.deleteScanTagSingle(
                                        scanID
                                    )
                                    Application.bookDao.deleteInventorySingle(
                                        scanID
                                    )
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            requireActivity().supportFragmentManager.popBackStackImmediate()
                        }

                        override fun onNegativeButtonClicked() {}
                    })
                return@OnKeyListener true
            }
            false
        })
    }

    fun enableUserInteraction(requireActivity: FragmentActivity?) {
        requireActivity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun disableUserInteraction(requireActivity: FragmentActivity?) {
        requireActivity!!.window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    companion object {
        fun newInstance(): OldInventoryMomentFragment {
            return OldInventoryMomentFragment()
        }
    }
}