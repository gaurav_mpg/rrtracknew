package com.markss.rfidtemplate.rapidread

import asset.trak.views.inventory.ReconcileAssetsFragment.Companion.setFragmentCallback
import asset.trak.utils.Constants.isInternetAvailable
import asset.trak.utils.RapidFirebase
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.ResponseTagHandler
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.TriggerEventHandler
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.BatchModeEventHandler
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.ResponseStatusHandler
import asset.trak.views.listener.RapidReadCallback
import com.google.android.material.floatingactionbutton.FloatingActionButton
import asset.trak.modelsrrtrack.MasterLocation
import androidx.constraintlayout.widget.ConstraintLayout
import asset.trak.database.entity.Inventorymaster
import asset.trak.views.module.InventoryViewModel
import android.os.Bundle
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import com.markss.rfidtemplate.home.MainActivity
import asset.trak.utils.CommonAlertDialog
import asset.trak.utils.CommonAlertDialog.OnButtonClickListener
import asset.trak.views.inventory.ReconcileAssetsFragment
import android.text.TextUtils
import com.markss.rfidtemplate.rfid.RFIDController
import com.zebra.rfid.api3.RFIDResults
import com.markss.rfidtemplate.inventory.InventoryListItem
import asset.trak.modelsrrtrack.AssetMain
import asset.trak.database.entity.ScanTag
import com.shashank.sony.fancytoastlib.FancyToast
import asset.trak.model.AssetSyncRequestDataModel
import okhttp3.RequestBody
import com.google.gson.Gson
import android.os.Looper
import android.content.IntentFilter
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import asset.trak.modelsrrtrack.AssetData
import asset.trak.utils.mainCoroutines
import asset.trak.views.fragments.HomeFragment
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.common.Constants
import com.markss.rfidtemplate.rapidread.RapidReadFragment
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.lang.Exception
import java.lang.Runnable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

class RapidReadFragment : Fragment(), ResponseTagHandler, TriggerEventHandler,
    BatchModeEventHandler, ResponseStatusHandler, RapidReadCallback {
    private var uniqueTags: TextView? = null
    private var inventoryButton: FloatingActionButton? = null
    private val btnScanpause: FloatingActionButton? = null
    var batchModeEventReceived = false
    private var locationData: MasterLocation? = null
    private var foundLocParent: CardView? = null
    private var foundForDifferentParent: CardView? = null
    private var tvFoundLocCount: TextView? = null
    private var tvNotFoundLocCount: TextView? = null
    private var tvDiffLocationCount: TextView? = null
    private var tvNotRegisteredCount: TextView? = null
    private var tvILastRecordDate: TextView? = null
    private var timeValue: TextView? = null
    private var btnReconcile: Button? = null
    private var btnInventoryRecord: Button? = null
    private var llBottomParent: LinearLayout? = null
    private var progressbar: ProgressBar? = null
    private var listInventoryList = HashSet<String>()
    private var scannedList = HashSet<String>()
    private var pendingInventoryScan: List<Inventorymaster>? = null
    private var inventoryViewModel: InventoryViewModel? = null
    private var totalRegisteredCount = 0
    private var countFoundCurrentLocation = 0
    private var countNotFoundCurrentLocation = 0
    private var countFoundDifferentLoc = 0
    private var countNotRegistered = 0
    private var isFromReconsile = false
    private val whichInventory = ""
    private var imgIgnore: View? = null
    private var tvRegisteredCount: TextView? = null
    private var tvLocation: TextView? = null
    private var inventorymasterList: List<Inventorymaster> = ArrayList()
    private var currentScanId: String? = null
    var ivBack: ImageView? = null
    private var isScan = true
    private var firstOpen = true
    private var scanTime = "Scan started on: NA"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_rr, container, false)
        ivBack = view.findViewById(R.id.backButton)
        val title = view.findViewById<TextView>(R.id.pageTitle)
        title.text = getString(R.string.lblRecordInventory)
        imgIgnore = view.findViewById(R.id.closeButton)
        imgIgnore?.setVisibility(View.VISIBLE)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_rr, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }

    var receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val isInventoryStartStop = intent.getBooleanExtra("istart", false)
            if (!isInventoryStartStop) {
                isScan = true
                inventoryButton!!.tag = "0"
                llBottomParent!!.visibility = View.GONE
                inventoryButton!!.setImageResource(android.R.drawable.ic_media_pause)
                listInventoryList = HashSet()

                //new requirement
                foundLocParent!!.visibility = View.GONE
                foundForDifferentParent!!.visibility = View.GONE
                inventoryViewModel!!.updateTime()
                updateScanDateAndTime()
                tvILastRecordDate!!.text = scanTime
            } else {
                        isScan = false
                        inventoryViewModel!!.stopTime()
                        inventoryButton!!.tag = "1"
                        inventoryButton!!.setImageResource(android.R.drawable.ic_media_play)
                        disableUserInteraction(requireActivity())
                        addDataToScanTag()
                      //  showCountFound()
                        updateCountInDb()
            }
        }
    }

    private fun updateScanDateAndTime() {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
        val cal = Calendar.getInstance()
        val dateFormat = sdf.format(cal.time)
        try {
            val latestDate = sdf.parse(dateFormat)
            val changedFormat = SimpleDateFormat("dd-MM-yyyy | hh:mm a")
            scanTime =
                getString(R.string.last_recorded_02_02_2022_10_43_am) + " " + changedFormat.format(
                    latestDate
                )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inventoryViewModel = ViewModelProvider(requireActivity()).get(
            InventoryViewModel::class.java
        )
        val mainActivity = activity as MainActivity?
        mainActivity!!.supportActionBar!!.navigationMode = ActionBar.NAVIGATION_MODE_STANDARD
        inventoryButton = mainActivity.findViewById(R.id.rr_inventoryButton)
        uniqueTags = mainActivity.findViewById(R.id.uniqueTagContent)
        foundLocParent = requireActivity().findViewById(R.id.foundLocParent)
        foundForDifferentParent = requireActivity().findViewById(R.id.foundForDifferentParent)
        tvFoundLocCount = requireActivity().findViewById(R.id.tvFoundLocCount)
        tvNotFoundLocCount = requireActivity().findViewById(R.id.tvNotFoundLocCount)
        tvDiffLocationCount = requireActivity().findViewById(R.id.tvDiffLocationCount)
        tvNotRegisteredCount = requireActivity().findViewById(R.id.tvNotRegisteredCount)
        tvILastRecordDate = requireActivity().findViewById(R.id.tvILastRecord12)
        timeValue = requireActivity().findViewById(R.id.timeValue)
        btnReconcile = requireActivity().findViewById(R.id.btnReconcile)
        btnInventoryRecord = requireActivity().findViewById(R.id.btnInventoryRecord)
        llBottomParent = requireActivity().findViewById(R.id.llBottomParent2)
        progressbar = requireActivity().findViewById(R.id.progressBar114)
        tvRegisteredCount = requireActivity().findViewById(R.id.tvRegisteredCountrr)
        tvLocation = requireActivity().findViewById(R.id.tvLocation12)
        locationData = arguments?.getParcelable("LocationData")
        totalRegisteredCount = arguments?.getInt("totalRegistered") ?: 0
        lifecycleScope.launch {
            inventorymasterList = CoroutineScope(Dispatchers.IO).async {  Application.bookDao.getPendingInventoryScanKt(
                locationData!!.LocID
            )}.await()
            currentScanId = inventorymasterList[0].scanID
            var countValue = ""
            countValue = CoroutineScope(Dispatchers.IO).async {  if (isFromReconsile) {
                Application.bookDao.getCountLocationId(
                    locationData!!.LocID, currentScanId!!
                ).toString()
            } else {
                Application.bookDao.getCountLocationIdAsset(
                    locationData!!.LocID
                ).toString()
            }}.await()
            tvRegisteredCount?.setText(countValue)
            inventoryViewModel!!.timerVal.observe(viewLifecycleOwner) { s -> timeValue?.setText(s) }
            inventoryViewModel!!.isStart.observe(viewLifecycleOwner) { aBoolean ->
                if (!aBoolean!!) {
                    inventoryViewModel!!.updateTime()
                }
            }
            listInventoryList = HashSet()
            scannedList = HashSet()
            pendingInventoryScan = CoroutineScope(Dispatchers.IO).async { Application.bookDao.getPendingInventoryScanJava(
                currentScanId!!
            )}.await()
            setFragmentCallback(this@RapidReadFragment)
            if (!pendingInventoryScan!!.isEmpty()) {
                val tags = Application.bookDao.getScanRfid(
                    pendingInventoryScan!![0].scanID
                )
                for (tag in tags) {
                    scannedList.add(tag)
                }
                //format and set the date here
                if (pendingInventoryScan!![0].scanOn == null || TextUtils.isEmpty(
                        pendingInventoryScan!![0].scanOn
                    )
                ) {
//                tvILastRecordDate.setText(getString(R.string.last_recorded_02_02_2022_10_43_am) + " NA ");
                } else {
                    val formater = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    val changedFormat = SimpleDateFormat("dd-MM-yyyy | hh:mm a")
                    try {
                        val latestDate = formater.parse(pendingInventoryScan!![0].scanOn)
                        tvILastRecordDate?.setText(
                            getString(R.string.last_recorded_02_02_2022_10_43_am) + " " + changedFormat.format(
                                latestDate
                            )
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
            if (scannedList.size < 10 && scannedList.size > 0) {
                uniqueTags?.setText("0" + scannedList.size)
            } else {
                uniqueTags?.setText(Integer.toString(scannedList.size))
            }
            if (RFIDController.mIsInventoryRunning) {
                inventoryButton?.setBackgroundResource(android.R.drawable.ic_media_pause)
            } else {
                inventoryButton?.setBackgroundResource(android.R.drawable.ic_media_play)
            }
            if (RFIDController.mRRStartedTime == 0L) Application.TAG_READ_RATE =
                0 else Application.TAG_READ_RATE =
                (Application.TOTAL_TAGS / (RFIDController.mRRStartedTime / 1000.toFloat())).toInt()
            if (Application.missedTags > 9999) {
                //orignal size is 60sp - reduced size 45sp
                uniqueTags?.setTextSize(45f)
            }
            tvLocation?.setText(locationData!!.Name)
            updateTexts()
        }

        imgIgnore!!.setOnClickListener { v: View? ->
            CommonAlertDialog(
                requireActivity(),
                "Are you sure you want to abandon this scan? Your data will be lost.",
                "Yes",
                "No",
                object : OnButtonClickListener {
                    override fun onPositiveButtonClicked() {
                        Application.isAbandoned = true
                        if (Application.isReconsiled) {
                            try {
                                if (pendingInventoryScan != null && !pendingInventoryScan!!.isEmpty()) {
                                    val lastItem = pendingInventoryScan!![0]
                                    Application.bookDao.deleteScanTagSingle(lastItem.scanID)
                                    Application.bookDao.deleteInventorySingle(lastItem.scanID)
                                    //sync api call
                                    requireActivity().supportFragmentManager.popBackStackImmediate()
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        } else {
                            val lastItem = pendingInventoryScan!![0]
                            Application.bookDao.deleteScanTagSingle(lastItem.scanID)
                            Application.bookDao.deleteInventorySingle(lastItem.scanID)
                            requireActivity().supportFragmentManager.popBackStackImmediate()
                        }
                    }

                    override fun onNegativeButtonClicked() {}
                }).show()
        }

        ivBack!!.setOnClickListener { v: View? ->
            //   requireActivity().getSupportFragmentManager().popBackStackImmediate();
            requireActivity().supportFragmentManager.popBackStack(
                null,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            )
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.content_frame, HomeFragment()).commit()
        }


        /*inventoryButton.setOnClickListener(v -> {
            if (inventoryButton.getTag().equals("1")) {
                //new requirement
                inventoryButton.setTag("0");
                llBottomParent.setVisibility(View.GONE);
                inventoryButton.setImageResource(android.R.drawable.ic_media_pause);
                listInventoryList = new HashSet<>();

                //new requirement
                foundLocParent.setVisibility(View.GONE);
                foundForDifferentParent.setVisibility(View.GONE);
                inventoryViewModel.updateTime();

            } else {

                inventoryViewModel.stopTime();
                inventoryButton.setTag("1");
                llBottomParent.setVisibility(View.VISIBLE);
                inventoryButton.setImageResource(android.R.drawable.ic_media_play);
                addDataToScanTag();
                showCountFound();
                updateCountInDb();
            }
        });*/
        btnReconcile?.setOnClickListener(View.OnClickListener { v: View? ->
            val bundle = Bundle()
            bundle.putInt("locationId", locationData!!.LocID)
            bundle.putString("scanId", currentScanId)
            bundle.putParcelable("LocationData", locationData)
            val fragInfo = ReconcileAssetsFragment()
            fragInfo.arguments = bundle
            requireActivity().supportFragmentManager.beginTransaction().replace(
                R.id.content_frame,
                fragInfo, MainActivity.TAG_CONTENT_FRAGMENT
            ).addToBackStack(null).commit()
        })
        if (isFromReconsile) {
            inventoryButton?.setTag("1")
            llBottomParent?.setVisibility(View.VISIBLE)
            inventoryButton?.setImageResource(android.R.drawable.ic_media_play)
            //  addDataToScanTag();
                showCountFound()
        } else {
            if (firstOpen) {
                updateScanDateAndTime()
                inventoryButton?.performClick()
                firstOpen = false
            } else {
                inventoryViewModel!!.stopTime()
                inventoryButton?.setTag("1")
                llBottomParent?.setVisibility(View.VISIBLE)
                inventoryButton?.setImageResource(android.R.drawable.ic_media_play)
                    showCountFound()

            }
        }
        tvILastRecordDate?.setText(scanTime)
    }

    fun updateTexts() {}
    override fun onDetach() {
        super.onDetach()
        inventoryViewModel!!.resetTimer()
        Application.isScanRestart = true
        if (RFIDController.mIsInventoryRunning) {
            inventoryButton!!.performClick()
        }
    }

    /**
     * method to reset tags info on the screen before starting inventory operation
     */
    fun resetTagsInfo() {
        updateTexts()
    }

    /**
     * method to start inventory operation on trigger press event received
     */
    override fun triggerPressEventRecieved() {
        if (!RFIDController.mIsInventoryRunning && activity != null) {
            requireActivity().runOnUiThread {
                val activity = activity as MainActivity?
                activity?.inventoryStartOrStop()
            }
        }
    }

    /**
     * method to stop inventory operation on trigger release event received
     */
    override fun triggerReleaseEventRecieved() {
        if (RFIDController.mIsInventoryRunning == true && activity != null) {
            //RFIDController.mInventoryStartPending = false;
            requireActivity().runOnUiThread {
                val activity = activity as MainActivity?
                activity?.inventoryStartOrStop()
            }
        }
    }

    override fun handleStatusResponse(results: RFIDResults) {
        requireActivity().runOnUiThread {
            if (results == RFIDResults.RFID_BATCHMODE_IN_PROGRESS) {
            } else if (results != RFIDResults.RFID_API_SUCCESS) {
                RFIDController.mIsInventoryRunning = false
                if (inventoryButton != null) {
                    inventoryButton!!.setBackgroundResource(android.R.drawable.ic_media_play)
                }
                RFIDController.isBatchModeInventoryRunning = false
            }
        }
    }

    /**
     * method to update inventory details on the screen on operation end summary received
     */
    fun updateInventoryDetails() {
        updateTexts()
    }

    /**
     * method to reset inventory operation status on the screen
     */
    fun resetInventoryDetail() {
        if (activity != null) requireActivity().runOnUiThread {
            if (RFIDController.ActiveProfile.id != "1") {
                if (inventoryButton != null && !RFIDController.mIsInventoryRunning &&
                    (RFIDController.isBatchModeInventoryRunning == null || !RFIDController.isBatchModeInventoryRunning)
                ) {
                    inventoryButton!!.setBackgroundResource(android.R.drawable.ic_media_play)
                }
            }
        }
    }

    override fun batchModeEventReceived() {
        batchModeEventReceived = true
        if (inventoryButton != null) inventoryButton!!.setBackgroundResource(android.R.drawable.ic_media_pause)
    }

    override fun handleTagResponse(inventoryListItem: InventoryListItem, isAddedToList: Boolean) {
        updateTexts()
        if (isScan) {
            listInventoryList.add(inventoryListItem.tagID)
            scannedList.add(inventoryListItem.tagID)
            uniqueTags!!.text = Integer.toString(scannedList.size)
        }
        //  String memoryBankData=inventoryListItem.getMemoryBankData();
    }

     fun addDataToScanTag() {

                if (pendingInventoryScan!!.isEmpty()) {
                } else {
                    val (_, scanID) = pendingInventoryScan!![0]

//                //liveData
//              listInventoryList.add("30361F89D45F85174876E80D")
//                               listInventoryList.add("30361F8BAC0475D74876E853");
//               listInventoryList.add("30361F87A02D02574876E80C");
//            listInventoryList.add("30361F8BAC3D46D74876E804");
//                    listInventoryList.add("30361F8BAC4242574876E871");
//               listInventoryList.add("30361F8BAC4275D74876E825");
//               listInventoryList.add("30361F8A405189174876E807");
//                listInventoryList.add("30361F88BC2098174876E811");
//
//
//                //scan
//                scannedList.add("30361F89D4581A574876E916")
//                    scannedList.add("30361F89D45F85174876E80D")
//                    scannedList.add("30361F8BAC0475D74876E853");
//                    scannedList.add("30361F87A02D02574876E80C");
//                    scannedList.add("30361F8BAC3D46D74876E804");
//                    scannedList.add("30361F8BAC4242574876E871");
//                    scannedList.add("30361F8BAC4275D74876E825");
//                    scannedList.add("30361F8A405189174876E807");
//                scannedList.add("30361F88BC2074D74876E880");
//                scannedList.add("30361F88BC5C0B174876E85B");
//                scannedList.add("30361F8A402D3D574876E8C3");
//                scannedList.add("30361F8A402A43D74876E877");
//                scannedList.add("30361F88BC2098174876E808");
//                scannedList.add("30361F88BC2098174876E811");
                    lifecycleScope.launch {(Dispatchers.Main)
                        progressbar?.visibility = View.VISIBLE

                        for (inventoryTag in listInventoryList) {
                            //code here
                            val assetMain =  CoroutineScope(Dispatchers.IO).async { Application.bookDao.getRFIDTagDetailsJava(inventoryTag)}.await()
                            val scanTag = ScanTag()
                            scanTag.scanId = scanID
                            var locId = -1
                            var assetId: String? = null
                            if (assetMain != null && assetMain.LocationId != -1) {
                                locId = assetMain.LocationId
                                assetId = assetMain.AssetID
                            }
                            scanTag.locationId = locId
                            scanTag.assetId = assetId
                            scanTag.rfidTag = inventoryTag
                            val getCountOfTagAlready = CoroutineScope(Dispatchers.IO).async { Application.bookDao.getCountOfTagAlreadyJava(
                                scanTag.rfidTag!!, scanTag.scanId!!
                            )}.await()
                            if (getCountOfTagAlready == 0) {
                                CoroutineScope(Dispatchers.IO).async {
                                    Application.bookDao.addScanTag(scanTag)
                                }.await()
                           }
                            val count = CoroutineScope(Dispatchers.IO).async {Application.bookDao.getCountOfTagPreviouslyNotFoundJava(
                                inventoryTag,
                                scanTag.scanId!!
                            )}.await()
                            if (count > 0) {
                                CoroutineScope(Dispatchers.IO).async {
                                    Application.bookDao.updateRfTagPreviouslyNotFoundJava(
                                        inventoryTag,
                                        scanTag.scanId!!,
                                        locationData!!.LocID
                                    )
                                }.await()
                            }
                        }
                        val notInScanTable = CoroutineScope(Dispatchers.IO).async{
                            Application.bookDao.getAssetNotFoundTagsInScanTableJava(locationData!!.LocID)}.await()
                        for ((AssetID, AssetRFID) in notInScanTable) {
                            val scanTag = ScanTag()
                            scanTag.scanId = scanID
                            val locId = -2
                            //  String assetId="NF";
                            //  scanTag.setLocationStatus("NF");
                            scanTag.locationId = locId
                            scanTag.assetId = AssetID
                            scanTag.rfidTag = AssetRFID
                            val getCountOfTagAlready = CoroutineScope(Dispatchers.IO).async {
                                Application.bookDao.getCountOfTagAlreadyJava(
                                    scanTag.rfidTag!!, scanTag.scanId!!
                                )
                            }.await()
                            if (getCountOfTagAlready == 0) {
                                CoroutineScope(Dispatchers.IO).async {
                                    Application.bookDao.addScanTagJava(scanTag)
                                }.await()
                            }
                        }
                    CoroutineScope(Dispatchers.IO).async {
                        showCountFound()
                    }.await()

                }

        }

    }

     fun updateCountInDb() {
         lifecycleScope.launch(Dispatchers.Main) {
             progressbar?.visibility=View.VISIBLE
             val inventoryMaster = pendingInventoryScan!![0]
             inventoryMaster.foundOnLocation = countFoundCurrentLocation
             inventoryMaster.notFound = countNotFoundCurrentLocation
             inventoryMaster.foundOfDiffLocation = countFoundDifferentLoc
             inventoryMaster.notRegistered = countNotRegistered

             inventoryMaster.registered = CoroutineScope(Dispatchers.IO).async {
                 Application.bookDao.getCountLocationId(
                     locationData!!.LocID, currentScanId!!
                 )
             }.await()

             //inventoryMaster.setStatus(asset.trak.utils.Constants.InventoryStatus.COMPLETED);
             val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
             val cal = Calendar.getInstance()
             val dateFormat = sdf.format(cal.time)
             inventoryMaster.scanOn = dateFormat
             CoroutineScope(Dispatchers.IO).async {
                 Application.bookDao.updateInventoryItemJava(inventoryMaster)
             }.await()

         }
    }

     fun showCountFound() {
      //  progressBar?.visibility = View.GONE
        if (locationData!!.LocID.toString() == null) {
            tvFoundLocCount!!.text = countFoundCurrentLocation.toString()
            tvNotFoundLocCount!!.text = countNotFoundCurrentLocation.toString()
            tvDiffLocationCount!!.text = countFoundDifferentLoc.toString()
            tvNotRegisteredCount!!.text = countNotRegistered.toString()
        } else {
            val (_, scanID) = pendingInventoryScan!![0]

            // Log.d("tag116", "showCountFound: "+(inventoryMaster.getScanID());

            /*Get Count*/
            lifecycleScope.launch(Dispatchers.Main) {
                countFoundCurrentLocation =   CoroutineScope(Dispatchers.IO).async {
                    Application.bookDao.getCountOfTagsFoundJava(
                        locationData!!.LocID, scanID
                    )}.await()
                    countNotFoundCurrentLocation =CoroutineScope(Dispatchers.IO).async { Application.bookDao.getCountOfTagsNotFoundJava(
                        scanID
                    )}.await()
                    countFoundDifferentLoc =CoroutineScope(Dispatchers.IO).async { Application.bookDao.getCountFoundDifferentLocJava(
                        locationData!!.LocID, scanID
                    )}.await()
                    countNotRegistered =CoroutineScope(Dispatchers.IO).async { Application.bookDao.getCountNotRegisteredJava(
                        scanID
                    )}.await()

                tvFoundLocCount!!.text = countFoundCurrentLocation.toString()
                tvNotFoundLocCount!!.text = countNotFoundCurrentLocation.toString()
                tvDiffLocationCount!!.text = countFoundDifferentLoc.toString()
                tvNotRegisteredCount!!.text = countNotRegistered.toString()
                foundLocParent?.visibility = View.VISIBLE
                foundForDifferentParent?.visibility = View.VISIBLE
                llBottomParent!!.visibility = View.VISIBLE
                enableUserInteraction(requireActivity())
                progressbar?.visibility=View.GONE
            }

            //    List<AssetMain> bookAndAssetData = bookDao.getFoundAtLocation(inventoryMaster.getScanID(), locationData.getLocID());

        }
        btnInventoryRecord!!.setOnClickListener { v: View? ->
            Log.d(
                "RapidRead",
                "$countNotFoundCurrentLocation $countFoundDifferentLoc $countFoundDifferentLoc"
            )
            if (isInternetAvailable(requireContext())) {
                if ((countNotFoundCurrentLocation == 0) && (countFoundDifferentLoc == 0) && (countNotRegistered == 0)) {
//                    progressBar.setVisibility(View.VISIBLE);
                    btnInventoryRecord!!.isEnabled = false
                    btnInventoryRecord!!.isClickable = false
                    val pendingInventoryScan = Application.bookDao.getPendingInventoryScan(
                        (currentScanId)!!
                    )
                    val inventoryMaster = pendingInventoryScan[0]
                    Application.bookDao.updateScanTagLocation(inventoryMaster.scanID)
                    postAssetSync()
                } else {
                    FancyToast.makeText(
                        requireActivity(),
                        "Please reconcile asset(s) to proceed.",
                        FancyToast.LENGTH_LONG,
                        FancyToast.WARNING,
                        false
                    ).show()
                }
            }
        }
    }

    private fun postAssetSync() {
        CommonAlertDialog(
            requireActivity(),
            "Are you sure you want to complete Scan?.",
            "Yes",
            "No",
            object : OnButtonClickListener {
                override fun onPositiveButtonClicked() {
                    //isReconsiled = false;
                    inventoryViewModel!!.isFirstTime = true
                    progressbar!!.visibility = View.VISIBLE
                    disableUserInteraction(activity)
                    val bookAndAssetData: MutableList<ScanTag> = ArrayList()
                    val pendingInventoryScan = Application.bookDao.getPendingInventoryScan(
                        currentScanId!!
                    )
                    val inventoryMaster = pendingInventoryScan[0]
                    bookAndAssetData.addAll(
                        Application.bookDao.getFoundAtLocationZonal(
                            inventoryMaster.scanID,
                            locationData!!.LocID
                        )
                    )
                    val assetSyncRequestDataModel = AssetSyncRequestDataModel()
                    val changedFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    var scanEndTime: String? = ""
                    try {
                        //   String currentDate = changedFormat.format(new Date());
                        scanEndTime = changedFormat.format(Date())
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

//                if (String.valueOf(locationData.getLocID()) == null) {
//                    locationData.setLocID(0);
//                }

                    //      List<AssetMain> list= bookDao.selectAssetMainLocationNullRecords(inventoryMaster.getScanID(),0);
                    assetSyncRequestDataModel.inventoryData.deviceID = inventoryMaster.deviceId
                    assetSyncRequestDataModel.inventoryData.foundForLoc =
                        inventoryMaster.foundOnLocation!!
                    assetSyncRequestDataModel.inventoryData.foundForOtherLoc =
                        inventoryMaster.foundOfDiffLocation!!
                    assetSyncRequestDataModel.inventoryData.noofAssetsScanned = scannedList.size
                    assetSyncRequestDataModel.inventoryData.scanDate =
                        inventoryMaster.scanStartDatetime
                    assetSyncRequestDataModel.inventoryData.locID = inventoryMaster.locationId!!
                    assetSyncRequestDataModel.inventoryData.locType = "Z"
                    assetSyncRequestDataModel.inventoryData.scanStartDatetime =
                        inventoryMaster.scanOn
                    assetSyncRequestDataModel.inventoryData.scanEndDatetime = scanEndTime
                    assetSyncRequestDataModel.inventoryData.notRegistered =
                        tvRegisteredCount!!.text.toString().toInt()
                    assetSyncRequestDataModel.inventoryData.scanID = inventoryMaster.scanID
                    assetSyncRequestDataModel.inventoryData.scannedBy =
                        Application.sharedPreferences.getString("username", "SYSTEM")
                    assetSyncRequestDataModel.inventoryData.notRegistered =
                        tvNotRegisteredCount!!.text.toString().toInt()

                    // Log.d("tag111", "onClick: " + inventoryMaster.getScanID() + " " + locationData.getLocID());
                    for (n in bookAndAssetData) {
                        val scanTag = AssetData()
                        // sending ID in rfidTag field, need to update attribute name accordingly in API
                        scanTag.assetRFID = n.rfidTag
                        if (n.scanId == null) {
                            n.scanId = "0"
                        }
                        scanTag.assetID = n.assetId
                        scanTag.locID = n.locationId!!
                        assetSyncRequestDataModel.assetData.add(scanTag)
                    }
                    RapidFirebase("Rapid", requireActivity(), assetSyncRequestDataModel)
                    val body: RequestBody = RequestBody.create(
                        "application/json".toMediaTypeOrNull(),
                        Gson().toJson(assetSyncRequestDataModel)
                    )
                    Log.e("data", "" + Gson().toJson(assetSyncRequestDataModel))
                    inventoryViewModel!!.postAssetSync(body)
                        .observe(viewLifecycleOwner) { response: Int ->
                            if (response == Constants.SUCCESS) {
                                Log.d("final", "postAssetSync: ")
                                btnInventoryRecord!!.isEnabled = true
                                btnInventoryRecord!!.isClickable = true
                                inventoryMaster.status =
                                    asset.trak.utils.Constants.InventoryStatus.COMPLETED
                                Application.bookDao.updateInventoryItem(inventoryMaster)
                                Application.isRecordInventory = true
                                Application.bookDao.deleteTblScanTagTableJava()
                                enableUserInteraction(activity)
                                FancyToast.makeText(
                                    requireActivity(),
                                    getString(R.string.data_sync_success),
                                    FancyToast.LENGTH_SHORT,
                                    FancyToast.SUCCESS,
                                    false
                                ).show()
                                requireActivity().runOnUiThread(object : Runnable {
                                    override fun run() {
                                        Handler((Looper.myLooper())!!).postDelayed(Runnable {
                                            progressbar!!.visibility = View.GONE
                                            val activity: Activity? = activity
                                            if (isAdded && activity != null) {
                                                requireActivity().supportFragmentManager.popBackStackImmediate()
                                            }
                                        }, 500)
                                    }
                                })
                                //                        new CommonAlertDialog(requireActivity(), getString(R.string.data_sync_success), "Ok", "", new CommonAlertDialog.OnButtonClickListener() {
//                            @Override
//                            public void onPositiveButtonClicked() {
//                                requireActivity().getSupportFragmentManager().popBackStackImmediate();
//                            }
//
//                            @Override
//                            public void onNegativeButtonClicked() {
//
//                            }
//                        }).show();
                            } else {
                                Log.d("final", "Failure: ")
                                progressbar!!.visibility = View.GONE
                                btnInventoryRecord!!.isEnabled = true
                                btnInventoryRecord!!.isClickable = true
                                enableUserInteraction(activity)
                                if (response != 401) {
                                    FancyToast.makeText(
                                        requireActivity(),
                                        getString(R.string.error_data_sync),
                                        FancyToast.LENGTH_SHORT,
                                        FancyToast.ERROR,
                                        false
                                    ).show()
                                }
                            }
                        }
                }

                override fun onNegativeButtonClicked() {
                    btnInventoryRecord!!.isEnabled = true
                    btnInventoryRecord!!.isClickable = true
                    progressbar!!.visibility = View.GONE
                }
            }).show()
    }

    override fun onResume() {
        super.onResume()
        requireActivity().registerReceiver(receiver, IntentFilter("INVENTORYSTART"))
        if (inventoryViewModel!!.isSearchClicked) {
            Log.d("tag12", " isSearchClicked")
            inventoryButton!!.tag = "1"
            llBottomParent!!.visibility = View.VISIBLE
            inventoryButton!!.setImageResource(android.R.drawable.ic_media_play)
            // addDataToScanTag();
                showCountFound()

        }
        this.requireView().isFocusableInTouchMode = true
        this.requireView().requestFocus()
        this.requireView().setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                CommonAlertDialog(
                    requireActivity(),
                    "Are you sure you want to abandon this scan? Your data will be lost.",
                    "Yes",
                    "No",
                    object : OnButtonClickListener {
                        override fun onPositiveButtonClicked() {
                            try {
                                Application.isAbandoned = true
                                if (Application.isReconsiled) {
                                    if (pendingInventoryScan != null && !pendingInventoryScan!!.isEmpty()) {
                                        val (_, scanID) = pendingInventoryScan!![0]
                                        Application.bookDao.deleteScanTagSingle(
                                            scanID
                                        )
                                        Application.bookDao.deleteInventorySingle(
                                            scanID
                                        )
                                        requireActivity().supportFragmentManager.popBackStackImmediate()
                                        //call sync api
                                    }
                                } else {
                                    if (pendingInventoryScan != null && !pendingInventoryScan!!.isEmpty()) {
                                        val (_, scanID) = pendingInventoryScan!![0]
                                        Application.bookDao.deleteScanTagSingle(
                                            scanID
                                        )
                                        Application.bookDao.deleteInventorySingle(
                                            scanID
                                        )
                                        requireActivity().supportFragmentManager.popBackStackImmediate()
                                    }
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                        override fun onNegativeButtonClicked() {}
                    }).show()
                return@OnKeyListener true
            }
            false
        })
    }

    fun enableUserInteraction(requireActivity: FragmentActivity?) {
        requireActivity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun disableUserInteraction(requireActivity: FragmentActivity?) {
        requireActivity!!.window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    override fun onDataSent(isBack: Boolean) {
        Log.d("tag12", "onDataSent: $isBack")
        isFromReconsile = isBack
        inventoryButton!!.tag = "1"
        llBottomParent!!.visibility = View.VISIBLE
        inventoryButton!!.setImageResource(android.R.drawable.ic_media_play)
        // addDataToScanTag();
            showCountFound()

    }

    override fun onStop() {
        super.onStop()
        isFromReconsile = false
        Log.d("tag12", "onStop: ")
        inventoryViewModel!!.isSearchClicked = false
        requireActivity().unregisterReceiver(receiver)
    }

    companion object {
        @JvmStatic
        fun newInstance(): RapidReadFragment {
            return RapidReadFragment()
        }
    }
}