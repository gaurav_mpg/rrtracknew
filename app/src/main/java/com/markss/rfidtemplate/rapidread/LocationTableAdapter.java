package com.markss.rfidtemplate.rapidread;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.markss.rfidtemplate.R;

import java.util.ArrayList;

import asset.trak.model.GlobalRegisterCountModel;

public class LocationTableAdapter extends RecyclerView.Adapter<LocationTableAdapter.MyViewHolder>{

    private ArrayList<GlobalRegisterCountModel> arrayList;

    public LocationTableAdapter(ArrayList<GlobalRegisterCountModel> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_global_table_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try{
            if (arrayList.get(position).getLocation()!=null) {
                holder.locText.setText(arrayList.get(position).getLocation());
            }
            if (arrayList.get(position).getRegistered()!=null) {
                holder.registerCount.setText(arrayList.get(position).getRegistered());
            }
            if (arrayList.get(position).getScanned()!=null) {
                holder.scanCount.setText(arrayList.get(position).getScanned());
            }
        }catch (Exception e){

        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView locText,registerCount,scanCount;
        View lastView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            locText=itemView.findViewById(R.id.locText);
            registerCount=itemView.findViewById(R.id.registerCount);
            scanCount=itemView.findViewById(R.id.scanCount);
            lastView=itemView.findViewById(R.id.lastView);
        }
    }
}