package com.markss.rfidtemplate.rapidread

import asset.trak.utils.Constants.isInternetAvailable
import asset.trak.views.inventory.ReconcileAssetsFragment.Companion.setFragmentCallback
import asset.trak.utils.showYesNoAlert
import asset.trak.utils.GlobalFirebase
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.ResponseTagHandler
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.TriggerEventHandler
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.BatchModeEventHandler
import com.markss.rfidtemplate.common.ResponseHandlerInterfaces.ResponseStatusHandler
import asset.trak.views.listener.RapidReadCallback
import com.google.android.material.floatingactionbutton.FloatingActionButton
import asset.trak.modelsrrtrack.MasterLocation
import androidx.constraintlayout.widget.ConstraintLayout
import asset.trak.database.entity.Inventorymaster
import asset.trak.views.module.InventoryViewModel
import androidx.recyclerview.widget.RecyclerView
import android.os.Bundle
import android.content.BroadcastReceiver
import android.content.Intent
import android.app.Activity
import android.content.Context
import androidx.lifecycle.ViewModelProviders
import com.markss.rfidtemplate.home.MainActivity
import asset.trak.views.inventory.ReconcileAssetsFragment
import com.markss.rfidtemplate.rfid.RFIDController
import asset.trak.views.fragments.HomeFragment
import asset.trak.utils.CommonAlertDialog.OnButtonClickListener
import com.zebra.rfid.api3.RFIDResults
import com.zebra.rfid.api3.ACCESS_OPERATION_CODE
import com.zebra.rfid.api3.ACCESS_OPERATION_STATUS
import com.markss.rfidtemplate.common.hextoascii
import com.markss.rfidtemplate.inventory.InventoryListItem
import asset.trak.modelsrrtrack.AssetMain
import asset.trak.database.entity.ScanTag
import asset.trak.model.GlobalRegisterCountModel
import android.content.IntentFilter
import android.os.Handler
import asset.trak.model.AssetSyncRequestDataModel
import okhttp3.RequestBody
import com.google.gson.Gson
import com.shashank.sony.fancytoastlib.FancyToast
import android.os.Looper
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import asset.trak.modelsrrtrack.AssetData
import asset.trak.utils.mainCoroutines
import com.markss.rfidtemplate.R
import com.markss.rfidtemplate.application.Application
import com.markss.rfidtemplate.common.Constants
import com.markss.rfidtemplate.rapidread.GlobalRapidReadFragment
import com.zebra.rfid.api3.TagData
import kotlinx.coroutines.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.lang.Exception
import java.lang.Runnable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class GlobalRapidReadFragment : Fragment(), ResponseTagHandler, TriggerEventHandler,
    BatchModeEventHandler, ResponseStatusHandler, RapidReadCallback {
    private var uniqueTags: TextView? = null
    private var inventoryButton: FloatingActionButton? = null
    private val btnScanpause: FloatingActionButton? = null
    var batchModeEventReceived = false
    private val locationData: MasterLocation? = null
    private var tvFoundLocCount: TextView? = null
    private var tvNotFoundLocCount: TextView? = null
    private var tvDiffLocationCount: TextView? = null
    private var tvNotRegisteredCount: TextView? = null
    private var tvILastRecordDate: TextView? = null
    private var btnInventoryRecord: Button? = null
    private var llBottomParent: LinearLayout? = null
    private var globalScanData: ConstraintLayout? = null
    private var progressBar: ProgressBar? = null
    private var listInventoryList = HashSet<String>()
    private var scannedList = HashSet<String>()
    private var imgIgnore: ImageView? = null
    private var pendingInventoryScan: List<Inventorymaster>? = null
    private var inventoryViewModel: InventoryViewModel? = null
    private var totalRegisteredCount = 0
    private val countFoundCurrentLocation = 0
    private val countNotFoundCurrentLocation = 0
    private val countFoundDifferentLoc = 0
    private val countNotRegistered = 0
    private var isFromReconsile = false
    private var whichInventory: String? = ""
    private var tvRegisteredCount: TextView? = null
    private var locRecyclerView: RecyclerView? = null
    private var notRegisteredCount: TextView? = null
    var ivBack: ImageView? = null
    private var isScan = false
    private var locationTableAdapter: LocationTableAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_rr_global, container, false)
        imgIgnore = view.findViewById(R.id.closeButton)
        ivBack = view.findViewById(R.id.backButton)
        imgIgnore?.setVisibility(View.VISIBLE)
        val title = view.findViewById<TextView>(R.id.pageTitle)
        title.text = "Record Inventory"
        return view
        //
    }

    var receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val isInventoryStartStop = intent.getBooleanExtra("istart", false)
            if (!isInventoryStartStop) {
                isScan = true
                inventoryButton!!.tag = "0"
                llBottomParent!!.visibility = View.GONE
                globalScanData!!.visibility = View.GONE
                inventoryButton!!.setImageResource(android.R.drawable.ic_media_pause)
                listInventoryList = HashSet()
                //new requirement
                updateScanDateAndTime()
            } else {
                CoroutineScope(Dispatchers.Main).launch {

                    CoroutineScope(Dispatchers.Main.immediate).async {
                        isScan = false
                        inventoryButton!!.tag = "1"
                        inventoryButton!!.setImageResource(android.R.drawable.ic_media_play)
                        disableUserInteraction(requireActivity())
                        addDataToScanTag()
                        showCountFound()
                        updateCountInDb()
                    }.await()
                }

            }
        }
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_rr, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inventoryViewModel = ViewModelProviders.of(requireActivity()).get(
            InventoryViewModel::class.java
        )
        val arguments = arguments
        val mainActivity = activity as MainActivity?
        mainActivity!!.supportActionBar!!.navigationMode = ActionBar.NAVIGATION_MODE_STANDARD
        inventoryButton = mainActivity.findViewById(R.id.rr_inventoryButton)
        uniqueTags = mainActivity.findViewById(R.id.uniqueTagContent)
        globalScanData = requireActivity().findViewById(R.id.globalScanData)
        tvFoundLocCount = requireActivity().findViewById(R.id.tvFoundLocCount)
        tvNotFoundLocCount = requireActivity().findViewById(R.id.tvNotFoundLocCount)
        tvDiffLocationCount = requireActivity().findViewById(R.id.tvDiffLocationCount)
        tvNotRegisteredCount = requireActivity().findViewById(R.id.tvNotRegisteredCount)
        tvILastRecordDate = requireActivity().findViewById(R.id.tvILastRecord12)
        btnInventoryRecord = requireActivity().findViewById(R.id.btnInventoryRecord)
        llBottomParent = requireActivity().findViewById(R.id.llBottomParent2)
        progressBar = requireActivity().findViewById(R.id.progressBar4)
        tvRegisteredCount = requireActivity().findViewById(R.id.tvRegisteredCountrr)
        val tvLocation = requireActivity().findViewById<TextView>(R.id.tvLocation)
        notRegisteredCount = requireActivity().findViewById(R.id.notRegisteredCount)
        // locationData = getArguments().getParcelable("LocationData");
        locRecyclerView = requireActivity().findViewById(R.id.locRecyclerView)
        totalRegisteredCount = arguments?.getInt("totalRegistered") ?: 0
        btnInventoryRecord?.setOnClickListener(View.OnClickListener { v: View? ->
            if (isInternetAvailable(requireContext())) {
                postAssetSync()
            }
        })
        val changedFormat = SimpleDateFormat("dd-MM-yyyy | hh:mm a")
        try {
            //   String currentDate = changedFormat.format(new Date());
            tvILastRecordDate?.setText(
                getString(R.string.last_recorded_02_02_2022_10_43_am) + " " + changedFormat.format(
                    Date()
                )
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }


        //   tvRegisteredCount.setText(String.valueOf(totalRegisteredCount));
        if (getArguments() == null || arguments?.containsKey("INVENTORY_NAME") == false) {
        } else {
            whichInventory = arguments!!.getString("INVENTORY_NAME")
            if (whichInventory == "global") {
                tvRegisteredCount?.setText(
                    Application.bookDao.getCountRegisterForGlobal().toString()
                )
            }
        }
        listInventoryList = HashSet()
        scannedList = HashSet()
        setFragmentCallback(this)

        //    tvLocation.setText(locationData.getName());
        pendingInventoryScan = Application.bookDao.getGlobalPendingInventoryScan()
        if (!pendingInventoryScan!!.isEmpty()) {
            val tags = Application.bookDao.getGlobalScanRfid(
                pendingInventoryScan!![0].scanID
            )
            for (tag in tags) {
                scannedList.add(tag)
            }
        }
        if (scannedList.size < 10 && scannedList.size > 0) {
            uniqueTags?.setText("0" + scannedList.size)
        } else {
            uniqueTags?.setText(Integer.toString(scannedList.size))
        }
        if (RFIDController.mIsInventoryRunning) {
            inventoryButton?.setBackgroundResource(android.R.drawable.ic_media_pause)
        } else {
            inventoryButton?.setBackgroundResource(android.R.drawable.ic_media_play)
        }
        if (RFIDController.mRRStartedTime == 0L) Application.TAG_READ_RATE =
            0 else Application.TAG_READ_RATE =
            (Application.TOTAL_TAGS / (RFIDController.mRRStartedTime / 1000.toFloat())).toInt()
        if (Application.missedTags > 9999) {
            //orignal size is 60sp - reduced size 45sp
            uniqueTags?.setTextSize(45f)
        }
        updateTexts()
        ivBack!!.setOnClickListener { v: View? ->
            //   requireActivity().getSupportFragmentManager().popBackStackImmediate();
            requireActivity().supportFragmentManager.popBackStack(
                null,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            )
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.content_frame, HomeFragment()).commit()
        }
        imgIgnore!!.setOnClickListener { v: View? ->
            showYesNoAlert(
                requireActivity(),
                "Are you sure you want to abandon Current Scan.This will lost your Scan Data?.",
                object : OnButtonClickListener {
                    override fun onPositiveButtonClicked() {
                        try {
                            if (pendingInventoryScan != null && !pendingInventoryScan!!.isEmpty()) {
                                val lastItem = pendingInventoryScan!![0]
                                Application.bookDao.deleteScanTagSingle(lastItem.scanID)
                                Application.bookDao.deleteInventorySingle(lastItem.scanID)
                                requireActivity().supportFragmentManager.popBackStackImmediate()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onNegativeButtonClicked() {
                        progressBar?.setVisibility(View.GONE)
                    }
                })
        }


//        inventoryButton?.setOnClickListener {
//            if (inventoryButton?.getTag()?.equals("1")==true) {
//                inventoryButton?.setTag("0");
//                llBottomParent?.setVisibility(View.GONE);
//                globalScanData?.setVisibility(View.GONE);
//                inventoryButton?.setImageResource(android.R.drawable.ic_media_pause);
//                listInventoryList = HashSet()
//            } else {
//                inventoryButton?.setTag("1");
//                llBottomParent?.setVisibility(View.VISIBLE);
//                globalScanData?.setVisibility(View.VISIBLE);
//                inventoryButton?.setImageResource(android.R.drawable.ic_media_play);
//                addDataToScanTag()
//                showCountFound()
//                updateCountInDb()
//            }
//        }
        if (isFromReconsile) {
            inventoryButton?.setTag("1")
            setGlobalLocalInventoryViews()
            // llBottomParent.setVisibility(View.VISIBLE);
            inventoryButton?.setImageResource(android.R.drawable.ic_media_play)
            addDataToScanTag()
            showCountFound()
        } else {
            //   inventoryButton.performClick();
        }

        /*  inventoryButton.setTag("1");
        llBottomParent.setVisibility(View.VISIBLE);
        globalScanData.setVisibility(View.VISIBLE);
        inventoryButton.setImageResource(android.R.drawable.ic_media_play);
        addDataToScanTag();
        showCountFound();
        updateCountInDb();*/
    }

    fun updateScanDateAndTime() {
        //format and set the date here
        val changedFormat = SimpleDateFormat("dd-MM-yyyy | hh:mm a")
        try {
            //   String currentDate = changedFormat.format(new Date());
            tvILastRecordDate!!.text =
                getString(R.string.last_recorded_02_02_2022_10_43_am) + " " + changedFormat.format(
                    Date()
                )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun updateTexts() {}
    override fun onDetach() {
        super.onDetach()
        //  inventoryViewModel.resetTimer();
        if (RFIDController.mIsInventoryRunning) {
            inventoryButton!!.performClick()
        }
    }

    /**
     * method to reset tags info on the screen before starting inventory operation
     */
    fun resetTagsInfo() {
        updateTexts()
    }

    /**
     * method to start inventory operation on trigger press event received
     */
    override fun triggerPressEventRecieved() {
        if (!RFIDController.mIsInventoryRunning && activity != null) {
            requireActivity().runOnUiThread {
                val activity = activity as MainActivity?
                activity?.inventoryStartOrStop()
            }
        }
    }

    /**
     * method to stop inventory operation on trigger release event received
     */
    override fun triggerReleaseEventRecieved() {
        if (RFIDController.mIsInventoryRunning == true && activity != null) {
            //RFIDController.mInventoryStartPending = false;
            requireActivity().runOnUiThread { //   progressBar.setVisibility(View.VISIBLE);
                val activity = activity as MainActivity?
                activity?.inventoryStartOrStop()
            }
        }
    }

    override fun handleStatusResponse(results: RFIDResults) {
        requireActivity().runOnUiThread {
            if (results == RFIDResults.RFID_BATCHMODE_IN_PROGRESS) {
            } else if (results != RFIDResults.RFID_API_SUCCESS) {
                RFIDController.mIsInventoryRunning = false
                if (inventoryButton != null) {
                    inventoryButton!!.setBackgroundResource(android.R.drawable.ic_media_play)
                }
                RFIDController.isBatchModeInventoryRunning = false
            }
        }
    }

    /**
     * method to update inventory details on the screen on operation end summary received
     */
    fun updateInventoryDetails() {
        updateTexts()
    }

    /**
     * method to reset inventory operation status on the screen
     */
    fun resetInventoryDetail() {
        if (activity != null) requireActivity().runOnUiThread {
            if (RFIDController.ActiveProfile.id != "1") {
                if (inventoryButton != null && !RFIDController.mIsInventoryRunning &&
                    (RFIDController.isBatchModeInventoryRunning == null || !RFIDController.isBatchModeInventoryRunning)
                ) {
                    inventoryButton!!.setBackgroundResource(android.R.drawable.ic_media_play)
                }
                if (uniqueTags != null) {
                }
            }
        }
    }

    override fun batchModeEventReceived() {
        batchModeEventReceived = true
        if (inventoryButton != null) inventoryButton!!.setBackgroundResource(android.R.drawable.ic_media_pause)
    }

    fun handleTagResponseFetch(response_tagData: TagData?) {
        requireActivity().runOnUiThread {
            if (response_tagData != null) {
                val readAccessOperation = response_tagData.opCode
                if (readAccessOperation != null) {
                    if (response_tagData.opStatus != null && response_tagData.opStatus != ACCESS_OPERATION_STATUS.ACCESS_SUCCESS) {
                        val strErr =
                            response_tagData.opStatus.toString().replace("_".toRegex(), " ")
                        // Toast.makeText(getActivity(), strErr.toLowerCase(), Toast.LENGTH_SHORT).show();
                    } else {
                        if (response_tagData.opCode === ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ) {
                            //      TextView text = (TextView) getActivity().findViewById(R.id.accessRWData);
                            val resultData =
                                if (RFIDController.asciiMode == true) hextoascii.convert(
                                    response_tagData.memoryBankData
                                ) else response_tagData.memoryBankData
                            // hextoascii.convert(response_tagData.getMemoryBankData());
                            Log.d("new1111", "run: $resultData")
                        } else {
                            Log.d("new1111", "run else: ")
                        }
                    }
                } else {
                    Toast.makeText(activity, R.string.err_access_op_failed, Toast.LENGTH_SHORT)
                        .show()
                    Constants.logAsMessage(
                        Constants.TYPE_DEBUG,
                        "ACCESS READ",
                        "memoryBankData is null"
                    )
                }
            } else {
                Toast.makeText(activity, R.string.err_access_op_failed, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun handleTagResponse(inventoryListItem: InventoryListItem, isAddedToList: Boolean) {
        updateTexts()
        if (isScan) {
            listInventoryList.add(inventoryListItem.tagID)
            scannedList.add(inventoryListItem.tagID)
            uniqueTags!!.text = Integer.toString(scannedList.size)
        }

//        Log.d("new1111", "handleTagResponse tag:  "+inventoryListItem.getTagID());
        /*  getInstance().accessOperationsRead(inventoryListItem.getTagID(),"0","0","00","TID",
                new RfidListeners() {
                    @Override
                    public void onSuccess(Object object) {
                        if (isAccessCriteriaRead && !mIsInventoryRunning) {

                            handleTagResponseFetch((TagData) object);
                        }


                    }

                    @Override
                    public void onFailure(Exception exception) {
                        if (exception instanceof InvalidUsageException) {
                            Toast.makeText(requireContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();

                        } else if (exception instanceof OperationFailureException) {
                            Toast.makeText(requireContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(requireContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(String message) {
                        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
                    }
                });*/
    }

    private fun addDataToScanTag() {
        try {
            if (pendingInventoryScan!!.isEmpty()) {
            } else {
                val (_, scanID) = pendingInventoryScan!![0]
                //
//                listInventoryList.add("30361F8A403243974876E9FA");
//                listInventoryList.add("30361F88BC0FF4574876E804");
//                listInventoryList.add("30361F88BC19AA174876E804");
//                listInventoryList.add("30361F8A40073E974876E820");
//                listInventoryList.add("30361F88BC2097174876E81C");
//                listInventoryList.add("30361F88BC2FA1974876E80E");
//                listInventoryList.add("30361F65E40910D74876F0ED");
//                listInventoryList.add("00361FB3281E3B574876E808");
//                listInventoryList.add("00361F68680A7B574876E803");
//                listInventoryList.add("30361F88BC4FF8974876E82B");
//                listInventoryList.add("30361F88BC1A27574876E848");
//                listInventoryList.add("30361F8BAC38C0D74876E8D0");
//                listInventoryList.add("30361F88BC4406D74876E84F");
//                listInventoryList.add("30361F8A40187E174876E86D");
//                listInventoryList.add("30361F8A4008DD974876E879");
//                listInventoryList.add("30361F87A01B68D74876E841");
//                listInventoryList.add("30361F8A4008DC574876E824");
//                listInventoryList.add("30361F87A03624D74876E9CE");
//                listInventoryList.add("00361F8A401885D74876E845");
//                listInventoryList.add("00361F8A401AF4D74876E80F");
//                listInventoryList.add("00361F88BC168E974876E830");
//                listInventoryList.add("00361F88BC581AD74876E801");
//                listInventoryList.add("30361F87A00BF4974876E80E");
//                listInventoryList.add("30361F87A00B5F974876E83B");
//                listInventoryList.add("30361F87A029E8974876E856");
//                listInventoryList.add("30361F88BC3F52574876E829");
//                listInventoryList.add("30361F8A404F0ED74876E9EA");
//                listInventoryList.add("30361F87A02134D74876E869");
//                listInventoryList.add("30361F88BC2584D74876E849");
//                listInventoryList.add("30361F87A035DD974876E887");
//                listInventoryList.add("30361F87A0362BD74876E847");
//                listInventoryList.add("30361F89D45E02D74876E82A");
//                listInventoryList.add("00361F8A402D8E574876E89A");
//                listInventoryList.add("30361F88BC328C574876E80F");
//                listInventoryList.add("00361F87983C82574876E835");
//                listInventoryList.add("30361F85083BB5174876F3AF");
//                listInventoryList.add("30361F88BC5C0B174876E85B");
//                listInventoryList.add("30361F88BC2074D74876E880");
//                listInventoryList.add("30361F8A4024F9D74876E803");
//                listInventoryList.add("30361F8A40332D974876E848");
                mainCoroutines {
                    progressBar!!.visibility = View.VISIBLE
                    CoroutineScope(Dispatchers.IO).async {
                        for (inventoryTag in listInventoryList) {
                            val assetMain = Application.bookDao.getRFIDTagDetails(inventoryTag)
                            val scanTag = ScanTag()
                            scanTag.scanId = scanID
                            var locId = -1
                            var assetId: String? = null
                            if (assetMain != null && assetMain.LocationId != -1) {
                                locId = assetMain.LocationId
                                assetId = assetMain.AssetID
                            }
                            scanTag.locationId = locId
                            scanTag.assetId = assetId
                            scanTag.rfidTag = inventoryTag
                            val getCountOfTagAlready = Application.bookDao.getCountOfTagAlready(
                                scanTag.rfidTag!!, scanTag.scanId!!
                            )
                            if (getCountOfTagAlready == 0)
                                Application.bookDao.addScanTag(scanTag)
                        }

                    }.await()
                    //                ExtensionKt.updateListToFirebase("Global",requireActivity(),listInventoryList);
                    val tableCountable: ArrayList<GlobalRegisterCountModel> =
                        ArrayList<GlobalRegisterCountModel>()
                    inventoryViewModel!!.gettableGlobal(scanID)
                        .observe(requireActivity()) { globalRegisterCountModels ->
                            tableCountable.addAll(globalRegisterCountModels!!)
                            locationTableAdapter = LocationTableAdapter(tableCountable)
                            locRecyclerView!!.adapter = locationTableAdapter
                            val notRegisterCount = Application.bookDao.getNotRegisteredCount()
                            notRegisteredCount!!.text = notRegisterCount.toString()
                        }
                    progressBar!!.visibility = View.GONE
                    enableUserInteraction(requireActivity())
                    llBottomParent!!.visibility = View.VISIBLE
                    globalScanData!!.visibility = View.VISIBLE
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun updateCountInDb() {
        try {
            if (!pendingInventoryScan!!.isEmpty()) {
                val inventoryMaster = pendingInventoryScan!![0]
                inventoryMaster.foundOnLocation = countFoundCurrentLocation
                inventoryMaster.notFound = countNotFoundCurrentLocation
                inventoryMaster.foundOfDiffLocation = countFoundDifferentLoc
                inventoryMaster.notRegistered = countNotRegistered
                inventoryMaster.registered = 0
                val sdf = SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.ENGLISH)
                val cal = Calendar.getInstance()
                val dateFormat = sdf.format(cal.time)
                inventoryMaster.scanOn = dateFormat
                Application.bookDao.updateInventoryItem(inventoryMaster)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showCountFound() {
        setGlobalLocalInventoryViews()
    }

    private fun setGlobalLocalInventoryViews() {

        // llBottomParent.setVisibility(View.VISIBLE);
        btnInventoryRecord!!.visibility = View.VISIBLE
    }

    override fun onStop() {
        super.onStop()
        requireActivity().unregisterReceiver(receiver)
    }

    override fun onResume() {
        super.onResume()
        requireActivity().registerReceiver(receiver, IntentFilter("INVENTORYSTART"))
        this.requireView().isFocusableInTouchMode = true
        this.requireView().requestFocus()
        this.requireView().setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                showYesNoAlert(
                    requireActivity(),
                    "Are you sure you want to abandon this scan? Your data will be lost.",
                    object : OnButtonClickListener {
                        override fun onPositiveButtonClicked() {
                            try {
                                if (Application.isReconsiled) {
                                    if (pendingInventoryScan != null && !pendingInventoryScan!!.isEmpty()) {
                                        val (_, scanID) = pendingInventoryScan!![0]
                                        Application.bookDao.deleteScanTagSingle(
                                            scanID
                                        )
                                        Application.bookDao.deleteInventorySingle(
                                            scanID
                                        )
                                        requireActivity().supportFragmentManager.popBackStackImmediate()
                                        //call sync api
                                    }
                                } else {
                                    if (pendingInventoryScan != null && !pendingInventoryScan!!.isEmpty()) {
                                        val (_, scanID) = pendingInventoryScan!![0]
                                        Application.bookDao.deleteScanTagSingle(
                                            scanID
                                        )
                                        Application.bookDao.deleteInventorySingle(
                                            scanID
                                        )
                                        requireActivity().supportFragmentManager.popBackStackImmediate()
                                    }
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                        override fun onNegativeButtonClicked() {}
                    })
                return@OnKeyListener true
            }
            false
        })
    }

    private fun postAssetSync() {
        showYesNoAlert(
            requireActivity(),
            "Are you sure you want to complete Scan?.",
            object : OnButtonClickListener {
                override fun onPositiveButtonClicked() {
                    progressBar!!.visibility = View.VISIBLE
                    btnInventoryRecord!!.isEnabled = false
                    btnInventoryRecord!!.isClickable = false
                    disableUserInteraction(activity)
                    val bookAndAssetData: MutableList<AssetMain> = ArrayList()
                    val pendingInventoryScan = Application.bookDao.getGlobalPendingInventoryScan()
                    val inventoryMaster = pendingInventoryScan[0]
                    bookAndAssetData.addAll(
                        Application.bookDao.getFoundAtLocationGlobal(
                            inventoryMaster.scanID
                        )
                    )
                    val assetSyncRequestDataModel = AssetSyncRequestDataModel()
                    val changedFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    var scanEndTime: String? = ""
                    try {
                        //   String currentDate = changedFormat.format(new Date());
                        scanEndTime = changedFormat.format(Date())
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    assetSyncRequestDataModel.inventoryData.deviceID = inventoryMaster.deviceId
                    assetSyncRequestDataModel.inventoryData.foundForLoc = 0
                    assetSyncRequestDataModel.inventoryData.foundForOtherLoc = 0
                    assetSyncRequestDataModel.inventoryData.noofAssetsScanned = scannedList.size
                    assetSyncRequestDataModel.inventoryData.scanDate =
                        inventoryMaster.scanStartDatetime
                    assetSyncRequestDataModel.inventoryData.locID = 0
                    assetSyncRequestDataModel.inventoryData.locType = "G"
                    assetSyncRequestDataModel.inventoryData.scanStartDatetime =
                        inventoryMaster.scanStartDatetime
                    assetSyncRequestDataModel.inventoryData.scanEndDatetime = scanEndTime
                    //       assetSyncRequestDataModel.inventoryData.notRegistered=Integer.parseInt(tvRegisteredCount.getText().toString());
                    assetSyncRequestDataModel.inventoryData.notRegistered = countNotRegistered
                    assetSyncRequestDataModel.inventoryData.scanID = inventoryMaster.scanID
                    assetSyncRequestDataModel.inventoryData.scannedBy =
                        Application.sharedPreferences.getString("username", "SYSTEM")
                    for (n in bookAndAssetData) {
                        val scanTag = AssetData()
                        // sending ID in rfidTag field, need to update attribute name accordingly in API
                        scanTag.assetRFID = n.AssetRFID
                        if (n.ScanID == null) {
                            n.ScanID = "0"
                        }
                        scanTag.assetID = n.AssetID
                        scanTag.locID = n.LocationId
                        assetSyncRequestDataModel.assetData.add(scanTag)
                    }
                    GlobalFirebase("GlobalInventory", requireActivity(), assetSyncRequestDataModel)
                    val body: RequestBody = RequestBody.create(
                        "application/json".toMediaTypeOrNull(),
                        Gson().toJson(assetSyncRequestDataModel)
                    )
                    Log.e("data", "" + Gson().toJson(assetSyncRequestDataModel))
                    inventoryViewModel!!.postAssetSync(body)
                        .observe(viewLifecycleOwner) { response: Int ->
                            if (response == Constants.SUCCESS) {
                                Log.d("final", "postAssetSync: ")
                                btnInventoryRecord!!.isEnabled = true
                                btnInventoryRecord!!.isClickable = true
                                inventoryMaster.status =
                                    asset.trak.utils.Constants.InventoryStatus.COMPLETED
                                Application.bookDao.updateInventoryItem(inventoryMaster)
                                Application.isRecordInventory = true
                                Application.bookDao.deleteScanTagSingle(inventoryMaster.scanID)
                                enableUserInteraction(activity)
                                FancyToast.makeText(
                                    requireActivity(),
                                    getString(R.string.data_sync_success),
                                    FancyToast.LENGTH_SHORT,
                                    FancyToast.SUCCESS,
                                    false
                                ).show()
                                requireActivity().runOnUiThread(object : Runnable {
                                    override fun run() {
                                        Handler((Looper.myLooper())!!).postDelayed(Runnable {
                                            progressBar!!.visibility = View.GONE
                                            val activity: Activity? = activity
                                            if (isAdded && activity != null) {
                                                requireActivity().supportFragmentManager.popBackStackImmediate()
                                            }
                                        }, 500)
                                    }
                                })

                                /* new CommonAlertDialog(requireActivity(), getString(R.string.data_sync_success), "Ok", "", new CommonAlertDialog.OnButtonClickListener() {
                            @Override
                            public void onPositiveButtonClicked() {
                                requireActivity().getSupportFragmentManager().popBackStackImmediate();
                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }
                        }).show();*/
                            } else {
                                Log.d("final", "Failure: ")
                                progressBar!!.visibility = View.GONE
                                btnInventoryRecord!!.isEnabled = true
                                btnInventoryRecord!!.isClickable = true
                                enableUserInteraction(activity)
                                if (response != 401) {
                                    FancyToast.makeText(
                                        requireActivity(),
                                        getString(R.string.error_data_sync),
                                        FancyToast.LENGTH_SHORT,
                                        FancyToast.ERROR,
                                        false
                                    ).show()
                                }
                            }
                        }
                }

                override fun onNegativeButtonClicked() {}
            })
    }

    fun enableUserInteraction(requireActivity: FragmentActivity?) {
        requireActivity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun disableUserInteraction(requireActivity: FragmentActivity?) {
        requireActivity!!.window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    override fun onDataSent(isBack: Boolean) {
        Log.d("tag12", "onDataSent: $isBack")
        isFromReconsile = isBack
        inventoryButton!!.tag = "1"
        setGlobalLocalInventoryViews()
        inventoryButton!!.setImageResource(android.R.drawable.ic_media_play)
        addDataToScanTag()
        showCountFound()
    }

    companion object {
        fun newInstance(whichInventory: String?): GlobalRapidReadFragment {
            val rapidReadFragment = GlobalRapidReadFragment()
            val args = Bundle()
            args.putString("INVENTORY_NAME", whichInventory)
            rapidReadFragment.arguments = args
            return rapidReadFragment
        }
    }
}